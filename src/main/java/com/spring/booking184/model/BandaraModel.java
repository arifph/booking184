package com.spring.booking184.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name="M_BANDARA")
public class BandaraModel {
	// Nama Column
	private Integer idBandara; // PK
	private String kodeBandara;
	private String namaBandara;
	private String jenisBandara;
	private String alamatBandara;
	private Integer isDelete;
	
	// Join Table KotaModel
	private Integer idKota;
	private KotaModel kotaModel;

	//audit Trail
	private Integer xIdCreatedBy;
	private UserModel xCreatedBy;
	private Date xCreatedDate;
	
	private Integer xIdUpdatedBy;
	private UserModel xUpdatedBy;
	private Date xUpdatedDate;
	
	private Integer xIdDeletedBy;
	private UserModel xDeletedBy;
	private Date xDeletedDate;
	//audit Trail
	
	@Id
	@Column(name="ID_BANDARA")
	@GeneratedValue(strategy=GenerationType.TABLE, generator="M_BANDARA") // utk buat nilai sequential
	@TableGenerator(name="M_BANDARA", table="M_SEQUENCE",
					pkColumnName="SEQUENCE_NAME", pkColumnValue="ID_BANDARA",
					valueColumnName="SEQUENCE_VALUE",allocationSize=1,initialValue=1)
	public Integer getIdBandara() {
		return idBandara;
	}
	public void setIdBandara(Integer idBandara) {
		this.idBandara = idBandara;
	}
	
	@Column(name="KODE_BANDARA")
	public String getKodeBandara() {
		return kodeBandara;
	}
	public void setKodeBandara(String kodeBandara) {
		this.kodeBandara = kodeBandara;
	}
	
	@Column(name="NAMA_BANDARA")
	public String getNamaBandara() {
		return namaBandara;
	}
	public void setNamaBandara(String namaBandara) {
		this.namaBandara = namaBandara;
	}
	
	@Column(name="JENIS_BANDARA")
	public String getJenisBandara() {
		return jenisBandara;
	}
	public void setJenisBandara(String jenisBandara) {
		this.jenisBandara = jenisBandara;
	}
	
	@Column(name="ALAMAT_BANDARA")
	public String getAlamatBandara() {
		return alamatBandara;
	}
	public void setAlamatBandara(String alamatBandara) {
		this.alamatBandara = alamatBandara;
	}
	
	@Column(name= "IS_DELETE")
	public Integer getIsDelete() {
		return isDelete;
	}
	public void setIsDelete(Integer isDelete) {
		this.isDelete = isDelete;
	}
	
	@Column(name="ID_KOTA")
	public Integer getIdKota() {
		return idKota;
	}
	public void setIdKota(Integer idKota) {
		this.idKota = idKota;
	}
	
	@ManyToOne
	@JoinColumn(name="ID_KOTA", nullable=true, updatable=false, insertable=false)
	public KotaModel getKotaModel() {
		return kotaModel;
	}
	public void setKotaModel(KotaModel kotaModel) {
		this.kotaModel = kotaModel;
	}
	
	@Column(name="X_ID_CREATED")
	public Integer getxIdCreatedBy() {
		return xIdCreatedBy;
	}
	public void setxIdCreatedBy(Integer xIdCreatedBy) {
		this.xIdCreatedBy = xIdCreatedBy;
	}
	
	@ManyToOne
	@JoinColumn(name="X_ID_CREATED", nullable=true, updatable=false, insertable=false)
	public UserModel getxCreatedBy() {
		return xCreatedBy;
	}
	public void setxCreatedBy(UserModel xCreatedBy) {
		this.xCreatedBy = xCreatedBy;
	}
	
	@Column(name="X_CREATED_DATE")
	public Date getxCreatedDate() {
		return xCreatedDate;
	}
	public void setxCreatedDate(Date xCreatedDate) {
		this.xCreatedDate = xCreatedDate;
	}
	
	@Column(name="X_ID_UPDATED")
	public Integer getxIdUpdatedBy() {
		return xIdUpdatedBy;
	}
	public void setxIdUpdatedBy(Integer xIdUpdatedBy) {
		this.xIdUpdatedBy = xIdUpdatedBy;
	}
	
	@ManyToOne
	@JoinColumn(name="X_ID_UPDATED", nullable=true, updatable=false, insertable=false)
	public UserModel getxUpdatedBy() {
		return xUpdatedBy;
	}
	public void setxUpdatedBy(UserModel xUpdatedBy) {
		this.xUpdatedBy = xUpdatedBy;
	}
	
	@Column(name="X_UPDATED_DATE")
	public Date getxUpdatedDate() {
		return xUpdatedDate;
	}
	public void setxUpdatedDate(Date xUpdatedDate) {
		this.xUpdatedDate = xUpdatedDate;
	}
	
	@Column(name="X_ID_DELETED")
	public Integer getxIdDeletedBy() {
		return xIdDeletedBy;
	}
	public void setxIdDeletedBy(Integer xIdDeletedBy) {
		this.xIdDeletedBy = xIdDeletedBy;
	}
	
	@ManyToOne
	@JoinColumn(name="X_ID_DELETED", nullable=true, updatable=false, insertable=false)
	public UserModel getxDeletedBy() {
		return xDeletedBy;
	}
	public void setxDeletedBy(UserModel xDeletedBy) {
		this.xDeletedBy = xDeletedBy;
	}
	
	@Column(name="X_DELETED_DATE")
	public Date getxDeletedDate() {
		return xDeletedDate;
	}
	public void setxDeletedDate(Date xDeletedDate) {
		this.xDeletedDate = xDeletedDate;
	}
	
}
