package com.spring.booking184.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name="M_STASIUN")
public class StasiunModel {

	private Integer idStasiun;
	private String kodeStasiun;
	private String namaStasiun;
	private String telepon;
	private String email;
	private String alamat;
	private Integer idKota;
	private KotaModel kotaModel;
	private Integer isDeleted;
	private Integer xIdCreatedBy;
	private UserModel xCreatedBy;
	private Date xCreatedDate;
	private Integer xIdUpdatedBy;
	private UserModel xUpdatedBy;
	private Date xUpdatedDate;
	private Integer xIdDeletedBy;
	private UserModel xDeletedBy;
	private Date xDeletedDate;
	
	@Id
	@Column(name="ID_STASIUN")
	@GeneratedValue(strategy=GenerationType.TABLE, generator="M_STASIUN")
	@TableGenerator(name="M_STASIUN", table="M_SEQUENCE", pkColumnName="SEQUENCE_NAME", 
		valueColumnName="SEQUENCE_VALUE", pkColumnValue="ID_STASIUN", allocationSize=1, initialValue=1)
	public Integer getIdStasiun() {
		return idStasiun;
	}
	public void setIdStasiun(Integer idStasiun) {
		this.idStasiun = idStasiun;
	}
	
	@Column(name="KODE_STASIUN")
	public String getKodeStasiun() {
		return kodeStasiun;
	}
	public void setKodeStasiun(String kodeStasiun) {
		this.kodeStasiun = kodeStasiun;
	}
	@Column(name="NAMA_STASIUN")
	public String getNamaStasiun() {
		return namaStasiun;
	}
	public void setNamaStasiun(String namaStasiun) {
		this.namaStasiun = namaStasiun;
	}
	
	@Column(name="TELEPON")
	public String getTelepon() {
		return telepon;
	}
	public void setTelepon(String telepon) {
		this.telepon = telepon;
	}
	
	@Column(name="EMAIL")
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Column(name="ALAMAT")
	public String getAlamat() {
		return alamat;
	}
	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}
	
	@Column(name="ID_KOTA")
	public Integer getIdKota() {
		return idKota;
	}
	public void setIdKota(Integer idKota) {
		this.idKota = idKota;
	}
	
	@ManyToOne
	@JoinColumn(name="ID_KOTA", insertable=false, nullable=true, updatable=false)
	public KotaModel getKotaModel() {
		return kotaModel;
	}
	public void setKotaModel(KotaModel kotaModel) {
		this.kotaModel = kotaModel;
	}
	
	@Column(name="IS_DELETED")	
	public Integer getIsDeleted() {
		return isDeleted;
	}
	public void setIsDeleted(Integer isDeleted) {
		this.isDeleted = isDeleted;
	}
	
	@Column(name="X_ID_CREATED_BY")
	public Integer getxIdCreatedBy() {
		return xIdCreatedBy;
	}
	public void setxIdCreatedBy(Integer xIdCreatedBy) {
		this.xIdCreatedBy = xIdCreatedBy;
	}
	
	@ManyToOne
	@JoinColumn(name="X_ID_CREATED_BY", insertable=false, nullable=true, updatable=false)
	public UserModel getxCreatedBy() {
		return xCreatedBy;
	}
	public void setxCreatedBy(UserModel xCreatedBy) {
		this.xCreatedBy = xCreatedBy;
	}
	
	@Column(name="X_CREATED_DATE")
	public Date getxCreatedDate() {
		return xCreatedDate;
	}
	public void setxCreatedDate(Date xCreatedDate) {
		this.xCreatedDate = xCreatedDate;
	}
	
	@Column(name="X_ID_UPDATED_BY")
	public Integer getxIdUpdatedBy() {
		return xIdUpdatedBy;
	}
	public void setxIdUpdatedBy(Integer xIdUpdatedBy) {
		this.xIdUpdatedBy = xIdUpdatedBy;
	}
	
	@ManyToOne
	@JoinColumn(name="X_ID_UPDATED_BY", insertable=false, nullable=true, updatable=false)
	public UserModel getxUpdatedBy() {
		return xUpdatedBy;
	}
	public void setxUpdatedBy(UserModel xUpdatedBy) {
		this.xUpdatedBy = xUpdatedBy;
	}
	
	@Column(name="X_UPDATED_DATE")
	public Date getxUpdatedDate() {
		return xUpdatedDate;
	}
	public void setxUpdatedDate(Date xUpdatedDate) {
		this.xUpdatedDate = xUpdatedDate;
	}
	
	@Column(name="X_ID_DELETED_BY")
	public Integer getxIdDeletedBy() {
		return xIdDeletedBy;
	}
	public void setxIdDeletedBy(Integer xIdDeletedBy) {
		this.xIdDeletedBy = xIdDeletedBy;
	}
	
	@ManyToOne
	@JoinColumn(name="X_ID_DELETED_BY", insertable=false, nullable=true, updatable=false)
	public UserModel getxDeletedBy() {
		return xDeletedBy;
	}
	public void setxDeletedBy(UserModel xDeletedBy) {
		this.xDeletedBy = xDeletedBy;
	}
	
	@Column(name="X_DELETED_DATE")
	public Date getxDeletedDate() {
		return xDeletedDate;
	}
	public void setxDeletedDate(Date xDeletedDate) {
		this.xDeletedDate = xDeletedDate;
	}
}
