package com.spring.booking184.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name="M_LOKASI")
public class LokasiModel {
	private Integer idLokasi;
	private String kodeLokasi;
	private String namaLokasi;
	
	@Id
	@Column(name="ID_LOKASI")
	@GeneratedValue(strategy=GenerationType.TABLE, generator="M_LOKASI")
	@TableGenerator(name="M_LOKASI", table="M_SEQUENCE",
					pkColumnName="SEQUENCE_NAME", pkColumnValue="ID_LOKASI",
					valueColumnName="SEQUENCE_VALUE",allocationSize=1,initialValue=1)
	public Integer getIdLokasi() {
		return idLokasi;
	}
	public void setIdLokasi(Integer idLokasi) {
		this.idLokasi = idLokasi;
	}
	
	@Column(name="KODE_LOKASI")
	public String getKodeLokasi() {
		return kodeLokasi;
	}
	public void setKodeLokasi(String kodeLokasi) {
		this.kodeLokasi = kodeLokasi;
	}
	
	@Column(name="NAMA_LOKASI")
	public String getNamaLokasi() {
		return namaLokasi;
	}
	public void setNamaLokasi(String namaLokasi) {
		this.namaLokasi = namaLokasi;
	}
}
