package com.spring.booking184.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name="M_MAHASISWA")
public class MahasiswaModel {
	
	private Integer idMahasiswa;
	private String nimMahasiswa;
	private String namaMahasiswa;
	private String teleponMahasiswa;
	private String genderMahasiswa;
	private String alamatMahasiswa;
	private Integer tahunLahirMahasiswa;
	
	@Id
	@Column(name="ID_MHS")
	@GeneratedValue(strategy=GenerationType.TABLE, generator="M_MAHASISWA") // utk buat nilai sequential
	@TableGenerator(name="M_MAHASISWA", table="M_SEQUENCE",
					pkColumnName="SEQUENCE_NAME", pkColumnValue="M_MAHASISWA_ID",
					valueColumnName="SEQUENCE_VALUE",allocationSize=1,initialValue=1)
	public Integer getIdMahasiswa() {
		return idMahasiswa;
	}
	public void setIdMahasiswa(Integer idMahasiswa) {
		this.idMahasiswa = idMahasiswa;
	}
	
	@Column(name="NIM_MHS")
	public String getNimMahasiswa() {
		return nimMahasiswa;
	}
	public void setNimMahasiswa(String nimMahasiswa) {
		this.nimMahasiswa = nimMahasiswa;
	}
	
	@Column(name="NAMA_MHS")
	public String getNamaMahasiswa() {
		return namaMahasiswa;
	}
	public void setNamaMahasiswa(String namaMahasiswa) {
		this.namaMahasiswa = namaMahasiswa;
	}
	
	@Column(name="TELEPON_MHS")
	public String getTeleponMahasiswa() {
		return teleponMahasiswa;
	}
	public void setTeleponMahasiswa(String teleponMahasiswa) {
		this.teleponMahasiswa = teleponMahasiswa;
	}
	
	@Column(name="GENDER_MHS")
	public String getGenderMahasiswa() {
		return genderMahasiswa;
	}
	public void setGenderMahasiswa(String genderMahasiswa) {
		this.genderMahasiswa = genderMahasiswa;
	}
	
	@Column(name="ALAMAT_MHS")
	public String getAlamatMahasiswa() {
		return alamatMahasiswa;
	}
	public void setAlamatMahasiswa(String alamatMahasiswa) {
		this.alamatMahasiswa = alamatMahasiswa;
	}
	
	@Column(name="TAHUN_LAHIR_MHS")
	public Integer getTahunLahirMahasiswa() {
		return tahunLahirMahasiswa;
	}
	public void setTahunLahirMahasiswa(Integer tahunLahirMahasiswa) {
		this.tahunLahirMahasiswa = tahunLahirMahasiswa;
	}
	
	
	


}
