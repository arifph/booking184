package com.spring.booking184.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

	
@Entity
@Table(name="M_TERMINAL") //menandakan tabel
public class TerminalModel {

	//	Integer nilai=null; nilai belum pasti gunakan null >> String, Longint
	//	int tabunganBCA=100000; kalo sudah nilai pasti pake int
	
	private Integer idTerminal; //PK dari FakultasModel
	private String kodeTerminal;
	private String namaTerminal;
	private String noTelpon;
	private String email;
	private String alamat;
	private Integer isDelete;
	
	//join table LokasiModel
	private Integer idKota; //FK dari LokasiModel
	private KotaModel kotaModel;
	
	//Audit Trail
	private Integer xIdCreatedBy; 
	private UserModel xCreatedBy; //id usernya
	private Date xCreatedDate; //
	
	private Integer xIdUpdatedBy;
	private UserModel xUpdatedBy;
	private Date xUpdatedDate;
	
	private Integer xIdDeletedBy;
	private UserModel xDeletedBy;
	private Date xDeletedDate;
	//Audit Traill

	
	
	@Id // menandakan primary key
	@Column(name="ID_TERMINAL")
	@GeneratedValue(strategy=GenerationType.TABLE, generator="M_TERMINAL") // utk buat nilai sequential
	@TableGenerator(name="M_TERMINAL", table="M_SEQUENCE",
					pkColumnName="SEQUENCE_NAME", pkColumnValue="ID_TERMINAL",
					valueColumnName="SEQUENCE_VALUE",allocationSize=1,initialValue=1)
	public Integer getIdTerminal() {
		return idTerminal;
	}
	public void setIdTerminal(Integer idTerminal) {
		this.idTerminal = idTerminal;
	}
	
	@Column(name="KODE_TERMINAL")
	public String getKodeTerminal() {
		return kodeTerminal;
	}
	public void setKodeTerminal(String kodeTerminal) {
		this.kodeTerminal = kodeTerminal;
	}
	
	@Column(name="NAMA_TERMINAL")
	public String getNamaTerminal() {
		return namaTerminal;
	}
	public void setNamaTerminal(String namaTerminal) {
		this.namaTerminal = namaTerminal;
	}
	
	@Column(name="NO_TELPON")
	public String getNoTelpon() {
		return noTelpon;
	}
	public void setNoTelpon(String noTelpon) {
		this.noTelpon = noTelpon;
	}
	
	@Column(name="EMAIL")
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Column(name="ALAMAT")
	public String getAlamat() {
		return alamat;
	}
	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}
	
	@Column(name="IS_DELETE")
	public Integer getIsDelete() {
		return isDelete;
	}
	public void setIsDelete(Integer isDelete) {
		this.isDelete = isDelete;
	}
	
	@Column(name="ID_KOTA")
	public Integer getIdKota() {
		return idKota;
	}
	public void setIdKota(Integer idKota) {
		this.idKota = idKota;
	}
	
	@ManyToOne
	@JoinColumn(name="ID_KOTA", nullable=true, updatable=false, insertable=false)
	public KotaModel getKotaModel() {
		return kotaModel;
	}
	public void setKotaModel(KotaModel kotaModel) {
		this.kotaModel = kotaModel;
	}
	
	@Column(name="X_ID_CREATED")
	public Integer getxIdCreatedBy() {
		return xIdCreatedBy;
	}
	public void setxIdCreatedBy(Integer xIdCreatedBy) {
		this.xIdCreatedBy = xIdCreatedBy;
	}
	
	@ManyToOne
	@JoinColumn(name="X_ID_CREATED", nullable=true, updatable=false, insertable=false)
	public UserModel getxCreatedBy() {
		return xCreatedBy;
	}
	public void setxCreatedBy(UserModel xCreatedBy) {
		this.xCreatedBy = xCreatedBy;
	}
	
	@Column(name="X_CREATED_DATE")
	public Date getxCreatedDate() {
		return xCreatedDate;
	}
	public void setxCreatedDate(Date xCreatedDate) {
		this.xCreatedDate = xCreatedDate;
	}
	
	@Column(name="X_ID_UPDATED")
	public Integer getxIdUpdatedBy() {
		return xIdUpdatedBy;
	}
	public void setxIdUpdatedBy(Integer xIdUpdatedBy) {
		this.xIdUpdatedBy = xIdUpdatedBy;
	}
	
	@ManyToOne
	@JoinColumn(name="X_ID_UPDATED", nullable=true, updatable=false, insertable=false)
	public UserModel getxUpdatedBy() {
		return xUpdatedBy;
	}
	public void setxUpdatedBy(UserModel xUpdatedBy) {
		this.xUpdatedBy = xUpdatedBy;
	}
	
	@Column(name="X_UPDATED_DATE")
	public Date getxUpdatedDate() {
		return xUpdatedDate;
	}
	public void setxUpdatedDate(Date xUpdatedDate) {
		this.xUpdatedDate = xUpdatedDate;
	}
	
	@Column(name="X_ID_DELETED")
	public Integer getxIdDeletedBy() {
		return xIdDeletedBy;
	}
	public void setxIdDeletedBy(Integer xIdDeletedBy) {
		this.xIdDeletedBy = xIdDeletedBy;
	}
	
	@ManyToOne
	@JoinColumn(name="X_ID_DELETED", nullable=true, updatable=false, insertable=false)
	public UserModel getxDeletedBy() {
		return xDeletedBy;
	}
	public void setxDeletedBy(UserModel xDeletedBy) {
		this.xDeletedBy = xDeletedBy;
	}
	
	@Column(name="X_DELETED_DATE")
	public Date getxDeletedDate() {
		return xDeletedDate;
	}
	public void setxDeletedDate(Date xDeletedDate) {
		this.xDeletedDate = xDeletedDate;
	}
	
	
	
	
}
