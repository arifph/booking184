package com.spring.booking184.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name="M_HOTEL")
public class HotelModel {
	
	private Integer idHotel;
	private String kodeHotel;
	private String namaHotel;
	private String bintang;
	private String telp;
	private String email;
	private String alamatHotel;
	private Integer feeAgen;
	private Integer isDelete;
	
	//join table LokasiModel
	private Integer idKota; //FK dari LokasiModel
	private KotaModel kotaModel;
	
	//audit Trail
	private Integer xIdCreatedBy;
	private UserModel xCreatedBy;
	private Date xCreatedDate;
		
	private Integer xIdUpdatedBy;
	private UserModel xUpdatedBy;
	private Date xUpdatedDate;
		
	private Integer xIdDeletedBy;
	private UserModel xDeletedBy;
	private Date xDeletedDate;
	//audit Trial
	
	@Id
	@Column(name="ID_HOTEL")
	@GeneratedValue(strategy=GenerationType.TABLE, generator="M_HOTEL") // utk buat nilai sequential
	@TableGenerator(name="M_HOTEL", table="M_SEQUENCE",
					pkColumnName="SEQUENCE_NAME", pkColumnValue="ID_HOTEL",
					valueColumnName="SEQUENCE_VALUE",allocationSize=1,initialValue=1)
	public Integer getIdHotel() {
		return idHotel;
	}
	public void setIdHotel(Integer idHotel) {
		this.idHotel = idHotel;
	}
	
	@Column(name="KODE_HOTEL")
	public String getKodeHotel() {
		return kodeHotel;
	}
	public void setKodeHotel(String kodeHotel) {
		this.kodeHotel = kodeHotel;
	}
	
	@Column(name="NAMA_HOTEL")
	public String getNamaHotel() {
		return namaHotel;
	}
	public void setNamaHotel(String namaHotel) {
		this.namaHotel = namaHotel;
	}
	
	@Column(name="BINTANG")
	public String getBintang() {
		return bintang;
	}
	public void setBintang(String bintang) {
		this.bintang = bintang;
	}
	@Column(name="TELP")
	public String getTelp() {
		return telp;
	}
	public void setTelp(String telp) {
		this.telp = telp;
	}
	
	@Column(name="EMAIL")
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Column(name="ALAMAT_HOTEL")
	public String getAlamatHotel() {
		return alamatHotel;
	}
	public void setAlamatHotel(String alamatHotel) {
		this.alamatHotel = alamatHotel;
	}
	
	@Column(name="FEE_AGEN")
	public Integer getFeeAgen() {
		return feeAgen;
	}
	public void setFeeAgen(Integer feeAgen) {
		this.feeAgen = feeAgen;
	}
	@Column(name="IS_DELETE")
	public Integer getIsDelete() {
		return isDelete;
	}
	public void setIsDelete(Integer isDelete) {
		this.isDelete = isDelete;
	}
	@Column(name="X_ID_CREATED")
	public Integer getxIdCreatedBy() {
		return xIdCreatedBy;
	}
	public void setxIdCreatedBy(Integer xIdCreatedBy) {
		this.xIdCreatedBy = xIdCreatedBy;
	}
	
	@ManyToOne
	@JoinColumn(name="X_ID_CREATED", nullable=true, updatable=false, insertable=false)
	public UserModel getxCreatedBy() {
		return xCreatedBy;
	}
	public void setxCreatedBy(UserModel xCreatedBy) {
		this.xCreatedBy = xCreatedBy;
	}
	@Column(name="X_CREATED_DATE")
	public Date getxCreatedDate() {
		return xCreatedDate;
	}
	public void setxCreatedDate(Date xCreatedDate) {
		this.xCreatedDate = xCreatedDate;
	}
	
	@Column(name="X_ID_UPDATED")
	public Integer getxIdUpdatedBy() {
		return xIdUpdatedBy;
	}
	public void setxIdUpdatedBy(Integer xIdUpdatedBy) {
		this.xIdUpdatedBy = xIdUpdatedBy;
	}
	
	@ManyToOne
	@JoinColumn(name="X_ID_UPDATED", nullable=true, updatable=false, insertable=false)
	public UserModel getxUpdatedBy() {
		return xUpdatedBy;
	}
	public void setxUpdatedBy(UserModel xUpdatedBy) {
		this.xUpdatedBy = xUpdatedBy;
	}
	
	@Column(name="X_UPDATED_DATE")
	public Date getxUpdatedDate() {
		return xUpdatedDate;
	}
	public void setxUpdatedDate(Date xUpdatedDate) {
		this.xUpdatedDate = xUpdatedDate;
	}
	
	@Column(name="X_ID_DELETED")
	public Integer getxIdDeletedBy() {
		return xIdDeletedBy;
	}
	public void setxIdDeletedBy(Integer xIdDeletedBy) {
		this.xIdDeletedBy = xIdDeletedBy;
	}
	
	@ManyToOne
	@JoinColumn(name="X_ID_DELETED", nullable=true, updatable=false, insertable=false)
	public UserModel getxDeletedBy() {
		return xDeletedBy;
	}
	public void setxDeletedBy(UserModel xDeletedBy) {
		this.xDeletedBy = xDeletedBy;
	}

	@Column(name="X_DELETED_DATE")
	public Date getxDeletedDate() {
		return xDeletedDate;
	}
	public void setxDeletedDate(Date xDeletedDate) {
		this.xDeletedDate = xDeletedDate;
	}
	
	@Column(name="ID_KOTA")
	public Integer getIdKota() {
		return idKota;
	}
	public void setIdKota(Integer idKota) {
		this.idKota = idKota;
	}
	
	@ManyToOne
	@JoinColumn(name="ID_KOTA", nullable=true, updatable=false, insertable=false)
	public KotaModel getKotaModel() {
		return kotaModel;
	}
	public void setKotaModel(KotaModel kotaModel) {
		this.kotaModel = kotaModel;
	}
	
	
}
