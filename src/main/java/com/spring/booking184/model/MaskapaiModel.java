package com.spring.booking184.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name="M_MASKAPAI")
public class MaskapaiModel {

	private Integer idMaskapai;
	private String kodeMaskapai;
	private String namaMaskapai;
	private String ruteMaskapai;
	private String milikMaskapai;
	private String ketMaskapai;
	private Integer feeMaskapai;
	private Integer isDelete;

	
	@Id
	@Column(name="ID_MASKAPAI")
	@GeneratedValue(strategy=GenerationType.TABLE, generator="M_MASKAPAI") // utk buat nilai sequential
	@TableGenerator(name="M_MASKAPAI", table="M_SEQUENCE",
					pkColumnName="SEQUENCE_NAME", pkColumnValue="ID_MASKAPAI",
					valueColumnName="SEQUENCE_VALUE",allocationSize=1,initialValue=1)
	
	public Integer getIdMaskapai() {
		return idMaskapai;
	}
	public void setIdMaskapai(Integer idMaskapai) {
		this.idMaskapai = idMaskapai;
	}
	
	@Column(name="KODE_MASKAPAI")
	public String getKodeMaskapai() {
		return kodeMaskapai;
	}
	public void setKodeMaskapai(String kodeMaskapai) {
		this.kodeMaskapai = kodeMaskapai;
	}
	
	@Column(name="NAMA_MASKAPAI")
	public String getNamaMaskapai() {
		return namaMaskapai;
	}
	public void setNamaMaskapai(String namaMaskapai) {
		this.namaMaskapai = namaMaskapai;
	}
	
	@Column(name="RUTE_MASKAPAI")
	public String getRuteMaskapai() {
		return ruteMaskapai;
	}
	public void setRuteMaskapai(String ruteMaskapai) {
		this.ruteMaskapai = ruteMaskapai;
	}
	
	@Column(name="MILIK_MASKAPAI")
	public String getMilikMaskapai() {
		return milikMaskapai;
	}
	public void setMilikMaskapai(String milikMaskapai) {
		this.milikMaskapai = milikMaskapai;
	}
	
	@Column(name="KET_MASKAPAI")
	public String getKetMaskapai() {
		return ketMaskapai;
	}
	public void setKetMaskapai(String ketMaskapai) {
		this.ketMaskapai = ketMaskapai;
	}
	
	@Column(name="FEE_MASKAPAI")
	public Integer getFeeMaskapai() {
		return feeMaskapai;
	}
	public void setFeeMaskapai(Integer feeMaskapai) {
		this.feeMaskapai = feeMaskapai;
	}
	
	@Column(name="IS_DELETE")
	public Integer getIsDelete() {
		return isDelete;
	}
	public void setIsDelete(Integer isDelete) {
		this.isDelete = isDelete;
	}
	
	
}
