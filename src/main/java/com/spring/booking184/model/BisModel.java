package com.spring.booking184.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name="M_BIS")
public class BisModel {
	
	private Integer idBis;
	private String namaBis;
	private String kodeBis;
	private String transitBis;
	private Integer teleponBis;
	private String emailBis;
	private String alamatBis;
	private String feeAgenBis;
	
	private Integer isDeleted;
	
	private Integer xIdCreatedBy;
	private UserModel xCreatedBy;
	private Date xCreatedDate;
	
	private Integer xIdUpdatedBy;
	private UserModel xUpdatedBy;
	private Date xUpdatedDate;
	
	private Integer xIdDeletedBy;
	private UserModel xDeletedBy;
	private Date xDeletedDate;
	
	@Id
	@Column(name="ID_BIS")
	@GeneratedValue(strategy=GenerationType.TABLE, generator="M_BIS") // utk buat nilai sequential
	@TableGenerator(name="M_BIS", table="M_SEQUENCE",
					pkColumnName="SEQUENCE_NAME", pkColumnValue="ID_BIS",
					valueColumnName="SEQUENCE_VALUE",allocationSize=1,initialValue=1)
	public Integer getIdBis() {
		return idBis;
	}
	public void setIdBis(Integer idBis) {
		this.idBis = idBis;
	}
	
	@Column(name="NAMA_BIS")
	public String getNamaBis() {
		return namaBis;
	}
	public void setNamaBis(String namaBis) {
		this.namaBis = namaBis;
	}
	
	@Column(name="KODE_BIS")
	public String getKodeBis() {
		return kodeBis;
	}
	public void setKodeBis(String kodeBis) {
		this.kodeBis = kodeBis;
	}
	
	@Column(name="TRANSIT_BIS")
	public String getTransitBis() {
		return transitBis;
	}
	public void setTransitBis(String transitBis) {
		this.transitBis = transitBis;
	}
	
	@Column(name="TELEPON_BUS")
	public Integer getTeleponBis() {
		return teleponBis;
	}
	public void setTeleponBis(Integer teleponBis) {
		this.teleponBis = teleponBis;
	}
	
	@Column(name="EMAIL_BIS")
	public String getEmailBis() {
		return emailBis;
	}
	public void setEmailBis(String emailBis) {
		this.emailBis = emailBis;
	}
	
	@Column(name="ALAMAT_BIS")
	public String getAlamatBis() {
		return alamatBis;
	}
	public void setAlamatBis(String alamatBis) {
		this.alamatBis = alamatBis;
	}
	
	@Column(name="FEE_AGEN_BIS")
	public String getFeeAgenBis() {
		return feeAgenBis;
	}
	public void setFeeAgenBis(String feeAgenBis) {
		this.feeAgenBis = feeAgenBis;
	}
	
	
	@Column(name="IS_DELETED")
	public Integer getIsDeleted() {
		return isDeleted;
	}
	public void setIsDeleted(Integer isDeleted) {
		this.isDeleted = isDeleted;
	}
	
	
	@Column(name="X_ID_CREATED")
	public Integer getxIdCreatedBy() {
		return xIdCreatedBy;
	}
	public void setxIdCreatedBy(Integer xIdCreatedBy) {
		this.xIdCreatedBy = xIdCreatedBy;
	}
	@ManyToOne
	@JoinColumn(name="X_ID_CREATED", nullable=true, updatable=false, insertable=false)
	public UserModel getxCreatedBy() {
		return xCreatedBy;
	}
	public void setxCreatedBy(UserModel xCreatedBy) {
		this.xCreatedBy = xCreatedBy;
	}
	@Column(name="X_DATE_CREATED")
	public Date getxCreatedDate() {
		return xCreatedDate;
	}
	public void setxCreatedDate(Date xCreatedDate) {
		this.xCreatedDate = xCreatedDate;
	}
	
	@Column(name="X_ID_UPDATED")
	public Integer getxIdUpdatedBy() {
		return xIdUpdatedBy;
	}
	public void setxIdUpdatedBy(Integer xIdUpdatedBy) {
		this.xIdUpdatedBy = xIdUpdatedBy;
	}
	@ManyToOne
	@JoinColumn(name="X_ID_UPDATED", nullable=true, updatable=false, insertable=false)
	public UserModel getxUpdatedBy() {
		return xUpdatedBy;
	}
	public void setxUpdatedBy(UserModel xUpdatedBy) {
		this.xUpdatedBy = xUpdatedBy;
	}
	@Column(name="X_DATE_UPDATED")
	public Date getxUpdatedDate() {
		return xUpdatedDate;
	}
	public void setxUpdatedDate(Date xUpdatedDate) {
		this.xUpdatedDate = xUpdatedDate;
	}
	
	@Column(name="X_ID_DELETED")
	public Integer getxIdDeletedBy() {
		return xIdDeletedBy;
	}
	public void setxIdDeletedBy(Integer xIdDeletedBy) {
		this.xIdDeletedBy = xIdDeletedBy;
	}
	@ManyToOne
	@JoinColumn(name="X_ID_DELETED", nullable=true, updatable=false, insertable=false)
	public UserModel getxDeletedBy() {
		return xDeletedBy;
	}
	public void setxDeletedBy(UserModel xDeletedBy) {
		this.xDeletedBy = xDeletedBy;
	}
	@Column(name="X_DATE_DELETED")
	public Date getxDeletedDate() {
		return xDeletedDate;
	}
	public void setxDeletedDate(Date xDeletedDate) {
		this.xDeletedDate = xDeletedDate;
	}

	
	
}
