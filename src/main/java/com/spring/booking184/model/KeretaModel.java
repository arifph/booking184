package com.spring.booking184.model;

//import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name="M_KERETA")
public class KeretaModel {
	
	private Integer idKereta; // PK
	private String kodeKereta;
	private String namaKereta;
	private String jenisKereta;
	private String ketKereta;
	private Integer feeKereta;
	private Integer isDelete;
	
	/*//join table LokasiModel
	private Integer idLokasi; // FK dari LokasiModel
	private LokasiModel lokasiModel;*/
	
/*	// Audit trail
	private Integer xIdCreatedBy;
	private UserModel xCreatedBy;
	private Date xCreatedDate;
	
	private Integer xIdUpdatedBy;
	private UserModel xUpdatedBy;
	private Date xUpdatedDate;
	
	private Integer xIdDeletedBy;
	private UserModel xDeletedBy;
	private Date xDeletedDate;
	// Audit trail
*/	
	/*@Column(name="ID_LOKASI")
	public Integer getIdLokasi() {
		return idLokasi;
	}
	public void setIdLokasi(Integer idLokasi) {
		this.idLokasi = idLokasi;
	}
	
	@ManyToOne //Kenapa many-to-one? Karena BANYAK kereta hanya bisa memiliki SATU lokasi
	@JoinColumn(name="ID_LOKASI", nullable=true, updatable=false, insertable=false)
	public LokasiModel getLokasiModel() {
		return lokasiModel;
	}
	public void setLokasiModel(LokasiModel lokasiModel) {
		this.lokasiModel = lokasiModel;
	}*/
	
	@Id //untuk primary key
	@Column(name="ID_KERETA")
	@GeneratedValue(strategy=GenerationType.TABLE, generator="M_KERETA") // utk autoincrement
	@TableGenerator(name="M_KERETA", table="M_SEQUENCE",
					pkColumnName="SEQUENCE_NAME", pkColumnValue="ID_KERETA",
					valueColumnName="SEQUENCE_VALUE",allocationSize=1,initialValue=1)
	public Integer getIdKereta() {
		return idKereta;
	}
	public void setIdKereta(Integer idKereta) {
		this.idKereta = idKereta;
	}
	@Column(name="KODE_KERETA")
	public String getKodeKereta() {
		return kodeKereta;
	}
	public void setKodeKereta(String kodeKereta) {
		this.kodeKereta = kodeKereta;
	}
	@Column(name="NAMA_KERETA")
	public String getNamaKereta() {
		return namaKereta;
	}
	public void setNamaKereta(String namaKereta) {
		this.namaKereta = namaKereta;
	}
	@Column(name="JENIS_KERETA")	
	public String getJenisKereta() {
		return jenisKereta;
	}
	public void setJenisKereta(String jenisKereta) {
		this.jenisKereta = jenisKereta;
	}
	@Column(name="KET_KERETA")
	public String getKetKereta() {
		return ketKereta;
	}
	public void setKetKereta(String ketKereta) {
		this.ketKereta = ketKereta;
	}
	@Column(name="FEE_KERETA")
	public Integer getFeeKereta() {
		return feeKereta;
	}
	public void setFeeKereta(Integer feeKereta) {
		this.feeKereta = feeKereta;
	}
	@Column(name="IS_DELETE")
	public Integer getIsDelete() {
		return isDelete;
	}
	public void setIsDelete(Integer isDelete) {
		this.isDelete = isDelete;
	}
	
	/*@Column(name="X_ID_CREATED")
	public Integer getxIdCreatedBy() {
		return xIdCreatedBy;
	}
	public void setxIdCreatedBy(Integer xIdCreatedBy) {
		this.xIdCreatedBy = xIdCreatedBy;
	}
	
	@ManyToOne
	@JoinColumn(name="X_ID_CREATED", nullable=true, updatable=false, insertable=false)
	public UserModel getxCreatedBy() {
		return xCreatedBy;
	}
	public void setxCreatedBy(UserModel xCreatedBy) {
		this.xCreatedBy = xCreatedBy;
	}
	
	@Column(name="X_CREATED_DATE")
	public Date getxCreatedDate() {
		return xCreatedDate;
	}
	public void setxCreatedDate(Date xCreatedDate) {
		this.xCreatedDate = xCreatedDate;
	}
	
	@Column(name="X_ID_UPDATED")
	public Integer getxIdUpdatedBy() {
		return xIdUpdatedBy;
	}
	public void setxIdUpdatedBy(Integer xIdUpdatedBy) {
		this.xIdUpdatedBy = xIdUpdatedBy;
	}
	
	@ManyToOne
	@JoinColumn(name="X_ID_UPDATED", nullable=true, updatable=false, insertable=false)
	public UserModel getxUpdatedBy() {
		return xUpdatedBy;
	}
	public void setxUpdatedBy(UserModel xUpdatedBy) {
		this.xUpdatedBy = xUpdatedBy;
	}
	
	@Column(name="X_UPDATED_DATE")
	public Date getxUpdatedDate() {
		return xUpdatedDate;
	}
	public void setxUpdatedDate(Date xUpdatedDate) {
		this.xUpdatedDate = xUpdatedDate;
	}
	
	@Column(name="X_ID_DELETED")
	public Integer getxIdDeletedBy() {
		return xIdDeletedBy;
	}
	public void setxIdDeletedBy(Integer xIdDeletedBy) {
		this.xIdDeletedBy = xIdDeletedBy;
	}
	
	@ManyToOne
	@JoinColumn(name="X_ID_DELETED", nullable=true, updatable=false, insertable=false)
	public UserModel getxDeletedBy() {
		return xDeletedBy;
	}
	public void setxDeletedBy(UserModel xDeletedBy) {
		this.xDeletedBy = xDeletedBy;
	}
	
	@Column(name="X_DELETED_DATE")
	public Date getxDeletedDate() {
		return xDeletedDate;
	}
	public void setxDeletedDate(Date xDeletedDate) {
		this.xDeletedDate = xDeletedDate;
	}*/
	
	
}
