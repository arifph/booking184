package com.spring.booking184.controller;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.spring.booking184.model.FakultasModel;
import com.spring.booking184.model.KotaModel;
import com.spring.booking184.model.LokasiModel;
import com.spring.booking184.model.TerminalModel;
import com.spring.booking184.service.FakultasService;
import com.spring.booking184.service.KotaService;
import com.spring.booking184.service.TerminalService;

@Controller
public class TerminalController extends UserController {
	
	@Autowired
	private TerminalService terminalService;
	
	@Autowired
	private KotaService kotaService;

	@RequestMapping(value ="terminal") //url action
	public String terminal() { //method
		String jsp ="terminal/terminal"; //target url atau halaman
		return jsp;
	}
	
	@RequestMapping(value ="terminal/cruds/add")
	public String terminalAdd(Model model) {
		
		//tampilkan lokasi select option
		this.kotaList(model);
		
		String jsp ="terminal/cruds/add";
		return jsp;
	}
	
	@RequestMapping(value ="terminal/cruds/create") 
	public String fakultasCreate(HttpServletRequest request, Model model) { 
		
		//get nilai dari variabel name di jsp
		String kodeTerminal = request.getParameter("kodeTerminal");
		String namaTerminal = request.getParameter("namaTerminal");
		Integer kota 		= Integer.valueOf(request.getParameter("kota"));
		String noTelepon	= request.getParameter("noTelepon");
		String email		= request.getParameter("email");
		String alamat		= request.getParameter("alamat");
		
		//set join table
		//Integer idKota = Integer.valueOf(request.getParameter("idKota"));
		
		// set nilai ke variabel modelnya
		TerminalModel terminalModel = new TerminalModel();
		terminalModel.setKodeTerminal(kodeTerminal);
		terminalModel.setNamaTerminal(namaTerminal);
		terminalModel.setIdKota(kota);
		terminalModel.setNoTelpon(noTelepon);
		terminalModel.setEmail(email);
		terminalModel.setAlamat(alamat);
		
		terminalModel.setIsDelete(0);
		
		//panggil method untuk cek kode dan nama
		//Integer jumlahDataFakultas = this.cekKodeFakultas(kodeFakultas);
		Integer jumlahDataTerminal = this.cekNamaTerminal(namaTerminal); //kalo disuruh ganti nama tinggal aktifin yg ini
		
		//Save audit trail untuk Created
		Integer xIdCreatedBy = this.userSearch().getIdUser();
		terminalModel.setxIdCreatedBy(xIdCreatedBy);
		terminalModel.setxCreatedDate(new Date());
		
		if (jumlahDataTerminal > 0) {
			//tidak bisa simpan
			model.addAttribute("jumlahDataTerminal", jumlahDataTerminal); // Lempar ke jsp jumlah datanya
			//model.addAttribute("jumlahDataFakultas2", jumlahDataFakultas2);
			model.addAttribute("namaTerminal", namaTerminal);
		} else {
			//simpan data
			this.terminalService.create(terminalModel);
		}
		
		String jsp ="terminal/terminal"; //abis save balik ke halaman utama
		return jsp;
	}
	
	@RequestMapping(value ="terminal/cruds/list") //url action
	public String terminalList(Model model) { //method
		List<TerminalModel> terminalModelList = new ArrayList<TerminalModel>();
		terminalModelList = this.terminalService.search();
		model.addAttribute("terminalModelList", terminalModelList);
		String jsp ="terminal/cruds/list"; //target url atau halaman
		return jsp;
	}
	
	@RequestMapping(value ="terminal/cruds/detail") //url action
	public String terminalDetail(HttpServletRequest request, Model model) { //method
		Integer idTerminal = Integer.valueOf(request.getParameter("idTerminal"));
		
		TerminalModel terminalModel = new TerminalModel();
		terminalModel = this.terminalService.searchId(idTerminal);
		
		model.addAttribute("terminalModel", terminalModel);
		
		//tampilkan lokasi select option
		this.kotaList(model);
		/*
		 * String var1= "JAKARTA"; model.addAttribute("var2", var1); String var2 =
		 * "ES JERUK"; model.addAttribute("minum", var2); String var3 = "PIZZA";
		 * model.addAttribute("makan", var3);
		 */
		
		String jsp ="terminal/cruds/detail"; //target url atau halaman
		return jsp;
	}
	
	@RequestMapping(value ="terminal/cruds/edit") //url action
	public String terminalEdit(HttpServletRequest request, Model model) { //method
		Integer idTerminal = Integer.valueOf(request.getParameter("idTerminal"));
		
		TerminalModel terminalModel = new TerminalModel();
		terminalModel = this.terminalService.searchId(idTerminal);
		
		model.addAttribute("terminalModel", terminalModel);
		//tampilkan lokasi select option
		this.kotaList(model);
		/*
		 * String var1= "JAKARTA"; model.addAttribute("var2", var1); String var2 =
		 * "ES JERUK"; model.addAttribute("minum", var2); String var3 = "PIZZA";
		 * model.addAttribute("makan", var3);
		 */
		
		String jsp ="terminal/cruds/edit"; //target url atau halaman
		return jsp;
	}
	
	@RequestMapping(value ="terminal/cruds/update") 
	public String terminalUpdate(HttpServletRequest request) { 
		
		//get nilai dari variabel name di jsp
		Integer idTerminal  = Integer.valueOf(request.getParameter("idTerminal"));
		String kodeTerminal = request.getParameter("kodeTerminal");
		String namaTerminal = request.getParameter("namaTerminal");
		//String kota			= request.getParameter("kota");
		String noTelpon		= request.getParameter("noTelpon");
		String email		= request.getParameter("email");
		String alamat		= request.getParameter("alamat");
		//set join table
		Integer kota = Integer.valueOf(request.getParameter("kota"));
		
		/*
		 * String varBackEnd = request.getParameter("varJSP");
		 * System.out.println(varBackEnd);
		 */
		
		// set nilai ke variabel modelnya
		TerminalModel terminalModel = new TerminalModel();
		
		terminalModel = this.terminalService.searchId(idTerminal);
		terminalModel.setAlamat(alamat);
		terminalModel.setEmail(email);
		terminalModel.setIdKota(kota);
		terminalModel.setKodeTerminal(kodeTerminal);
		terminalModel.setNamaTerminal(namaTerminal);
		terminalModel.setNoTelpon(noTelpon);
		
		
		//set join table
		//terminalModel.setIdKota(idKota);
		
		
		//Save audit trail untuk Created
		Integer xIdUpdatedBy = this.userSearch().getIdUser();
		terminalModel.setxIdUpdatedBy(xIdUpdatedBy);
		terminalModel.setxUpdatedDate(new Date());
				
		
		//simpan data
		this.terminalService.update(terminalModel);
		String jsp ="terminal/terminal"; //abis save balik ke halaman utama
		return jsp;
	}
	
	@RequestMapping(value ="terminal/cruds/remove") //url action
	public String terminalRemove(HttpServletRequest request, Model model) { //method
		Integer idTerminal = Integer.valueOf(request.getParameter("idTerminal"));
		
		TerminalModel terminalModel = new TerminalModel();
		terminalModel = this.terminalService.searchId(idTerminal);
		
		model.addAttribute("terminalModel", terminalModel);
		//tampilkan lokasi select option
		this.kotaList(model);
		
		String jsp ="terminal/cruds/remove"; //target url atau halaman
		return jsp;
	}
	
	@RequestMapping(value ="terminal/cruds/delete") 
	public String terminalDelete(HttpServletRequest request) { 
		
		//get nilai dari variabel name di jsp
		Integer idTerminal = Integer.valueOf(request.getParameter("idTerminal"));
		
		// set nilai ke variabel modelnya
		TerminalModel terminalModel = new TerminalModel();
		
		terminalModel = this.terminalService.searchId(idTerminal);
	
		terminalModel.setIsDelete(1); //status 1 artinya didelete
		
		//Save audit trail untuk Created
		Integer xIdDeletedBy = this.userSearch().getIdUser();
		terminalModel.setxIdDeletedBy(xIdDeletedBy);
		terminalModel.setxDeletedDate(new Date());
		
		//simpan data
		this.terminalService.deleteTemporary(terminalModel);
		String jsp ="terminal/terminal"; //abis save balik ke halaman utama
		return jsp;
	}
	
//	@RequestMapping(value="fakultas/cruds/search/nama")
//	public String fakultasSearchNama(HttpServletRequest request, Model model) {
//		String namaFakultas = request.getParameter("namaFakultasKey");
//		List<FakultasModel> fakultasModelList = new ArrayList<FakultasModel>();
//		fakultasModelList = this.fakultasService.searchNama(namaFakultas);
//		
//		model.addAttribute("fakultasModelList", fakultasModelList); // menampilkan ke jsp
//		String jsp ="fakultas/cruds/list";
//		return jsp;
//	}
	
//	@RequestMapping(value="fakultas/cruds/search/kode")
//	public String fakultasSearchKode(HttpServletRequest request, Model model) {
//		String kodeFakultas = request.getParameter("kodeFakultasKey");
//		List<FakultasModel> fakultasModelList = new ArrayList<FakultasModel>();
//		fakultasModelList = this.fakultasService.searchKode(kodeFakultas);
//		
//		model.addAttribute("fakultasModelList", fakultasModelList); // menampilkan ke jsp
//		String jsp ="fakultas/cruds/list";
//		return jsp;
//	}
	
//	@RequestMapping(value="fakultas/cruds/search")
//	public String fakultasSearch(HttpServletRequest request, Model model) {
//		String kodeFakultas = request.getParameter("keyword");
//		String namaFakultas = request.getParameter("keyword");
//		List<FakultasModel> fakultasModelList = new ArrayList<FakultasModel>();
//		fakultasModelList = this.fakultasService.searchKodeOrNama(kodeFakultas, namaFakultas);
//		
//		model.addAttribute("fakultasModelList", fakultasModelList); // menampilkan ke jsp
//		String jsp ="fakultas/cruds/list";
//		return jsp;
//	}
	
	public Integer cekKodeTerminal(String kodeTerminal) {
		List<TerminalModel> terminalModelList = new ArrayList<TerminalModel>();
		terminalModelList = this.terminalService.searchKodeEqual(kodeTerminal);
		Integer jumlahDataTerminal=0;
		if (terminalModelList == null) {
			jumlahDataTerminal=0;
		} else {
			jumlahDataTerminal = terminalModelList.size();
		}
		return jumlahDataTerminal;
	}
	
	public Integer cekNamaTerminal(String namaTerminal) {
		List<TerminalModel> terminalModelList = new ArrayList<TerminalModel>(); //object
		terminalModelList = this.terminalService.searchNamaEqual(namaTerminal); 
		
		Integer jumlahDataTerminal=0;
		
		if (terminalModelList == null) {
			jumlahDataTerminal=0;
		} else {
			jumlahDataTerminal=terminalModelList.size();
		}
		return jumlahDataTerminal; 
	}
	//fungsi generatekode
//	public String generateKodeFakultas() {
//		String kodeFakultasAuto = "FK";
//		
//		List<FakultasModel> fakultasModelList = new ArrayList<FakultasModel>(); 
//		fakultasModelList = this.fakultasService.search();
//		Integer jumlahDataFakultas=0;
//		if (fakultasModelList == null) {
//			jumlahDataFakultas=0;
//		} else {
//			jumlahDataFakultas=fakultasModelList.size();
//		}
//		
//		kodeFakultasAuto = kodeFakultasAuto + "000" +(jumlahDataFakultas+1);
//		
//		return kodeFakultasAuto;
//	}
	
	public void kotaList(Model model) {
		List<KotaModel> kotaModelList = new ArrayList<>(); 
		kotaModelList = this.kotaService.search();
		model.addAttribute("kotaModelList", kotaModelList);
	}
}
