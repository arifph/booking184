package com.spring.booking184.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.spring.booking184.model.FakultasModel;
import com.spring.booking184.model.JurusanModel;
import com.spring.booking184.model.MaskapaiModel;
import com.spring.booking184.service.MaskapaiService;

@Controller
public class MaskapaiController {

	@Autowired
	private MaskapaiService maskapaiService;
	
	
	@RequestMapping(value = "maskapai")// url action
	public String maskapai() {// method
		
		String jsp = "maskapai/maskapai"; //target atau halaman
		return jsp;
	}
	
	//model untuk lempar ke jsp
		@RequestMapping(value = "maskapai/cruds/add")
		public String maskapaiAdd(Model model) {
			String kodeMaskapaiAuto = this.generateKodeMaskapai();
			model.addAttribute("kodeMaskapaiAuto", kodeMaskapaiAuto);
						
			String jsp = "maskapai/cruds/add"; 
			return jsp;
		}
	
		@RequestMapping(value = "maskapai/cruds/create")
		public String maskapaiCreate(HttpServletRequest request, Model model) {
			
			//get nilai dari variable di jsp
			String kodeMaskapai = request.getParameter("kodeMaskapai");
			String namaMaskapai = request.getParameter("namaMaskapai");
			String ruteMaskapai = request.getParameter("ruteMaskapai");
			String milikMaskapai = request.getParameter("milikMaskapai");
			String ketMaskapai = request.getParameter("ketMaskapai");
			Integer feeMaskapai = Integer.valueOf(request.getParameter("feeMaskapai"));
			Integer jumlahMaskapaiByKode = this.cekKodeMaskapai(kodeMaskapai);
			Integer jumlahMaskapaiByNama = this.cekNamaMaskapai(namaMaskapai);
			
			//set nilai ke variable di modelnya
			MaskapaiModel maskapaiModel = new MaskapaiModel();
			maskapaiModel.setKodeMaskapai(kodeMaskapai);
			maskapaiModel.setNamaMaskapai(namaMaskapai);
			maskapaiModel.setRuteMaskapai(ruteMaskapai);
			maskapaiModel.setMilikMaskapai(milikMaskapai);
			maskapaiModel.setKetMaskapai(ketMaskapai);
			maskapaiModel.setFeeMaskapai(feeMaskapai);
			maskapaiModel.setIsDelete(0);
			
			if (jumlahMaskapaiByKode > 0 || jumlahMaskapaiByNama > 0) {
				model.addAttribute("jumlahMaskapaiByKode", jumlahMaskapaiByKode);
				model.addAttribute("jumlahMaskapaiByNama", jumlahMaskapaiByNama);
				model.addAttribute("kodeMaskapai", kodeMaskapai);
				model.addAttribute("namaMaskapai", namaMaskapai);
			} else {			
				//simpan data
				this.maskapaiService.create(maskapaiModel);
			}
			
			String jsp = "maskapai/maskapai"; 
			return jsp;
		}
		
		
		//menampilkan list data
		@RequestMapping(value = "maskapai/cruds/list")
		public String maskapaiList(Model model) {
			List<MaskapaiModel> maskapaiModelList = new ArrayList<MaskapaiModel>();
			//String kodeRole = this.userSearch().getRoleModel().getKodeRole();
			
			
			maskapaiModelList = this.maskapaiService.search();
			model.addAttribute("maskapaiModelList",maskapaiModelList);
			
			String jsp = "maskapai/cruds/list"; 
			return jsp;
		}
		
		//controller mengatur detail
		@RequestMapping(value = "maskapai/cruds/detail")
		public String maskapaiDetail(HttpServletRequest request, Model model) {
			Integer idMaskapai = Integer.valueOf(request.getParameter("idMaskapai"));
			
			MaskapaiModel maskapaiModel = new MaskapaiModel();
			maskapaiModel = this.maskapaiService.searchId(idMaskapai);
			
			model.addAttribute("maskapaiModel", maskapaiModel);
			
			String jsp = "maskapai/cruds/detail"; 
			return jsp;
		}
		
		//untuk mengatur edit
		@RequestMapping(value = "maskapai/cruds/edit")
		public String maskapaiEdit(HttpServletRequest request, Model model) {
			Integer idMaskapai = Integer.valueOf(request.getParameter("idMaskapai"));
			
			MaskapaiModel maskapaiModel = new MaskapaiModel();
			maskapaiModel = this.maskapaiService.searchId(idMaskapai);
			model.addAttribute("maskapaiModel", maskapaiModel);
			
			String jsp = "maskapai/cruds/edit"; 
			return jsp;
		}
		
		//unuk mengatur update
		@RequestMapping(value = "maskapai/cruds/update")
		public String maskapaiUpdate(HttpServletRequest request) {
			
			//get nilai dari variable di jsp
			Integer idMaskapai = Integer.valueOf(request.getParameter("idMaskapai"));
			String kodeMaskapai = request.getParameter("kodeMaskapai");
			String namaMaskapai = request.getParameter("namaMaskapai");
			String ruteMaskapai = request.getParameter("ruteMaskapai");
			String milikMaskapai = request.getParameter("milikMaskapai");
			String ketMaskapai = request.getParameter("ketMaskapai");
			Integer feeMaskapai = Integer.valueOf(request.getParameter("feeMaskapai"));
			
			
			
			//set nilai ke variable di modelnya
			MaskapaiModel maskapaiModel = new MaskapaiModel();
			
			maskapaiModel =this.maskapaiService.searchId(idMaskapai);
			
			maskapaiModel.setKodeMaskapai(kodeMaskapai);
			maskapaiModel.setNamaMaskapai(namaMaskapai);
			maskapaiModel.setRuteMaskapai(ruteMaskapai);
			maskapaiModel.setMilikMaskapai(milikMaskapai);
			maskapaiModel.setKetMaskapai(ketMaskapai);
			maskapaiModel.setFeeMaskapai(feeMaskapai);
			
			//save audit trail
//			Integer xIdUpdatedBy = this.userSearch().getIdUser();
//			fakultasModel.setxIdUpdatedBy(xIdUpdatedBy);
//			fakultasModel.setxUpdatedDate(new Date());
			
			//simpan data
			this.maskapaiService.update(maskapaiModel);
			
			String jsp = "maskapai/maskapai"; 
			return jsp;
		}
		
		//untuk mengatur remove
		@RequestMapping(value = "maskapai/cruds/remove")
		public String maskapaiRemove(HttpServletRequest request, Model model) {
			Integer idMaskapai = Integer.valueOf(request.getParameter("idMaskapai"));
			
			MaskapaiModel maskapaiModel = new MaskapaiModel();
			maskapaiModel = this.maskapaiService.searchId(idMaskapai);
			

			model.addAttribute("maskapaiModel", maskapaiModel);

			String jsp = "maskapai/cruds/remove"; 
			return jsp;
		}
		
		//untuk mengatur delete
		@RequestMapping(value = "maskapai/cruds/delete")
		public String maskapaiDelete(HttpServletRequest request) {
			
			//get nilai dari variable di jsp
			Integer idMaskapai = Integer.valueOf(request.getParameter("idMaskapai"));
			
			//set nilai ke variable di modelnya
			MaskapaiModel maskapaiModel = new MaskapaiModel();
			
			maskapaiModel = this.maskapaiService.searchId(idMaskapai);
			
			maskapaiModel.setIsDelete(1); //status 1 artinya didelete
			
			//save audit trail untuk deleted
//			Integer xIdDeletedBy = this.userSearch().getIdUser();
//			fakultasModel.setxIdDeletedBy(xIdDeletedBy);
//			fakultasModel.setxDeletedDate(new Date());
//			
//			this.fakultasService.deleteTemporary(fakultasModel);

			this.maskapaiService.deleteTemporary(maskapaiModel);
			
			String jsp = "maskapai/maskapai"; 
			return jsp;
		}
		
		public String generateKodeMaskapai() {
			String kodeMaskapaiAuto = "MAS";
			
			List<MaskapaiModel> maskapaiModelList = new ArrayList<MaskapaiModel>(); //kode butuh list
			//String kodeRole = this.userSearch().getRoleModel().getKodeRole();
			
			maskapaiModelList = this.maskapaiService.search();
			Integer jumlahDataMaskapai = 0;
			
			if (maskapaiModelList == null) {
				jumlahDataMaskapai = 0;
			} else {
				jumlahDataMaskapai = maskapaiModelList.size();
			}
			
			kodeMaskapaiAuto = kodeMaskapaiAuto +"00"+(jumlahDataMaskapai+1);
			
			return kodeMaskapaiAuto;
		}
		
		public Integer cekKodeMaskapai(String kodeMaskapai) {
			List<MaskapaiModel> maskapaiModelList = new ArrayList<MaskapaiModel>(); //kode butuh list
			maskapaiModelList = this.maskapaiService.searchKodeEqual(kodeMaskapai);
			Integer jumlahDataMaskapai = 0;
			if (maskapaiModelList == null) {
				jumlahDataMaskapai = 0;
			} else {
				jumlahDataMaskapai = maskapaiModelList.size();
			}
			
			return jumlahDataMaskapai;
		}
		
		public Integer cekNamaMaskapai(String namaMaskapai) {
			List<MaskapaiModel> maskapaiModelList = new ArrayList<MaskapaiModel>(); //kode butuh list
			maskapaiModelList = this.maskapaiService.searchNamaEqual(namaMaskapai);
			Integer jumlahDataMaskapai = 0;
			
			if (maskapaiModelList == null) {
				jumlahDataMaskapai = 0;
			} else {
				jumlahDataMaskapai = maskapaiModelList.size();
			}
			return jumlahDataMaskapai;
		}
		
		
		
	
}
