package com.spring.booking184.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.spring.booking184.model.KotaModel;
import com.spring.booking184.model.StasiunModel;
import com.spring.booking184.service.KotaService;
import com.spring.booking184.service.StasiunService;

@Controller
public class StasiunController extends UserController {
	
	@Autowired
	private StasiunService stasiunService;
	
	@Autowired
	private KotaService kotaService;

	@RequestMapping(value="/stasiun")
	public String stasiun() {
		String jsp = "stasiun/stasiun";
		return jsp;
	}
	
	@RequestMapping(value="stasiun/cruds/list")
	public String stasiunList(Model model) {
		List<StasiunModel> stasiunModelList;
		String kodeRole = this.userSearch().getRoleModel().getKodeRole();
		
		if(kodeRole.equals("ROLE_ADMIN")) {
			stasiunModelList = this.stasiunService.searchWithDeleted();
		} else {
			stasiunModelList = this.stasiunService.search();
		}
		
		model.addAttribute("stasiunModelList", stasiunModelList);
		
		String jsp = "stasiun/cruds/list";
		return jsp;
	}
	
	@RequestMapping(value="stasiun/cruds/add")
	public String stasiunAdd(Model model) {
		List<KotaModel> kotaModelList = kotaService.search();
		model.addAttribute("kotaModelList", kotaModelList);
		String jsp = "stasiun/cruds/add";
		return jsp;
	}
	
	@RequestMapping(value="stasiun/cruds/save")
	public String stasiunSave(HttpServletRequest request) {
		String kodeStasiun = request.getParameter("kodeStasiun");
		String namaStasiun = request.getParameter("namaStasiun");
		Integer idKota = Integer.valueOf(request.getParameter("idKota"));
		String telepon = request.getParameter("telepon");
		String email = request.getParameter("email");
		String alamat = request.getParameter("alamat");
		Integer xIdCreatedBy = this.userSearch().getIdUser();
		Date xCreatedDate = new Date();
		
		StasiunModel stasiunModel = new StasiunModel();
		stasiunModel.setKodeStasiun(kodeStasiun);
		stasiunModel.setNamaStasiun(namaStasiun);
		stasiunModel.setAlamat(alamat);
		stasiunModel.setEmail(email);
		stasiunModel.setIdKota(idKota);
		stasiunModel.setTelepon(telepon);
		stasiunModel.setIsDeleted(0);
		stasiunModel.setxIdCreatedBy(xIdCreatedBy);
		stasiunModel.setxCreatedDate(xCreatedDate);
		
		this.stasiunService.save(stasiunModel);
		
		String jsp = "stasiun/stasiun";
		return jsp;
	}
	
	@RequestMapping(value="stasiun/cruds/edit")
	public String stasiunEdit(HttpServletRequest request, Model model) {
		Integer idStasiun = Integer.valueOf(request.getParameter("idStasiun"));
		StasiunModel stasiunModel = stasiunService.searchByIdStasiun(idStasiun);
		List<KotaModel> kotaModelList = kotaService.search();
		model.addAttribute("kotaModelList", kotaModelList);
		model.addAttribute("stasiunModel", stasiunModel);
		
		String jsp = "stasiun/cruds/edit";
		return jsp;
	}
	
	@RequestMapping(value="stasiun/cruds/update")
	public String stasiunUpdate(HttpServletRequest request) {
		Integer idStasiun = Integer.valueOf(request.getParameter("idStasiun"));		
		String kodeStasiun = request.getParameter("kodeStasiun");
		String namaStasiun = request.getParameter("namaStasiun");
		Integer idKota = Integer.valueOf(request.getParameter("idKota"));
		String telepon = request.getParameter("telepon");
		String email = request.getParameter("email");
		String alamat = request.getParameter("alamat");
		Integer xIdUpdatedBy = this.userSearch().getIdUser();
		Date xUpdatedDate = new Date();
		
		StasiunModel stasiunModel = stasiunService.searchByIdStasiun(idStasiun);
		stasiunModel.setKodeStasiun(kodeStasiun);
		stasiunModel.setNamaStasiun(namaStasiun);
		stasiunModel.setAlamat(alamat);
		stasiunModel.setEmail(email);
		stasiunModel.setIdKota(idKota);
		stasiunModel.setTelepon(telepon);
		stasiunModel.setxIdUpdatedBy(xIdUpdatedBy);
		stasiunModel.setxUpdatedDate(xUpdatedDate);
		
		this.stasiunService.update(stasiunModel);
		
		String jsp = "stasiun/stasiun";
		return jsp;
	}
	
	@RequestMapping(value="stasiun/cruds/remove")
	public String stasiunRemove(HttpServletRequest request, Model model) {
		Integer idStasiun = Integer.valueOf(request.getParameter("idStasiun"));
		StasiunModel stasiunModel = stasiunService.searchByIdStasiun(idStasiun);
		List<KotaModel> kotaModelList = kotaService.search();
		model.addAttribute("kotaModelList", kotaModelList);
		model.addAttribute("stasiunModel", stasiunModel);
		
		String jsp = "stasiun/cruds/delete";
		return jsp;
	}
	
	@RequestMapping(value="stasiun/cruds/delete")
	public String stasiunDelete(HttpServletRequest request) {
		Integer idStasiun = Integer.valueOf(request.getParameter("idStasiun"));
		Integer xIdDeletedBy = this.userSearch().getIdUser();
		Date xDeletedDate = new Date();
		
		StasiunModel stasiunModel = stasiunService.searchByIdStasiun(idStasiun);
		stasiunModel.setIsDeleted(1);
		stasiunModel.setxIdDeletedBy(xIdDeletedBy);
		stasiunModel.setxDeletedDate(xDeletedDate);
		this.stasiunService.deleteTemporary(stasiunModel);
		
		String jsp = "stasiun/stasiun";
		return jsp;
	}
	
	@RequestMapping(value="stasiun/cruds/detail")
	public String stasiunDetail(HttpServletRequest request, Model model) {
		Integer idStasiun = Integer.valueOf(request.getParameter("idStasiun"));
		StasiunModel stasiunModel = stasiunService.searchByIdStasiun(idStasiun);
		List<KotaModel> kotaModelList = kotaService.search();
		model.addAttribute("kotaModelList", kotaModelList);
		model.addAttribute("stasiunModel", stasiunModel);
		
		String jsp = "stasiun/cruds/detail";
		return jsp;
	}
}
