package com.spring.booking184.controller;

import java.util.ArrayList;
//import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.spring.booking184.model.KeretaModel;
//import com.spring.booking184.model.LokasiModel;
import com.spring.booking184.service.KeretaService;
//import com.spring.booking184.service.LokasiService;

@Controller
public class KeretaController /*extends UserController*/{

	@Autowired
	private KeretaService keretaService;
	
	/*@Autowired
	private LokasiService lokasiService;*/
	
	@RequestMapping(value="kereta") // url action
	public String kereta() { // method
		String jsp = "kereta/kereta"; // target atau halaman
		return jsp;
	}
	
	// untuk memunculkan POP UP tambah kereta
	@RequestMapping(value="kereta/cruds/add")
	public String keretaAdd(Model model) {
		String kodeKeretaAuto = this.generateKodeKereta();
		model.addAttribute("kodeKeretaAuto", kodeKeretaAuto);
		
		/*// tampilkan lokasi untuk select option
		this.lokasiList(model);*/
		
		String jsp = "kereta/cruds/add";
		return jsp;
	}
	
	/*// untuk menambah kereta
	@RequestMapping(value="kereta/cruds/create")
	public String keretaCreate(HttpServletRequest request) {
		
		// get nilai dari variabel nama di jsp
		String kodeKereta = request.getParameter("kodeKereta");
		String namaKereta = request.getParameter("namaKereta");
		
		// set nilai ke variabel di modelnya
		KeretaModel keretaModel = new KeretaModel();
		keretaModel.setKodeKereta(kodeKereta);
		keretaModel.setNamaKereta(namaKereta);
		
		// simpan data
		this.keretaService.create(keretaModel);
		
		String jsp = "kereta/kereta"; // setelah nge-save, kembali ke sini lagi
		return jsp;
	}*/
	
	// untuk menambah kereta
		@RequestMapping(value="kereta/cruds/create")
		public String keretaCreate(HttpServletRequest request, Model model) {
			
			// get nilai dari variabel nama di jsp
			String kodeKereta = request.getParameter("kodeKereta");
			String namaKereta = request.getParameter("namaKereta");
			String jenisKereta = request.getParameter("jenisKereta");
			String ketKereta = request.getParameter("ketKereta");
			Integer feeKereta = Integer.valueOf(request.getParameter("feeKereta"));
			
			/*// set join table
			Integer idLokasi = Integer.valueOf(request.getParameter("idLokasi"));*/
			
			// set nilai ke variabel di modelnya
			KeretaModel keretaModel = new KeretaModel();
			keretaModel.setKodeKereta(kodeKereta);
			keretaModel.setNamaKereta(namaKereta);
			keretaModel.setJenisKereta(jenisKereta);
			keretaModel.setKetKereta(ketKereta);
			keretaModel.setFeeKereta(feeKereta);
			keretaModel.setIsDelete(0); // 0 artinya tidak di-delete
			
			/*// set join table
			keretaModel.setIdLokasi(idLokasi);*/
			
			Integer jumlahKeretaByNama = this.cekNamaKereta(namaKereta, 0);
			Integer jumlahKeretaByKode = this.cekKodeKereta(kodeKereta);
			Integer jumlahKeretaByNamaDeleted = this.cekNamaKereta(namaKereta, 1);
			
			/*// save audit trail untuk created
			Integer xIdCreatedBy = this.userSearch().getIdUser();
			keretaModel.setxIdCreatedBy(xIdCreatedBy);
			keretaModel.setxCreatedDate(new Date());*/
			
			if (jumlahKeretaByNama > 0 || jumlahKeretaByKode > 0) {
				model.addAttribute("jumlahKeretaByNama", jumlahKeretaByNama);
				model.addAttribute("jumlahKeretaByKode", jumlahKeretaByKode);
				model.addAttribute("kodeKereta", kodeKereta);
				model.addAttribute("namaKereta", namaKereta);
			}
//			else if(jumlahKeretaByNamaDeleted > 0) {
//				this.keretaService.updateByNama(keretaModel, namaKereta, kodeKereta, jenisKereta, ketKereta, feeKereta);
//			}
			else {
				this.keretaService.create(keretaModel);
			}
			
			/*if (jumlahDataKereta > 0 || jumlahDataKereta2 > 0) {
				model.addAttribute("jumlahDataKereta", jumlahDataKereta);
				model.addAttribute("jumlahDataKereta2", jumlahDataKereta2);
				model.addAttribute("kodeKereta", kodeKereta);
				model.addAttribute("namaKereta", namaKereta);
			} else {
				if(jumlahDataKereta2_2 > 0) {
					// simpan data
					this.keretaService.delete(keretaModel);
					this.keretaService.create(keretaModel);
				}
				else {
					// simpan data
					this.keretaService.create(keretaModel);
				}
//				// simpan data
//				this.keretaService.create(keretaModel);
			}*/
			
			// simpan data
//			this.keretaService.create(keretaModel);
			
			/*// tampilkan lokasi untuk select option
			this.lokasiList(model);*/
			
			String jsp = "kereta/kereta"; // setelah nge-save, kembali ke sini lagi
			return jsp;
		}
	
	@RequestMapping(value="kereta/cruds/list")
	public String keretaList(Model model) {
		List<KeretaModel> keretaModelList = new ArrayList<KeretaModel>();
		/*String kodeRole = this.userSearch().getRoleModel().getKodeRole(); 
		keretaModelList = this.keretaService.search(kodeRole);*/
		
		keretaModelList = this.keretaService.search();
		model.addAttribute("keretaModelList", keretaModelList);
		
		String jsp = "kereta/cruds/list";
		return jsp;
	}
	
	// untuk memunculkan POP UP detil kereta
	@RequestMapping(value="kereta/cruds/detail")
	public String keretaDetail(HttpServletRequest request, Model model) {
		Integer idKereta = Integer.valueOf(request.getParameter("idKereta"));
		
		KeretaModel keretaModel = new KeretaModel();
		keretaModel = this.keretaService.searchId(idKereta);
		
		model.addAttribute("keretaModel", keretaModel);
		
		/*
		 * String var1 = "JAKARTA"; model.addAttribute("var2", var1); // var2 harus sama
		 * persis dengan yang ada di detail.jsp
		 * 
		 * String var3 = "NASI PECEL"; model.addAttribute("var3", var3);
		 */
		
		/*// tampilkan lokasi
		this.lokasiList(model);*/
		
		String jsp = "kereta/cruds/detail";
		return jsp;
	}
	
	// untuk memunculkan POP UP edit kereta
	@RequestMapping(value="kereta/cruds/edit")
	public String keretaList(HttpServletRequest request, Model model) {
		Integer idKereta = Integer.valueOf(request.getParameter("idKereta"));
		
		KeretaModel keretaModel = new KeretaModel();
		keretaModel = this.keretaService.searchId(idKereta);
		model.addAttribute("keretaModel", keretaModel);
		
		/*// tampilkan lokasi
		this.lokasiList(model);*/
		
		String jsp = "kereta/cruds/edit";
		return jsp;
	}
	
	// untuk melakukan update kereta
	@RequestMapping(value="kereta/cruds/update")
	public String keretaUpdate(HttpServletRequest request, Model model) {
		
		// get nilai dari variabel nama di jsp
		Integer idKereta = Integer.valueOf(request.getParameter("idKereta"));
		String kodeKereta = request.getParameter("kodeKereta");
		String namaKereta = request.getParameter("namaKereta");
		String jenisKereta = request.getParameter("jenisKereta");
		String ketKereta = request.getParameter("ketKereta");
		Integer feeKereta = Integer.valueOf(request.getParameter("feeKereta"));
//		Integer idLokasi = Integer.valueOf(request.getParameter("idLokasi"));
		
		/*
		 * String varBackEnd = request.getParameter("varJSP");
		 * System.out.println(varBackEnd);
		 */
		
		// set nilai ke variabel di modelnya
		KeretaModel keretaModel = new KeretaModel();
		keretaModel = this.keretaService.searchId(idKereta);
		keretaModel.setKodeKereta(kodeKereta);
		keretaModel.setNamaKereta(namaKereta);
		keretaModel.setJenisKereta(jenisKereta);
		keretaModel.setKetKereta(ketKereta);
		keretaModel.setFeeKereta(feeKereta);
		/*keretaModel.setIdLokasi(idLokasi);*/
		
		// tampilkan lokasi untuk select option
//		this.lokasiList(model);
		
		/*// save audit trail untuk updated
		Integer xIdUpdatedBy = this.userSearch().getIdUser();
		keretaModel.setxIdUpdatedBy(xIdUpdatedBy);
		keretaModel.setxUpdatedDate(new Date());*/
		
		// simpan data
		this.keretaService.update(keretaModel);
		
		String jsp = "kereta/kereta"; // setelah nge-save, kembali ke sini lagi
		return jsp;
	}
	
	// untuk memunculkan POP UP hapus kereta
	@RequestMapping(value="kereta/cruds/remove")
	public String keretaRemove(HttpServletRequest request, Model model) {
		
		// get nilai dari variabel nama di jsp
		Integer idKereta = Integer.valueOf(request.getParameter("idKereta"));
		
		// set nilai ke variabel di modelnya
		KeretaModel keretaModel = new KeretaModel();
		keretaModel = this.keretaService.searchId(idKereta);
		
		model.addAttribute("keretaModel", keretaModel);
		
		/*// tampilkan lokasi
		this.lokasiList(model);*/
		
		String jsp = "kereta/cruds/remove"; // setelah nge-save, kembali ke sini lagi
		return jsp;
	}
	
	/*// untuk menghapus data kereta
	@RequestMapping(value="kereta/cruds/delete")
	public String keretaDelete(HttpServletRequest request) {
		
		// get nilai dari variabel nama di jsp
		Integer idKereta = Integer.valueOf(request.getParameter("idKereta"));
		
		// set nilai ke variabel di modelnya
		KeretaModel keretaModel = new KeretaModel();
		keretaModel = this.keretaService.searchId(idKereta);
		
		// simpan data
		this.keretaService.delete(keretaModel);
		
		String jsp = "kereta/kereta"; // setelah nge-save, kembali ke sini lagi
		return jsp;
	}*/
	
	// untuk menghapus data kereta
		@RequestMapping(value="kereta/cruds/delete")
		public String keretaDelete(HttpServletRequest request) {
			
			// get nilai dari variabel nama di jsp
			Integer idKereta = Integer.valueOf(request.getParameter("idKereta"));
			
			// set nilai ke variabel di modelnya
			KeretaModel keretaModel = new KeretaModel();
			keretaModel = this.keretaService.searchId(idKereta);
			keretaModel.setIsDelete(1);

/*			// simpan data
			this.keretaService.delete(keretaModel);*/
			
			/*// save audit trail untuk deleted
			Integer xIdDeletedBy = this.userSearch().getIdUser();
			keretaModel.setxIdDeletedBy(xIdDeletedBy);
			keretaModel.setxDeletedDate(new Date());*/
			
			// simpan data
			this.keretaService.deleteTemporary(keretaModel);
			
			String jsp = "kereta/kereta"; // setelah nge-save, kembali ke sini lagi
			return jsp;
		}
	
	/*@RequestMapping(value="kereta/cruds/search/nama")
	public String keretaSearchNama(HttpServletRequest request, Model model) {
		String namaKereta = request.getParameter("namaKeretaKey");
		List<KeretaModel> keretaModelList = new ArrayList<KeretaModel>();
		keretaModelList = this.keretaService.searchNama(namaKereta);
		model.addAttribute("keretaModelList", keretaModelList);
		
		String jsp = "kereta/cruds/list";
		return jsp;
	}
	
	@RequestMapping(value="kereta/cruds/search/kode")
	public String keretaSearchKode(HttpServletRequest request, Model model) {
		String kodeKereta = request.getParameter("kodeKeretaKey");
		List<KeretaModel> keretaModelList = new ArrayList<KeretaModel>();
		keretaModelList = this.keretaService.searchKode(kodeKereta);
		model.addAttribute("keretaModelList", keretaModelList);
		
		String jsp = "kereta/cruds/list";
		return jsp;
	}
	
	@RequestMapping(value="kereta/cruds/search")
	public String keretaSearchKodeOrNama(HttpServletRequest request, Model model) {
		String kodeKereta = request.getParameter("keyword");
		String namaKereta = request.getParameter("keyword");
		List<KeretaModel> keretaModelList = new ArrayList<KeretaModel>();
		keretaModelList = this.keretaService.searchKodeOrNama(kodeKereta, namaKereta);
		model.addAttribute("keretaModelList", keretaModelList);
		
		String jsp = "kereta/cruds/list";
		return jsp;
	}*/
	
	public Integer cekKodeKereta(String kodeKereta) {
		List<KeretaModel> keretaModelList = new ArrayList<KeretaModel>();
		keretaModelList = this.keretaService.searchKodeEqual(kodeKereta);
		Integer jumlahDataKereta = 0;
		if (keretaModelList == null) {
			jumlahDataKereta = 0;
		} else {
			jumlahDataKereta = keretaModelList.size();
		}
		return jumlahDataKereta;
	}
	
	public Integer cekNamaKereta(String namaKereta, Integer isDelete) {
		List<KeretaModel> keretaModelList = new ArrayList<KeretaModel>();
		keretaModelList = this.keretaService.searchNamaEqual(namaKereta, isDelete);
		Integer jumlahDataKereta = 0;
		if (keretaModelList == null) {
			jumlahDataKereta = 0;
		} else {
			jumlahDataKereta = keretaModelList.size();
		}
		return jumlahDataKereta;
	}
	
	public String generateKodeKereta() {
		String kodeKeretaAuto = "KRA";
		
		List<KeretaModel> keretaModelList = new ArrayList<KeretaModel>();
		/*String kodeRole = this.userSearch().getRoleModel().getKodeRole();
		keretaModelList = this.keretaService.search(kodeRole);*/
		keretaModelList = this.keretaService.search();
		Integer jumlahDataKereta = 0;
		
		if (keretaModelList == null) {
			jumlahDataKereta = 0;
			kodeKeretaAuto = kodeKeretaAuto + "00" + (jumlahDataKereta+1);
		} else {
			jumlahDataKereta = keretaModelList.size();
			
			if(jumlahDataKereta>9) {
				kodeKeretaAuto = kodeKeretaAuto + "0" + (jumlahDataKereta+1);
			}
			else if(jumlahDataKereta>99) {
				kodeKeretaAuto = kodeKeretaAuto + (jumlahDataKereta+1);
			}
			else {
				kodeKeretaAuto = kodeKeretaAuto + "00" + (jumlahDataKereta+1);
			}
		}
		
//		kodeKeretaAuto = kodeKeretaAuto + "00" + (jumlahDataKereta+1);
		
		return kodeKeretaAuto;
	}
	
	/*public void lokasiList(Model model) {
		List<LokasiModel> lokasiModelList = new ArrayList<LokasiModel>();
		lokasiModelList = this.lokasiService.search();
		model.addAttribute("lokasiModelList", lokasiModelList);
	}*/
	
}
