package com.spring.booking184.controller;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.spring.booking184.model.KotaModel;
import com.spring.booking184.model.LokasiModel;
import com.spring.booking184.service.KotaService;
import com.spring.booking184.service.LokasiService;

@Controller
public class KotaController extends UserController {
	
	@Autowired
	private KotaService kotaService;
	
	@Autowired
	private LokasiService lokasiService;

	private String kodeKota;

	@RequestMapping(value ="kota") //url action
	public String kota() { //method
		String jsp ="kota/kota"; //target url atau halaman
		return jsp;
	}
	
	@RequestMapping(value ="kota/cruds/add")
	public String kotaAdd(Model model) {
//		String kodeKotaAuto = this.generateKodeKota();
//		model.addAttribute("kodeKotaAuto", kodeKotaAuto);
//		
//		//tampilkan lokasi select option
//		this.lokasiList(model);
		
		String jsp ="kota/cruds/add";
		return jsp;
	}
	
	@RequestMapping(value ="kota/cruds/create") 
	public String kotaCreate(HttpServletRequest request, Model model) { 
		
		//get nilai dari variabel name di jsp
		//String idKota = request.getParameter("idKota");
		String kodeKota = request.getParameter("kodeKota");
		String namaKota = request.getParameter("namaKota");
		/*
		 * //set join table Integer idLokasi =
		 * Integer.valueOf(request.getParameter("idLokasi"));
		 */
		
		// set nilai ke variabel modelnya
		KotaModel kotaModel = new KotaModel();
		//kotaModel.setKodeKota(idKota);
		kotaModel.setKodeKota(kodeKota);
		kotaModel.setNamaKota(namaKota);
		kotaModel.setIsDelete(0);
		
		
		//panggil method untuk cek kode dan nama
		//Integer jumlahDataKota = this.cekKodeKota(kodeKota);
		Integer jumlahDataKota = this.cekNamaKota(namaKota); //kalo disuruh ganti nama tinggal aktifin yg ini
		
		//Save audit trail untuk Created
		Integer xIdCreatedBy = this.userSearch().getIdUser();
		kotaModel.setxIdCreatedBy(xIdCreatedBy);
		kotaModel.setxCreatedDate(new Date());
		
		
			//simpan data
			this.kotaService.create(kotaModel);
		
		String jsp ="kota/kota"; //abis save balik ke halaman utama
		return jsp;
	}
	
	@RequestMapping(value ="kota/cruds/list") //url action
	public String kotaList(Model model) { //method
		List<KotaModel> kotaModelList = new ArrayList<KotaModel>();
		kotaModelList = this.kotaService.search();
		model.addAttribute("kotaModelList", kotaModelList);
		String jsp ="kota/cruds/list"; //target url atau halaman
		return jsp;
	}
	
	@RequestMapping(value ="kota/cruds/detail") //url action
	public String kotaDetail(HttpServletRequest request, Model model) { //method
		Integer idKota = Integer.valueOf(request.getParameter("idKota"));
		
		KotaModel kotaModel = new KotaModel();
		kotaModel = this.kotaService.searchId(idKota);
		
		model.addAttribute("kotaModel", kotaModel);
		
		//tampilkan lokasi select option
		this.lokasiList(model);
		/*
		 * String var1= "JAKARTA"; model.addAttribute("var2", var1); String var2 =
		 * "ES JERUK"; model.addAttribute("minum", var2); String var3 = "PIZZA";
		 * model.addAttribute("makan", var3);
		 */
		
		String jsp ="kota/cruds/detail"; //target url atau halaman
		return jsp;
	}
	
	@RequestMapping(value ="kota/cruds/edit") //url action
	public String kotaEdit(HttpServletRequest request, Model model) { //method
		Integer idKota = Integer.valueOf(request.getParameter("idKota"));
		
		KotaModel kotaModel = new KotaModel();
		kotaModel = this.kotaService.searchId(idKota);
		
		model.addAttribute("kotaModel", kotaModel);
		//tampilkan lokasi select option
		this.lokasiList(model);
		/*
		 * String var1= "JAKARTA"; model.addAttribute("var2", var1); String var2 =
		 * "ES JERUK"; model.addAttribute("minum", var2); String var3 = "PIZZA";
		 * model.addAttribute("makan", var3);
		 */
		
		String jsp ="kota/cruds/edit"; //target url atau halaman
		return jsp;
	}
	
	@RequestMapping(value ="kota/cruds/update") 
	public String kotaUpdate(HttpServletRequest request) { 
		
		//get nilai dari variabel name di jsp
		Integer idKota = Integer.valueOf(request.getParameter("idKota"));
		String kodeKota = request.getParameter("kodeKota");
		String namaKota = request.getParameter("namaKota");
		//set join table
		//Integer idLokasi = Integer.valueOf(request.getParameter("idLokasi"));
		
		/*
		 * String varBackEnd = request.getParameter("varJSP");
		 * System.out.println(varBackEnd);
		 */
		
		// set nilai ke variabel modelnya
		KotaModel kotaModel = new KotaModel();
		
		kotaModel = this.kotaService.searchId(idKota);
		
		kotaModel.setKodeKota(kodeKota);
		kotaModel.setNamaKota(namaKota);
		
		
		
		
		//Save audit trail untuk Created
		Integer xIdUpdatedBy = this.userSearch().getIdUser();
		kotaModel.setxIdUpdatedBy(xIdUpdatedBy);
		kotaModel.setxUpdatedDate(new Date());
				
		
		//simpan data
		this.kotaService.update(kotaModel);
		String jsp ="kota/kota"; //abis save balik ke halaman utama
		return jsp;
	}
	
	@RequestMapping(value ="kota/cruds/remove") //url action
	public String kotaRemove(HttpServletRequest request, Model model) { //method
		Integer idKota = Integer.valueOf(request.getParameter("idKota"));
		
		KotaModel kotaModel = new KotaModel();
		kotaModel = this.kotaService.searchId(idKota);
		
		model.addAttribute("kotaModel", kotaModel);
		//tampilkan lokasi select option
		this.lokasiList(model);
		
		String jsp ="kota/cruds/remove"; //target url atau halaman
		return jsp;
	}
	
	@RequestMapping(value ="kota/cruds/delete") 
	public String kotaDelete(HttpServletRequest request) { 
		
		//get nilai dari variabel name di jsp
		Integer idKota = Integer.valueOf(request.getParameter("idKota"));
		
		// set nilai ke variabel modelnya
		KotaModel kotaModel = new KotaModel();
		
		kotaModel = this.kotaService.searchId(idKota);
	
		kotaModel.setIsDelete(1); //status 1 artinya didelete
		
		//Save audit trail untuk Created
		Integer xIdDeletedBy = this.userSearch().getIdUser();
		kotaModel.setxIdDeletedBy(xIdDeletedBy);
		kotaModel.setxDeletedDate(new Date());
		
		//simpan data
		this.kotaService.deleteTemporary(kotaModel);
		String jsp ="kota/kota"; //abis save balik ke halaman utama
		return jsp;
	}
	
	@RequestMapping(value="kota/cruds/search/nama")
	public String kotaSearchNama(HttpServletRequest request, Model model) {
		String namaKota = request.getParameter("namaKotaKey");
		List<KotaModel> kotaModelList = new ArrayList<KotaModel>();
		kotaModelList = this.kotaService.searchNama(namaKota);
		
		model.addAttribute("kotaModelList", kotaModelList); // menampilkan ke jsp
		String jsp ="kota/cruds/list";
		return jsp;
	}
	
	@RequestMapping(value="kota/cruds/search/kode")
	public String kotaSearchKode(HttpServletRequest request, Model model) {
		String kodeKota = request.getParameter("kodeKotaKey");
		List<KotaModel> kotaModelList = new ArrayList<KotaModel>();
		kotaModelList = this.kotaService.searchKode(kodeKota);
		
		model.addAttribute("kotaModelList", kotaModelList); // menampilkan ke jsp
		String jsp ="kota/cruds/list";
		return jsp;
	}
	
	@RequestMapping(value="kota/cruds/search")
	public String kotaSearch(HttpServletRequest request, Model model) {
		String kodeKota = request.getParameter("keyword");
		String namaKota = request.getParameter("keyword");
		List<KotaModel> kotaModelList = new ArrayList<KotaModel>();
		kotaModelList = this.kotaService.searchKodeOrNama(kodeKota, namaKota);
		
		model.addAttribute("kotaModelList", kotaModelList); // menampilkan ke jsp
		String jsp ="kota/cruds/list";
		return jsp;
	}
	
	public Integer cekKodeKota(String kodeKota) {
		List<KotaModel> kotaModelList = new ArrayList<KotaModel>();
		kotaModelList = this.kotaService.searchKodeEqual(kodeKota);
		Integer jumlahDataKota=0;
		if (kotaModelList == null) {
			jumlahDataKota=0;
		} else {
			jumlahDataKota = kotaModelList.size();
		}
		return jumlahDataKota;
	}
	
	public Integer cekNamaKota(String namaKota) {
		List<KotaModel> kotaModelList = new ArrayList<KotaModel>(); //object
		kotaModelList = this.kotaService.searchNamaEqual(namaKota); 
		
		Integer jumlahDataKota=0;
		
		if (kotaModelList == null) {
			jumlahDataKota=0;
		} else {
			jumlahDataKota=kotaModelList.size();
		}
		return jumlahDataKota; 
	}
	//fungsi generatekode
	public String generateKodeKota() {
		String kodeKotaAuto = "FK";
		
		List<KotaModel> kotaModelList = new ArrayList<KotaModel>(); 
		kotaModelList = this.kotaService.search();
		Integer jumlahDataKota=0;
		if (kotaModelList == null) {
			jumlahDataKota=0;
		} else {
			jumlahDataKota=kotaModelList.size();
		}
		
		kodeKotaAuto = kodeKotaAuto + "000" +(jumlahDataKota+1);
		
		return kodeKotaAuto;
	}
	
	public void lokasiList(Model model) {
		List<LokasiModel> lokasiModelList = new ArrayList<>(); 
		lokasiModelList = this.lokasiService.search();
		model.addAttribute("lokasiModelList", lokasiModelList);
	}
}
