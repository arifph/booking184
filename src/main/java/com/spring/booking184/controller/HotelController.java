package com.spring.booking184.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.spring.booking184.model.HotelModel;
import com.spring.booking184.model.KotaModel;

import com.spring.booking184.service.HotelService;
import com.spring.booking184.service.KotaService;

@Controller
public class HotelController extends UserController {
	
	@Autowired
	private HotelService hotelService;
	
	@Autowired
	private KotaService kotaService;

	@RequestMapping(value="hotel")
	public String hotel() {
		// TODO Auto-generated method stub
		String jsp = "hotel/hotel";
		return jsp;
	}
	
	@RequestMapping(value="hotel/cruds/add")
	public String hotelAdd(Model model) {
		// TODO Auto-generated method stub
		String kodeHotelAuto = this.generateKodeHotel();
		model.addAttribute("kodeHotelAuto", kodeHotelAuto);
	
		this.kotaList(model); //menambahkan lokasi
		String jsp = "hotel/cruds/add";
		return jsp;
	}
	
	@RequestMapping(value="hotel/cruds/create")
	public String hotelCreate(HttpServletRequest request, Model model) {
		// TODO Auto-generated method stub
		String kodeHotel = request.getParameter("kodeHotel");
		String namaHotel = request.getParameter("namaHotel");
		String bintang = request.getParameter("bintang");
		String telp = request.getParameter("telp");
		String email = request.getParameter("email");
		String alamatHotel = request.getParameter("alamatHotel");
		Integer feeAgen = Integer.valueOf(request.getParameter("feeAgen")) ;
		Integer idKota = Integer.valueOf(request.getParameter("idKota"));
		
		HotelModel hotelModel = new HotelModel();
		hotelModel.setKodeHotel(kodeHotel);
		hotelModel.setNamaHotel(namaHotel);
		hotelModel.setBintang(bintang);
		hotelModel.setTelp(telp);
		hotelModel.setEmail(email);
		hotelModel.setAlamatHotel(alamatHotel);
		hotelModel.setFeeAgen(feeAgen);
		hotelModel.setIsDelete(0);
		
		Integer jumlahDataHotel = this.cekKodeHotel(kodeHotel);
		
		hotelModel.setIdKota(idKota);
		
		//save audit trail untuk created
		Integer xIdCreatedBy = this.userSearch().getIdUser();
		hotelModel.setxIdCreatedBy(xIdCreatedBy);
		hotelModel.setxCreatedDate(new Date());
		
		if (jumlahDataHotel >0) {
			model.addAttribute("jumlahDataHotel", jumlahDataHotel);
			model.addAttribute("kodeHotel", kodeHotel);
		} else {			
			//simpan data
			this.hotelService.create(hotelModel);
		}
		
		String jsp = "hotel/hotel";
		return jsp;
	}
	
	@RequestMapping(value = "hotel/cruds/list")
	public String hotelList(Model model) {
		List<HotelModel> hotelModelList = new ArrayList<HotelModel>();
		String kodeRole = this.userSearch().getRoleModel().getKodeRole();
		
		hotelModelList = this.hotelService.search(kodeRole);
		model.addAttribute("hotelModelList", hotelModelList);
		
		String jsp = "hotel/cruds/list"; 
		return jsp;
	}
	
	//controller mengatur detail
	@RequestMapping(value = "hotel/cruds/detail")
	public String hotelDetail(HttpServletRequest request, Model model) {
		Integer idHotel = Integer.valueOf(request.getParameter("idHotel"));
		HotelModel hotelModel = new HotelModel();
		hotelModel = this.hotelService.searchId(idHotel);
				
		model.addAttribute("hotelModel", hotelModel);
				
		this.kotaList(model); //menambahkan lokasi

				
		String jsp = "hotel/cruds/detail"; 
		return jsp;
		}
		
	@RequestMapping(value = "hotel/cruds/edit")
	public String hotelEdit(HttpServletRequest request, Model model) {
		Integer idHotel = Integer.valueOf(request.getParameter("idHotel"));
		
		HotelModel hotelModel = new HotelModel();
		hotelModel = this.hotelService.searchId(idHotel);
				
		model.addAttribute("hotelModel", hotelModel);
				
		this.kotaList(model); //menambahkan lokasi

				
		String jsp = "hotel/cruds/edit"; 
		return jsp;
	}
	
	@RequestMapping(value="hotel/cruds/update")
	public String hotelUpdate(HttpServletRequest request) {
		// TODO Auto-generated method stub
		Integer idHotel = Integer.valueOf(request.getParameter("idHotel"));
		String kodeHotel = request.getParameter("kodeHotel");
		String namaHotel = request.getParameter("namaHotel");
		String bintang = request.getParameter("bintang");
		String telp = request.getParameter("telp");
		String email = request.getParameter("email");
		String alamatHotel = request.getParameter("alamatHotel");
		Integer feeAgen = Integer.valueOf(request.getParameter("feeAgen")) ;
		Integer idKota = Integer.valueOf(request.getParameter("idKota"));
		
		HotelModel hotelModel = new HotelModel();
		hotelModel = this.hotelService.searchId(idHotel);
		
		hotelModel.setKodeHotel(kodeHotel);
		hotelModel.setNamaHotel(namaHotel);
		hotelModel.setBintang(bintang);
		hotelModel.setTelp(telp);
		hotelModel.setEmail(email);
		hotelModel.setAlamatHotel(alamatHotel);
		hotelModel.setFeeAgen(feeAgen);
		hotelModel.setIdKota(idKota);
		
		//save audit trail untuk created
		Integer xIdUpdatedBy = this.userSearch().getIdUser();
		hotelModel.setxIdUpdatedBy(xIdUpdatedBy);
		hotelModel.setxUpdatedDate(new Date());

		//simpan data
		this.hotelService.update(hotelModel);
		
		String jsp = "hotel/hotel";
		return jsp;
	}
	
	@RequestMapping(value = "hotel/cruds/remove")
	public String hotelRemove(HttpServletRequest request, Model model) {
		Integer idHotel = Integer.valueOf(request.getParameter("idHotel"));
		
		HotelModel hotelModel = new HotelModel();
		hotelModel = this.hotelService.searchId(idHotel);
		
		this.kotaList(model); //menambahkan lokasi

		model.addAttribute("hotelModel", hotelModel);

		String jsp = "hotel/cruds/remove"; 
		return jsp;
	}
	
	@RequestMapping(value="hotel/cruds/delete")
	public String hotelDelete(HttpServletRequest request) {
		// TODO Auto-generated method stub
		Integer idHotel = Integer.valueOf(request.getParameter("idHotel"));
		
		HotelModel hotelModel = new HotelModel();
		hotelModel = this.hotelService.searchId(idHotel);
		hotelModel.setIsDelete(1);
		
		//save audit trail untuk created
		Integer xIdDeletedBy = this.userSearch().getIdUser();
		hotelModel.setxIdDeletedBy(xIdDeletedBy);
		hotelModel.setxDeletedDate(new Date());

		//simpan data
		this.hotelService.deleteTemporary(hotelModel);
		
		String jsp = "hotel/hotel";
		return jsp;
	}
	
	
	public Integer cekKodeHotel(String kodeHotel) {
		List<HotelModel> hotelModelList = new ArrayList<HotelModel>(); //kode butuh list
		hotelModelList = this.hotelService.searchKodeEqual(kodeHotel);
		Integer jumlahDataHotel = 0;
		if (hotelModelList == null) {
			jumlahDataHotel = 0;
		} else {
			jumlahDataHotel = hotelModelList.size();
		}
		
		return jumlahDataHotel;
	}
	
	public Integer cekNamaHotel(String namaHotel) {
		List<HotelModel> hotelModelList = new ArrayList<HotelModel>(); //kode butuh list
		hotelModelList = this.hotelService.searchKodeEqual(namaHotel);
		Integer jumlahDataHotel = 0;
		if (hotelModelList == null) {
			jumlahDataHotel = 0;
		} else {
			jumlahDataHotel = hotelModelList.size();
		}
		
		return jumlahDataHotel;
	}
	public String generateKodeHotel() {
		String kodeHotelAuto = "HTL";
		
		List<HotelModel> hotelModelList = new ArrayList<HotelModel>(); //kode butuh list
		String kodeRole = this.userSearch().getRoleModel().getKodeRole();
		
		hotelModelList = this.hotelService.search(kodeRole);
		Integer jumlahDataFakultas = 0;
		
		if (hotelModelList == null) {
			jumlahDataFakultas = 0;
		} else {
			jumlahDataFakultas = hotelModelList.size();
		}
		
		kodeHotelAuto = kodeHotelAuto +"00"+(jumlahDataFakultas+1);
		
		return kodeHotelAuto;
	}
	
	public void kotaList(Model model) {
		List<KotaModel> kotaModelList = new ArrayList<KotaModel>(); //kode butuh list
		kotaModelList = this.kotaService.search();
		model.addAttribute("kotaModelList", kotaModelList);
	}
}
