package com.spring.booking184.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.spring.booking184.model.BisModel;
import com.spring.booking184.service.BisService;

@Controller
public class BisController extends UserController{
	
	@Autowired
	private BisService bisService;
	
	@RequestMapping(value ="bis") // request mapping adalah url action
	public String Fakultas() { // method

		String jsp = "bis/bis"; // target atau halaman

		return jsp;
	}
	
	@RequestMapping(value="bis/cruds/add")
	public String BisAdd() {
		
		String jsp = "bis/cruds/add";
		
		return jsp;
	}
	
	@RequestMapping(value="bis/cruds/create")
	public String BisCreate(HttpServletRequest request) {
		
		String kodeBis = request.getParameter("kodeBis");
		String namaBis = request.getParameter("namaBis");
		String transitBis = request.getParameter("transitBis");
		Integer teleponBis = Integer.valueOf(request.getParameter("teleponBis"));
		String emailBis = request.getParameter("emailBis");
		String alamatBis = request.getParameter("alamatBis");
		String feeAgenBis = request.getParameter("feeAgenBis");
		
		BisModel bisModel = new BisModel();
		bisModel.setKodeBis(kodeBis);
		bisModel.setNamaBis(namaBis);
		bisModel.setTransitBis(transitBis);
		bisModel.setTeleponBis(teleponBis);
		bisModel.setEmailBis(emailBis);
		bisModel.setAlamatBis(alamatBis);
		bisModel.setFeeAgenBis(feeAgenBis);
		
		bisModel.setIsDeleted(0);
		
		Integer xIdCreatedBy = this.userSearch().getIdUser();
		bisModel.setxIdCreatedBy(xIdCreatedBy);
		bisModel.setxCreatedDate(new Date());
		
		this.bisService.create(bisModel);
		
		String jsp = "bis/bis";
		
		return jsp;
	}
	
	@RequestMapping(value="bis/cruds/list")
	public String BisList(Model model) {
		
		List<BisModel> bisModelList = new ArrayList<BisModel>();
		
		String kodeRole = this.userSearch().getRoleModel().getKodeRole();		
		bisModelList = this.bisService.search(kodeRole);
		
		model.addAttribute("bisModelList", bisModelList);
		
		String jsp = "bis/cruds/list";
		return jsp;
		
	}
	
	@RequestMapping(value="bis/cruds/detail")
	public String BisDetail(HttpServletRequest request, Model model) {
		Integer idBis = Integer.valueOf(request.getParameter("idBis"));
		BisModel bisModel = new BisModel();
		bisModel = this.bisService.searchId(idBis);
		
		model.addAttribute("bisModel", bisModel);
		String jsp = "bis/cruds/detail";
		return jsp;
	}
	
	@RequestMapping(value="bis/cruds/edit")
	public String BisEdit (HttpServletRequest request, Model model) {
		Integer idBis = Integer.valueOf(request.getParameter("idBis"));
		BisModel bisModel = new BisModel();
		bisModel = this.bisService.searchId(idBis);
		
		model.addAttribute("bisModel", bisModel);
		
		String jsp = "bis/cruds/edit";
		return jsp;
	}
	
	@RequestMapping(value="bis/cruds/update")
	public String BisUpdate(HttpServletRequest request, Model model) {
		
		Integer idBis = Integer.valueOf(request.getParameter("idBis"));
		
		String kodeBis = request.getParameter("kodeBis");
		String namaBis = request.getParameter("namaBis");
		String transitBis = request.getParameter("transitBis");
		Integer teleponBis = Integer.valueOf(request.getParameter("teleponBis"));
		String emailBis = request.getParameter("emailBis");
		String alamatBis = request.getParameter("alamatBis");
		String feeAgenBis = request.getParameter("feeAgenBis");
		
		BisModel bisModel = new BisModel();
		
		bisModel = this.bisService.searchId(idBis);
		
		bisModel.setKodeBis(kodeBis);
		bisModel.setNamaBis(namaBis);
		bisModel.setTransitBis(transitBis);
		bisModel.setTeleponBis(teleponBis);
		bisModel.setEmailBis(emailBis);
		bisModel.setAlamatBis(alamatBis);
		bisModel.setFeeAgenBis(feeAgenBis); 
		
		Integer xIdUpdatedBy = this.userSearch().getIdUser();
		bisModel.setxIdUpdatedBy(xIdUpdatedBy);
		bisModel.setxUpdatedDate(new Date()); 
		
		this.bisService.update(bisModel);
		
		String jsp = "bis/bis";
		
		return jsp;
		
	}
	
	@RequestMapping(value="bis/cruds/delete")
	public String BisDelete(HttpServletRequest request, Model model) {
		Integer idBis = Integer.valueOf(request.getParameter("idBis"));
		BisModel bisModel = new BisModel();
		bisModel = this.bisService.searchId(idBis);
		
		model.addAttribute("bisModel", bisModel);
		
		String jsp = "bis/cruds/delete";
		return jsp;
	}
	
	@RequestMapping(value="bis/cruds/remove")
	public String BisRemove(HttpServletRequest request, Model model) {
		Integer idBis = Integer.valueOf(request.getParameter("idBis"));
		BisModel bisModel = new BisModel();
		
		bisModel = this.bisService.searchId(idBis);
		bisModel.setIsDeleted(1);
		
		Integer xIdDeletedBy = this.userSearch().getIdUser();
		bisModel.setxIdDeletedBy(xIdDeletedBy);
		bisModel.setxDeletedDate(new Date());
		
		this.bisService.deleteTemporary(bisModel);
		
		String jsp = "bis/cruds/delete";
		return jsp;
	}
}	
