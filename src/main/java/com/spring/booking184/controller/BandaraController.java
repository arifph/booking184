package com.spring.booking184.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.spring.booking184.model.BandaraModel;
import com.spring.booking184.model.KotaModel;
import com.spring.booking184.service.BandaraService;
import com.spring.booking184.service.KotaService;


@Controller
public class BandaraController extends UserController {

	@Autowired
	private BandaraService bandaraService;
	
	@Autowired
	private KotaService kotaService;
	
	@RequestMapping(value = "bandara")
	public String jurusan() {
		
		String jsp = "bandara/bandara";
		return jsp;
	}
	
	@RequestMapping(value = "bandara/cruds/add")
	public String bandaraAdd(Model model) {

		//String kodeJurusanAuto = this.generateKodeJurusan();
		//model.addAttribute("kodeJurusanAuto", kodeJurusanAuto);
		
		this.kotaList(model); //menambahkan lokasi
		
		String jsp = "bandara/cruds/add"; 
		return jsp;
	}
	
	@RequestMapping(value = "bandara/cruds/create")
	public String bandaraCreate(HttpServletRequest request) {
		
		//get nilai dari variable di jsp
		String kodeBandara = request.getParameter("kodeBandara");
		String jenisBandara = request.getParameter("jenisBandara");
		String namaBandara = request.getParameter("namaBandara");
		String alamatBandara = request.getParameter("alamatBandara");
		Integer idKota = Integer.valueOf(request.getParameter("idKota"));
		
		//set nilai ke variable di modelnya
		BandaraModel bandaraModel = new BandaraModel();
		bandaraModel.setKodeBandara(kodeBandara);
		bandaraModel.setJenisBandara(jenisBandara);
		bandaraModel.setNamaBandara(namaBandara);
		bandaraModel.setAlamatBandara(alamatBandara);
		bandaraModel.setIsDelete(0);
		bandaraModel.setIdKota(idKota);

		// Save Audit Trail Create
		Integer xIdCreatedBy = this.userSearch().getIdUser();
		bandaraModel.setxIdCreatedBy(xIdCreatedBy);
		bandaraModel.setxCreatedDate(new Date());
		
		//simpan data
		this.bandaraService.create(bandaraModel);
		
		String jsp = "bandara/bandara"; 
		return jsp;
	}
	
	@RequestMapping(value = "bandara/cruds/list")
	public String bandaraList(Model model) {
		List<BandaraModel> bandaraModelList = new ArrayList<BandaraModel>();
		String kodeRole = this.userSearch().getRoleModel().getKodeRole();
		
		bandaraModelList = this.bandaraService.search(kodeRole);
		model.addAttribute("bandaraModelList",bandaraModelList);
		
		String jsp = "bandara/cruds/list"; 
		return jsp;
	}
	
	//controller mengatur detail
	@RequestMapping(value = "bandara/cruds/detail")
	public String bandaraDetail(HttpServletRequest request, Model model) {
		Integer idBandara = Integer.valueOf(request.getParameter("idBandara"));
		
		BandaraModel bandaraModel = new BandaraModel();
		bandaraModel = this.bandaraService.searchId(idBandara);
		
		model.addAttribute("bandaraModel", bandaraModel);
		
		// Tampilkan kota untuk select option
		this.kotaList(model);

		String jsp = "bandara/cruds/detail"; 
		return jsp;
	}
	
	//untuk mengatur edit
	@RequestMapping(value = "bandara/cruds/edit")
	public String bandaraEdit(HttpServletRequest request, Model model) {
		Integer idBandara = Integer.valueOf(request.getParameter("idBandara"));
		
		BandaraModel bandaraModel = new BandaraModel();
		bandaraModel = this.bandaraService.searchId(idBandara);
		model.addAttribute("bandaraModel", bandaraModel);
		this.kotaList(model); //menambahkan lokasi

		String jsp = "bandara/cruds/edit"; 
		return jsp;
	}
		
	//unuk mengatur update
	@RequestMapping(value = "bandara/cruds/update")
	public String bandaraUpdate(HttpServletRequest request) {
		
		//get nilai dari variable di jsp
		Integer idBandara = Integer.valueOf(request.getParameter("idBandara"));
		String kodeBandara = request.getParameter("kodeBandara");
		String jenisBandara = request.getParameter("jenisBandara");
		String namaBandara = request.getParameter("namaBandara");
		String alamatBandara = request.getParameter("alamatBandara");
		Integer idKota = Integer.valueOf(request.getParameter("idKota"));
		
		//set nilai ke variable di modelnya
		BandaraModel bandaraModel = new BandaraModel();
		bandaraModel = this.bandaraService.searchId(idBandara);
		bandaraModel.setKodeBandara(kodeBandara);
		bandaraModel.setJenisBandara(jenisBandara);
		bandaraModel.setNamaBandara(namaBandara);
		bandaraModel.setAlamatBandara(alamatBandara);
		bandaraModel.setIdKota(idKota);

		// Save Audit Trail Update
		Integer xIdUpdatedBy = this.userSearch().getIdUser();
		bandaraModel.setxIdUpdatedBy(xIdUpdatedBy);
		bandaraModel.setxUpdatedDate(new Date());
		
		//simpan data
		this.bandaraService.update(bandaraModel);
		
		String jsp = "bandara/bandara"; 
		return jsp;
	}
	
	//untuk mengatur remove
	@RequestMapping(value = "bandara/cruds/remove")
	public String bandaraRemove(HttpServletRequest request, Model model) {
		Integer idBandara = Integer.valueOf(request.getParameter("idBandara"));
		
		BandaraModel bandaraModel = new BandaraModel();
		bandaraModel = this.bandaraService.searchId(idBandara);
		model.addAttribute("bandaraModel", bandaraModel);
		
		// Tampilkan lokasi untuk select option
		this.kotaList(model);

		String jsp = "bandara/cruds/remove"; 
		return jsp;
	}
	
	//untuk mengatur delete
	@RequestMapping(value = "bandara/cruds/delete")
	public String bandaraDelete(HttpServletRequest request) {
		
		//get nilai dari variable di jsp
		Integer idBandara = Integer.valueOf(request.getParameter("idBandara"));
		
		//set nilai ke variable di modelnya
		BandaraModel bandaraModel = new BandaraModel();
		bandaraModel = this.bandaraService.searchId(idBandara);
		bandaraModel.setIsDelete(1);
		
		// Save Audit Trail Delet
		Integer xIdDeletedBy = this.userSearch().getIdUser();
		bandaraModel.setxIdDeletedBy(xIdDeletedBy);
		bandaraModel.setxDeletedDate(new Date());

		// simpan data
		this.bandaraService.deleteTemporary(bandaraModel);
		
		String jsp = "bandara/bandara"; 
		return jsp;
	}
	
	public void kotaList(Model model) {
		List<KotaModel> kotaModelList = new ArrayList<KotaModel>(); //kode butuh list
		kotaModelList = this.kotaService.search();
		model.addAttribute("kotaModelList", kotaModelList);
	}
	
}
