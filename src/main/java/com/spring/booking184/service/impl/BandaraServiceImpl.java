package com.spring.booking184.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spring.booking184.dao.BandaraDao;
import com.spring.booking184.model.BandaraModel;
import com.spring.booking184.service.BandaraService;

@Service
@Transactional
public class BandaraServiceImpl implements BandaraService {

	@Autowired
	private BandaraDao bandaraDao;
	
	@Override
	public void create(BandaraModel bandaraModel) {
		// TODO Auto-generated method stub
		this.bandaraDao.create(bandaraModel);
	}

	@Override
	public void update(BandaraModel bandaraModel) {
		// TODO Auto-generated method stub
		this.bandaraDao.update(bandaraModel);
	}

	@Override
	public void delete(BandaraModel bandaraModel) {
		// TODO Auto-generated method stub
		this.bandaraDao.delete(bandaraModel);
	}

	@Override
	public List<BandaraModel> search(String kodeRole) {
		// TODO Auto-generated method stub
		return this.bandaraDao.search(kodeRole);
	}

	@Override
	public BandaraModel searchId(Integer idBandara) {
		// TODO Auto-generated method stub
		return this.bandaraDao.searchId(idBandara);
	}

	@Override
	public void deleteTemporary(BandaraModel bandaraModel) {
		// TODO Auto-generated method stub
		this.bandaraDao.update(bandaraModel);
		
	}

}
