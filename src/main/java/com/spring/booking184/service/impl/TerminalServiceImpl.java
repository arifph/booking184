package com.spring.booking184.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spring.booking184.dao.TerminalDao;
import com.spring.booking184.model.TerminalModel;
import com.spring.booking184.service.TerminalService;

@Service
@Transactional
public class TerminalServiceImpl implements TerminalService {

	@Autowired
	private TerminalDao terminalDao;

	@Override
	public void create(TerminalModel terminalModel) {
		// TODO Auto-generated method stub
		this.terminalDao.create(terminalModel);
	}

	@Override
	public void update(TerminalModel terminalModel) {
		// TODO Auto-generated method stub
		this.terminalDao.update(terminalModel);
	}

	@Override
	public void delete(TerminalModel terminalModel) {
		// TODO Auto-generated method stub
		this.terminalDao.delete(terminalModel);
	}

	@Override
	public List<TerminalModel> search() {
		// TODO Auto-generated method stub
		return this.terminalDao.search();
	}

	@Override
	public TerminalModel searchId(Integer idTerminal) {
		// TODO Auto-generated method stub
		return this.terminalDao.searchId(idTerminal);
	}

	@Override
	public List<TerminalModel> searchNama(String namaTerminal) {
		// TODO Auto-generated method stub
		return this.terminalDao.searchNama(namaTerminal);
	}

	@Override
	public List<TerminalModel> searchKode(String kodeTerminal) {
		// TODO Auto-generated method stub
		return this.terminalDao.searchKode(kodeTerminal);
	}

	@Override
	public List<TerminalModel> searchKodeOrNama(String kodeTerminal, String namaTerminal) {
		// TODO Auto-generated method stub
		return this.terminalDao.searchKodeOrNama(kodeTerminal, namaTerminal);
	}

	@Override
	public List<TerminalModel> searchKodeEqual(String kodeTerminal) {
		// TODO Auto-generated method stub
		return this.terminalDao.searchKodeEqual(kodeTerminal);
	}

	@Override
	public List<TerminalModel> searchNamaEqual(String namaTerminal) {
		// TODO Auto-generated method stub
		return this.terminalDao.searchNamaEqual(namaTerminal);
	}

	@Override
	public void deleteTemporary(TerminalModel terminalModel) {
		// TODO Auto-generated method stub
		this.terminalDao.deleteTemporary(terminalModel);
	}
	
	
}
