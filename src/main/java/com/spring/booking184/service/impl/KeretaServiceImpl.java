package com.spring.booking184.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spring.booking184.dao.KeretaDao;
import com.spring.booking184.model.KeretaModel;
import com.spring.booking184.service.KeretaService;

@Service
@Transactional
public class KeretaServiceImpl implements KeretaService{

	@Autowired
	private KeretaDao keretaDao;
	
	@Override
	public void create(KeretaModel keretaModel) {
		// TODO Auto-generated method stub
		this.keretaDao.create(keretaModel);
	}

	@Override
	public void update(KeretaModel keretaModel) {
		// TODO Auto-generated method stub
		this.keretaDao.update(keretaModel);
	}
	
//	@Override
//	public void updateByNama(KeretaModel keretaModel, String namaKereta, String kodeKereta, String jenisKereta, String ketKereta, Integer feeKereta) {
//		// TODO Auto-generated method stub
//		this.keretaDao.updateByNama(keretaModel, namaKereta, kodeKereta, jenisKereta, ketKereta, feeKereta);
//	}

	@Override
	public void delete(KeretaModel keretaModel) {
		// TODO Auto-generated method stub
		this.keretaDao.delete(keretaModel);
	}
	
	@Override
	public List<KeretaModel> search() {
		// TODO Auto-generated method stub
		return this.keretaDao.search();
	}
	
	/*@Override
	public List<KeretaModel> search(String kodeRole) {
		// TODO Auto-generated method stub
		return this.keretaDao.search(kodeRole);
	}*/

	@Override
	public KeretaModel searchId(Integer idKereta) {
		// TODO Auto-generated method stub
		return this.keretaDao.searchId(idKereta);
	}

	/*@Override
	public List<KeretaModel> searchNama(String namaKereta) {
		// TODO Auto-generated method stub
		return this.keretaDao.searchNama(namaKereta);
	}

	@Override
	public List<KeretaModel> searchKode(String kodeKereta) {
		// TODO Auto-generated method stub
		return this.keretaDao.searchKode(kodeKereta);
	}
	
	@Override
	public List<KeretaModel> searchKodeOrNama(String kodeKereta, String namaKereta) {
		// TODO Auto-generated method stub
		return this.keretaDao.searchKodeOrNama(kodeKereta, namaKereta);
	}*/

	@Override
	public List<KeretaModel> searchKodeEqual(String kodeKereta) {
		// TODO Auto-generated method stub
		return this.keretaDao.searchKodeEqual(kodeKereta);
	}

	@Override
	public List<KeretaModel> searchNamaEqual(String namaKereta, Integer isDelete) {
		// TODO Auto-generated method stub
		return this.keretaDao.searchNamaEqual(namaKereta, isDelete);
	}

	@Override
	public void deleteTemporary(KeretaModel keretaModel) {
		// TODO Auto-generated method stub
		this.keretaDao.update(keretaModel);
	}
}
