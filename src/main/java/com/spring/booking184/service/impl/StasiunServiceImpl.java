package com.spring.booking184.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.booking184.dao.StasiunDao;
import com.spring.booking184.model.StasiunModel;
import com.spring.booking184.service.StasiunService;

@Service
@Transactional
public class StasiunServiceImpl implements StasiunService {

	@Autowired
	private StasiunDao stasiunDao;
	
	@Override
	public void save(StasiunModel stasiunModel) {
		// TODO Auto-generated method stub
		this.stasiunDao.save(stasiunModel);
	}

	@Override
	public void update(StasiunModel stasiunModel) {
		// TODO Auto-generated method stub
		this.stasiunDao.update(stasiunModel);
	}

	@Override
	public void delete(StasiunModel stasiunModel) {
		// TODO Auto-generated method stub
		this.stasiunDao.delete(stasiunModel);
	}

	@Override
	public List<StasiunModel> search() {
		// TODO Auto-generated method stub
		return this.stasiunDao.search();
	}

	@Override
	public StasiunModel searchByIdStasiun(Integer idStasiun) {
		// TODO Auto-generated method stub
		return this.stasiunDao.searchByIdStasiun(idStasiun);
	}

	@Override
	public void deleteTemporary(StasiunModel stasiunModel) {
		// TODO Auto-generated method stub
		this.stasiunDao.update(stasiunModel);
	}

	@Override
	public List<StasiunModel> searchWithDeleted() {
		// TODO Auto-generated method stub
		return this.stasiunDao.searchWithDeleted();
	}

}
