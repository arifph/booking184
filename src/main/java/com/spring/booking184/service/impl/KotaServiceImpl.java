package com.spring.booking184.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.spring.booking184.dao.KotaDao;
import com.spring.booking184.model.KotaModel;
import com.spring.booking184.service.KotaService;

@Service
@Transactional
public class KotaServiceImpl implements KotaService{
	
	@Autowired
	private KotaDao kotaDao;

	@Override
	public void create(KotaModel kotaModel) {
		// TODO Auto-generated method stub
		this.kotaDao.create(kotaModel);
	}

	@Override
	public void update(KotaModel kotaModel) {
		// TODO Auto-generated method stub
		this.kotaDao.update(kotaModel);
	}

	@Override
	public void delete(KotaModel kotaModel) {
		// TODO Auto-generated method stub
		this.kotaDao.delete(kotaModel);
	}

	@Override
	public List<KotaModel> search() {
		// TODO Auto-generated method stub
		return this.kotaDao.search();
	}

	@Override
	public KotaModel searchId(Integer idKota) {
		// TODO Auto-generated method stub
		return this.kotaDao.searchId(idKota);
	}

	@Override
	public List<KotaModel> searchNama(String namaKota) {
		// TODO Auto-generated method stub
		return this.kotaDao.searchNama(namaKota);
	}

	@Override
	public List<KotaModel> searchKode(String kodeKota) {
		// TODO Auto-generated method stub
		return this.kotaDao.searchKode(kodeKota);
	}

	@Override
	public List<KotaModel> searchKodeOrNama(String kodeKota, String namaKota) {
		// TODO Auto-generated method stub
		return this.kotaDao.searchKodeOrNama(kodeKota, namaKota);
	}

	@Override
	public List<KotaModel> searchKodeEqual(String kodeKota) {
		// TODO Auto-generated method stub
		return this.kotaDao.searchKodeEqual(kodeKota);
	}

	@Override
	public List<KotaModel> searchNamaEqual(String namaKota) {
		// TODO Auto-generated method stub
		return this.kotaDao.searchNamaEqual(namaKota);
	}

	@Override
	public void deleteTemporary(KotaModel kotaModel) {
		// TODO Auto-generated method stub
		this.kotaDao.update(kotaModel);;
	}


}
