package com.spring.booking184.service.impl;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spring.booking184.dao.MahasiswaDao;
import com.spring.booking184.model.MahasiswaModel;
import com.spring.booking184.service.MahasiswaService;

@Service
@Transactional
public class MahasiswaServiceImpl implements MahasiswaService{

	//koneksi ke dao
	@Autowired
	private MahasiswaDao mahasiswaDao;
	
	@Override
	public void create(MahasiswaModel mahasiswaModel) {
		// TODO Auto-generated method stub
		this.mahasiswaDao.create(mahasiswaModel);
	}

	@Override
	public List<MahasiswaModel> list() {
		// TODO Auto-generated method stub
		return this.mahasiswaDao.list();
	}

	@Override
	public MahasiswaModel searchById(Integer idMahasiswa) {
		// TODO Auto-generated method stub
		return this.mahasiswaDao.searchById(idMahasiswa);
	}

	@Override
	public void delete(MahasiswaModel mahasiswaModel) {
		// TODO Auto-generated method stub
		this.mahasiswaDao.delete(mahasiswaModel);
	}

	@Override
	public void update(MahasiswaModel mahasiswaModel) {
		// TODO Auto-generated method stub
		this.mahasiswaDao.update(mahasiswaModel);
	}

	@Override
	public List<MahasiswaModel> searchByNim(String nimMahasiswa) {
		// TODO Auto-generated method stub
		return this.mahasiswaDao.searchByNim(nimMahasiswa);
	}

	@Override
	public MahasiswaModel searchByNamaPasti(String namaMahasiswa) {
		// TODO Auto-generated method stub
		return this.mahasiswaDao.searchByNamaPasti(namaMahasiswa);
	}

	@Override
	public List<MahasiswaModel> searchByNamaLikeDepan(String namaMahasiswa) {
		// TODO Auto-generated method stub
		return this.mahasiswaDao.searchByNamaLikeDepan(namaMahasiswa);
	}

	@Override
	public List<MahasiswaModel> searchByNamaLikeAkhir(String namaMahasiswa) {
		// TODO Auto-generated method stub
		return this.mahasiswaDao.searchByNamaLikeAkhir(namaMahasiswa);
	}

	@Override
	public List<MahasiswaModel> searchByNamaLikeOrTahunLahir(String namaMahasiswa, Integer tahunLahirMahasiswa) {
		// TODO Auto-generated method stub
		return this.mahasiswaDao.searchByNamaLikeOrTahunLahir(namaMahasiswa, tahunLahirMahasiswa);
	}

	@Override
	public MahasiswaModel searchByTeleponSingle(String teleponMahasiswa) {
		// TODO Auto-generated method stub
		return this.mahasiswaDao.searchByTeleponSingle(teleponMahasiswa);
	}

	@Override
	public List<MahasiswaModel> searchByTeleponList(String teleponMahasiswa) {
		// TODO Auto-generated method stub
		return this.mahasiswaDao.searchByTeleponList(teleponMahasiswa);
	}

}
