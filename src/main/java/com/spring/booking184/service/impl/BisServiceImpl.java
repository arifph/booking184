package com.spring.booking184.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.booking184.dao.BisDao;
import com.spring.booking184.model.BisModel;
import com.spring.booking184.service.BisService;

@Service
@Transactional
public class BisServiceImpl implements BisService{
	
	@Autowired
	private BisDao bisDao;
	
	@Override
	public void create(BisModel bisModel) {
		// TODO Auto-generated method stub
		this.bisDao.create(bisModel);
	}

	@Override
	public void update(BisModel bisModel) {
		// TODO Auto-generated method stub
		this.bisDao.update(bisModel);
	}

	@Override
	public void delete(BisModel bisModel) {
		// TODO Auto-generated method stub
		this.bisDao.delete(bisModel);
	}

	@Override
	public List<BisModel> search(String kodeRole) {
		// TODO Auto-generated method stub
		return this.bisDao.search(kodeRole);
	}

	@Override
	public BisModel searchId(Integer idBis) {
		// TODO Auto-generated method stub
		return this.bisDao.searchId(idBis);
	}

	@Override
	public void deleteTemporary(BisModel bisModel) {
		// TODO Auto-generated method stub
		this.bisDao.update(bisModel);
	}
}
