package com.spring.booking184.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spring.booking184.dao.LokasiDao;
import com.spring.booking184.model.LokasiModel;
import com.spring.booking184.service.LokasiService;

@Service
@Transactional
public class LokasiServiceImpl implements LokasiService{
	@Autowired
	private LokasiDao lokasiDao;
	
	@Override
	public List<LokasiModel> search(){
		return this.lokasiDao.search();
	}
}
