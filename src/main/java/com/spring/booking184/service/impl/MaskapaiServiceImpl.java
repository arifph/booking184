package com.spring.booking184.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spring.booking184.dao.MaskapaiDao;
import com.spring.booking184.model.MaskapaiModel;
import com.spring.booking184.service.MaskapaiService;

@Service
@Transactional
public class MaskapaiServiceImpl implements MaskapaiService {
	
	@Autowired
	private MaskapaiDao maskapaiDao;

	@Override
	public void create(MaskapaiModel maskapaiModel) {
		// TODO Auto-generated method stub
		this.maskapaiDao.create(maskapaiModel);
	}

	@Override
	public List<MaskapaiModel> search() {
		// TODO Auto-generated method stub
		return this.maskapaiDao.search();
	}

	@Override
	public MaskapaiModel searchId(Integer idMaskapai) {
		// TODO Auto-generated method stub
		return this.maskapaiDao.searchId(idMaskapai);
	}

	@Override
	public void update(MaskapaiModel maskapaiModel) {
		// TODO Auto-generated method stub
		this.maskapaiDao.delete(maskapaiModel);
	}

	@Override
	public void delete(MaskapaiModel maskapaiModel) {
		// TODO Auto-generated method stub
		this.maskapaiDao.delete(maskapaiModel);
	}

	@Override
	public void deleteTemporary(MaskapaiModel maskapaiModel) {
		// TODO Auto-generated method stub
		this.maskapaiDao.update(maskapaiModel);
	}

	@Override
	public List<MaskapaiModel> searchKodeEqual(String kodeMaskapai) {
		// TODO Auto-generated method stub
		return this.maskapaiDao.searchKodeEqual(kodeMaskapai);
	}

	@Override
	public List<MaskapaiModel> searchNamaEqual(String namaMaskapai) {
		// TODO Auto-generated method stub
		return this.maskapaiDao.searchNamaEqual(namaMaskapai);
	}
	
	
	
}
