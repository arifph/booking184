package com.spring.booking184.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spring.booking184.dao.HotelDao;
import com.spring.booking184.model.HotelModel;
import com.spring.booking184.service.HotelService;

@Service
@Transactional
public class HotelServiceImpl implements HotelService{

	@Autowired
	private HotelDao hotelDao;
	
	@Override
	public void create(HotelModel hotelModel) {
		// TODO Auto-generated method stub
		this.hotelDao.create(hotelModel);
	}

	@Override
	public void update(HotelModel hotelModel) {
		// TODO Auto-generated method stub
		this.hotelDao.update(hotelModel);
		
	}

	@Override
	public void delete(HotelModel hotelModel) {
		// TODO Auto-generated method stub
		this.hotelDao.delete(hotelModel);
	}

	@Override
	public List<HotelModel> search(String kodeRole) {
		// TODO Auto-generated method stub
		return this.hotelDao.search(kodeRole);
	}

	@Override
	public List<HotelModel> searchKodeEqual(String kodeHotel) {
		// TODO Auto-generated method stub
		return this.hotelDao.searchKodeEqual(kodeHotel);
	}

	@Override
	public List<HotelModel> searchNamaEqual(String namaHotel) {
		// TODO Auto-generated method stub
		return this.hotelDao.searchNamaEqual(namaHotel);
	}

	@Override
	public void deleteTemporary(HotelModel hotelModel) {
		// TODO Auto-generated method stub
		this.hotelDao.update(hotelModel);
	}

	@Override
	public HotelModel searchId(Integer idHotel) {
		// TODO Auto-generated method stub
		return this.hotelDao.searchId(idHotel);
	}

}
