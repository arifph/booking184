package com.spring.booking184.service;

import java.util.List;

import com.spring.booking184.model.FakultasModel;
import com.spring.booking184.model.MaskapaiModel;

public interface MaskapaiService {
	
	public void create(MaskapaiModel maskapaiModel);
	public void update(MaskapaiModel maskapaiModel);
	public void delete(MaskapaiModel maskapaiModel);
	public List<MaskapaiModel> search();
	public MaskapaiModel searchId(Integer idMaskapai);
	public void deleteTemporary(MaskapaiModel maskapaiModel);
	public List<MaskapaiModel> searchKodeEqual(String kodeMaskapai);
	public List<MaskapaiModel> searchNamaEqual(String namaMaskapai);

}
