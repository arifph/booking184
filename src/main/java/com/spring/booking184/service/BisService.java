package com.spring.booking184.service;

import java.util.List;

import com.spring.booking184.model.BisModel;


public interface BisService {
	
	public void create(BisModel bisModel);
	public void update(BisModel bisModel);
	public void delete(BisModel bisModel);
	public List<BisModel> search(String kodeRole);
	public BisModel searchId(Integer idBis);
	public void deleteTemporary(BisModel bisModel);
}
