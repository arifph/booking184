package com.spring.booking184.service;

import java.util.List;

import com.spring.booking184.model.KeretaModel;

public interface KeretaService {
	
	public void create(KeretaModel keretaModel);
	public void update(KeretaModel keretaModel);
//	public void updateByNama(KeretaModel keretaModel, String namaKereta, String kodeKereta, String jenisKereta, String ketKereta, Integer feeKereta);
	public void delete(KeretaModel keretaModel);
	public List<KeretaModel> search();
//	public List<KeretaModel> search(String kodeRole);
	public KeretaModel searchId(Integer idKereta);
	/*public List<KeretaModel> searchNama(String namaKereta);
	public List<KeretaModel> searchKode(String kodeKereta);
	public List<KeretaModel> searchKodeOrNama(String kodeKereta, String namaKereta);*/
	public List<KeretaModel> searchKodeEqual(String kodeKereta);
	public List<KeretaModel> searchNamaEqual(String namaKereta, Integer isDelete);
	public void deleteTemporary(KeretaModel keretaModel);
	
	
	/*public output nama(type input)*/
	/*syntax input tanpa output*/
	/*public void simpan(String kodeKereta);
	public void ubahId(Integer idKereta);
	public void ubahNama(String namaKereta);
	public void cari(String namaKereta);
	
	public void createDua(Integer idKereta, String kodeKereta, String namaKereta);*/ 
	// sama saja dengan method yg create (di atas), hanya saja yg atas lebih OOP. Dan yang ini harus masukkan inputan satu per satu.
	
	/*syntax tanpa input dengan output*/
	/*SELECT * FROM M_FAKULTAS*/ /*= public KeretaModel selectAll();*/
	
	/*syntax input dan output*/
	
	/*SELECT * FROM M_FAKULTAS WHERE NAMA_FAKULTAS = '';*/ 
	/*= public KeretaModel searchNama(String namaKereta);*/
	
	/*SELECT * FROM M_FAKULTAS WHERE NAMA_FAKULTAS = '' AND KODE_FAKULTAS='';*/ 
	/*= public KeretaModel searchNamaAndKode(String namaKereta, String kodeKereta);*/
	
	/*SELECT * FROM M_FAKULTAS WHERE ID_FAKULTAS = '' OR KODE_FAKULTAS='';*/ 
	/*= public KeretaModel searchIdOrKode(Integer idKereta, String kodeKereta);*/
	
}
