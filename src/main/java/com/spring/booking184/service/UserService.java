package com.spring.booking184.service;

import com.spring.booking184.model.UserModel;

public interface UserService {
	public UserModel searchUsernamePassword(String username, String password);
}
