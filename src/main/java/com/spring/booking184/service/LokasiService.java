package com.spring.booking184.service;

import java.util.List;

import com.spring.booking184.model.LokasiModel;

public interface LokasiService {
	public List<LokasiModel> search();
}
