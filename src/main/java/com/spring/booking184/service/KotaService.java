package com.spring.booking184.service;

import java.util.List;

import com.spring.booking184.model.KotaModel;

public interface KotaService {

	public void create(KotaModel kotaModel);

	public void update(KotaModel kotaModel);

	public void delete(KotaModel kotaModel);// sudah meliputi semua <<<

	public List<KotaModel> search();
	
	public KotaModel searchId(Integer idKota);
	
	public List<KotaModel> searchNama(String namaKota);
	
	public List<KotaModel> searchKode(String kodeKota);
	
	public List<KotaModel> searchKodeOrNama(String kodeKota, String namaKota);
	
	public List<KotaModel> searchKodeEqual(String kodeKota);
	
	public List<KotaModel> searchNamaEqual(String namaKota);

	public void deleteTemporary(KotaModel kotaModel);
	/*
	 * public FakultasModel searchNama(String namaFakultas);
	 * 
	 * public FakultasModel searchNamaAndKode(String namaFakultas, String
	 * kodeFakultas);
	 * 
	 * public FakultasModel searchIdOrKode (Integer idFakultas, String
	 * kodeFakultas);
	 */
	
	/*
	 * public output nama(type input) sintax input tanpa output public void
	 * simpan(String kodeFakultas); public void ubah(Integer idFakultas); public
	 * void cari(String namaFakultas);
	 */

	/*
	 * sintax tanpa input dengan output SELECT * FROM M_FAKULTAS << pilih seluruh
	 * kolom dari M_FAKULTAS public FakultasModel selectAll();
	 */

	/*
	 * sintax input dan output 
	 * SELECT * FROM M_FAKULTAS WHERE NAMA_FAKULTAS='';
	 * public FakultasModel searchNama(String namaFakultas);
	 * SELECT * FROM M_FAKULTAS WHERE NAMA_FAKULTAS='' AND KODE_FAKULTAS='';
	 * public FakultasModel searchNamaAndKode(String namaFakultas, String kodeFakultas);
	 * SELECT * FROM M_FAKULTAS WHERE ID_FAKULTAS='' OR KODE_FAKULTAS='';
	 * public FakultasModel searchIdOrKode (Integer idFakultas, String kodeFakultas); 
	 */
	
}
