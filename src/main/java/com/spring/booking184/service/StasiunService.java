package com.spring.booking184.service;

import java.util.List;

import com.spring.booking184.model.StasiunModel;

public interface StasiunService {

	public void save(StasiunModel stasiunModel);
	public void update(StasiunModel stasiunModel);
	public void delete(StasiunModel stasiunModel);
	public void deleteTemporary(StasiunModel stasiunModel);
	public List<StasiunModel> search();
	public List<StasiunModel> searchWithDeleted();
	public StasiunModel searchByIdStasiun(Integer idStasiun);
}
