package com.spring.booking184.dao;

import com.spring.booking184.model.UserModel;

public interface UserDao {
	public UserModel searchUsernamePassword(String username, String password);
}
