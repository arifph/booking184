package com.spring.booking184.dao;

import java.util.List;

import com.spring.booking184.model.LokasiModel;

public interface LokasiDao {

	public List<LokasiModel> search();

}
