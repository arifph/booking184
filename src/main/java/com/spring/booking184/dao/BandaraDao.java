package com.spring.booking184.dao;

import java.util.List;

import com.spring.booking184.model.BandaraModel;

public interface BandaraDao {

	public void create(BandaraModel bandaraModel);
	public void update(BandaraModel bandaraModel);
	public void delete(BandaraModel bandaraModel);
	public List<BandaraModel> search(String kodeRole);
	public BandaraModel searchId(Integer idBandara);
}
