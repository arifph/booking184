package com.spring.booking184.dao;

import java.util.List;

import com.spring.booking184.model.MaskapaiModel;

public interface MaskapaiDao {
	
	public void create(MaskapaiModel maskapaiModel);
	public void update(MaskapaiModel maskapaiModel);
	public void delete(MaskapaiModel maskapaiModel);
	public List<MaskapaiModel> search();
	public MaskapaiModel searchId(Integer idMaskapai);
	public List<MaskapaiModel> searchKodeEqual(String kodeMaskapai);
	public List<MaskapaiModel> searchNamaEqual(String namaMaskapai);

	
}
