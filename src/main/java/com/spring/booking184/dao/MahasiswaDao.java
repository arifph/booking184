package com.spring.booking184.dao;

import java.util.List;

import com.spring.booking184.model.MahasiswaModel;

public interface MahasiswaDao {

	//polanya: public Output namaService(Input);
	
	public void create(MahasiswaModel mahasiswaModel);
	public List<MahasiswaModel> list(); // list itu search all data
	public MahasiswaModel searchById(Integer idMahasiswa);
	public void delete(MahasiswaModel mahasiswaModel);
	public void update(MahasiswaModel mahasiswaModel);
	public List<MahasiswaModel> searchByNim(String nimMahasiswa);
	
	public MahasiswaModel searchByNamaPasti(String namaMahasiswa);
	public List<MahasiswaModel> searchByNamaLikeDepan(String namaMahasiswa);
	public List<MahasiswaModel> searchByNamaLikeAkhir(String namaMahasiswa);
	public List<MahasiswaModel> searchByNamaLikeOrTahunLahir(String namaMahasiswa, Integer tahunLahirMahasiswa);

	public MahasiswaModel searchByTeleponSingle(String teleponMahasiswa);
	public List<MahasiswaModel> searchByTeleponList(String teleponMahasiswa);
}
