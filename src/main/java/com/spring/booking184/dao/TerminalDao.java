package com.spring.booking184.dao;

import java.util.List;

import com.spring.booking184.model.TerminalModel;

public interface TerminalDao {

	public void create(TerminalModel terminalModel);
	public void update(TerminalModel terminalModel);
	public void delete(TerminalModel terminalModel);
	public List<TerminalModel> search();
	public TerminalModel searchId(Integer idTerminal);
	public List<TerminalModel> searchNama(String namaTerminal);
	public List<TerminalModel> searchKode(String kodeTerminal);
	public List<TerminalModel> searchKodeOrNama(String kodeTerminal, String namaTerminal);
	public List<TerminalModel> searchKodeEqual(String kodeTerminal);
	public List<TerminalModel> searchNamaEqual(String namaTerminal);
	public void deleteTemporary(TerminalModel terminalModel);
}
