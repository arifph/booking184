package com.spring.booking184.dao;

import java.util.List;

import com.spring.booking184.model.KotaModel;

public interface KotaDao {
	
	public void create(KotaModel kotaModel); 

	public void update(KotaModel kotaModel);

	public void delete(KotaModel kotaModel);// sudah meliputi semua <<<

	public List<KotaModel> search();
	
	public KotaModel searchId(Integer idKota);
	
	public List<KotaModel> searchNama(String namaKota);
	
	public List<KotaModel> searchKode(String kodeKota);
	
	public List<KotaModel> searchKodeOrNama(String kodeKota, String namaFakultas);
	
	public List<KotaModel> searchKodeEqual(String kodeKota);
	
	public List<KotaModel> searchNamaEqual(String namaKota);
	
}
