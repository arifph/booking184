package com.spring.booking184.dao;

import java.util.List;

import com.spring.booking184.model.KeretaModel;

public interface KeretaDao {
	
	// Yang ada di KeretaService harus ada di KeretaDao
	public void create(KeretaModel keretaModel);
	public void update(KeretaModel keretaModel);
//	public void updateByNama(KeretaModel keretaModel, String namaKereta, String kodeKereta, String jenisKereta, String ketKereta, Integer feeKereta);
	public void delete(KeretaModel keretaModel);
	public List<KeretaModel> search();
//	public List<KeretaModel> search(String kodeRole);
	public KeretaModel searchId(Integer idKereta);
	/*public List<KeretaModel> searchNama(String namaKereta);
	public List<KeretaModel> searchKode(String kodeKereta);
	public List<KeretaModel> searchKodeOrNama(String kodeKereta, String namaKereta);*/
	public List<KeretaModel> searchKodeEqual(String kodeKereta);
	public List<KeretaModel> searchNamaEqual(String namaKereta, Integer isDelete);
}
