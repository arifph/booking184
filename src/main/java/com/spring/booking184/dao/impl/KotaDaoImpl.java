package com.spring.booking184.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.booking184.dao.KotaDao;
import com.spring.booking184.model.KotaModel;

@Repository
public class KotaDaoImpl implements KotaDao{

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void create(KotaModel kotaModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.save(kotaModel);
	}

	@Override
	public void update(KotaModel kotaModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.update(kotaModel);	
	}

	@Override
	public void delete(KotaModel kotaModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(kotaModel);
	}

	@Override
	public List<KotaModel> search() {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession(); //syntax start query
		List<KotaModel> kotaModelList = new ArrayList<KotaModel>(); //instance return
		String query = "from KotaModel";
		String kondisi=" where isDelete !=1 "; //mengahapus temporary muncul kondisi ini
		kotaModelList = session.createQuery(query+kondisi, KotaModel.class).list();
		
		return kotaModelList;
	}

	@Override
	public KotaModel searchId(Integer idKota) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession(); //1.syntax start query
		
		KotaModel kotaModel = new KotaModel(); //2.instance return
		String query = " from KotaModel where idKota="+idKota+" ";  //<<karena id integer tidak pake petik ''
		kotaModel = (KotaModel) session.createQuery(query).uniqueResult(); //<< isinya satu pake unique result
		
		return kotaModel;
	}

	@Override
	public List<KotaModel> searchNama(String namaKota) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession(); //1. syntax start query
		List<KotaModel> kotaModelList = new ArrayList<KotaModel>(); //2. instance return
		
		String query = " from KotaModel where namaKota like '%"+namaKota+"%' "; //3. query
		kotaModelList = session.createQuery(query).list(); //4. hibernate 
		return kotaModelList;
	}

	@Override
	public List<KotaModel> searchKode(String kodeKota) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<KotaModel> kotaModelList = new ArrayList<KotaModel>();
		
		String query = " from KotaModel where kodeKota like '%"+kodeKota+"%' ";
		kotaModelList = session.createQuery(query).list();
		return kotaModelList;
	}

	@Override
	public List<KotaModel> searchKodeOrNama(String kodeKota, String namaKota) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<KotaModel> kotaModelList = new ArrayList<KotaModel>();
		
		String query = " from KotaModel "
						+ "where kodekota like '%"+kodeKota+"%' "
						+ "or namaKota like '%"+namaKota+"%'  ";
		kotaModelList = session.createQuery(query).list();
		return kotaModelList;
	}

	@Override
	public List<KotaModel> searchKodeEqual(String kodeKota) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<KotaModel> kotaModelList = new ArrayList<KotaModel>();
		
		String query = " from KotaModel where kodeKota = '"+kodeKota+"' ";
		kotaModelList = session.createQuery(query).list();
		return kotaModelList;
	}

	@Override
	public List<KotaModel> searchNamaEqual(String namaKota) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<KotaModel> kotaModelList = new ArrayList<KotaModel>();
		
		String query = " from KotaModel where namaKota = '"+namaKota+"' ";
		kotaModelList = session.createQuery(query).list();
		return kotaModelList;
	}


}
