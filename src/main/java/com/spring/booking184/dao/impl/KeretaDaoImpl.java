package com.spring.booking184.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.booking184.dao.KeretaDao;
import com.spring.booking184.model.KeretaModel;

@Repository
public class KeretaDaoImpl implements KeretaDao{
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void create(KeretaModel keretaModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.save(keretaModel); // .save itu fungsi untuk insert into
//		session.saveOrUpdate(keretaModel);
	}

	@Override
	public void update(KeretaModel keretaModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.update(keretaModel);
	}
	
//	@Override
//	public void updateByNama(KeretaModel keretaModel, String namaKereta, String kodeKereta, String jenisKereta, String ketKereta, Integer feeKereta) {
//		// TODO Auto-generated method stub
//		Session session = this.sessionFactory.getCurrentSession();
////		session.update(keretaModel);
////		String query = " from KeretaModel where namaKereta = " + namaKereta + " ";
////		session.update(namaKereta, keretaModel);
//		Query query = session.createQuery("update " + keretaModel + ""
//						+ " set "
//						+ "kodeKereta = :kodeKereta, "
//						+ "jenisKereta = :jenisKereta, "
//						+ "ketKereta = :ketKereta, "
//						+ "feeKereta = :feeKereta, "
//						+ "isDelete = :isDelete "
//						+ "where namaKereta = :namaKereta");
//		
////		Query query = session.createQuery("update M_KERETA"
////				+ " set "
////				+ "kodeKereta = :kodeKereta, "
////				+ "jenisKereta = :jenisKereta, "
////				+ "ketKereta = :ketKereta, "
////				+ "feeKereta = :feeKereta, "
////				+ "isDelete = :isDelete "
////				+ "where namaKereta = :namaKereta");
//
//		
//		query.setParameter("kodeKereta", kodeKereta);
//		query.setParameter("namaKereta", namaKereta);
//		query.setParameter("jenisKereta", jenisKereta);
//		query.setParameter("ketKereta", ketKereta);
//		query.setParameter("feeKereta", feeKereta);
//		query.setParameter("isDelete", 0);
//		
//		query.executeUpdate();
//	}

	@Override
	public void delete(KeretaModel keretaModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(keretaModel);
	}
	
//	@Override
//	public void deleteByNama(KeretaModel keretaModel, String namaKereta) {
//		// TODO Auto-generated method stub
//		Session session = this.sessionFactory.getCurrentSession();
//		Query query = session.createQuery("delete " + keretaModel + " where namaKereta = " + namaKereta);
//	}

	@Override
	public List<KeretaModel> search() {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		
		List<KeretaModel> keretaModelList = new ArrayList<KeretaModel>();
		String query = " from KeretaModel";
		String kondisi = " where isDelete != 1";
		keretaModelList = session.createQuery(query+kondisi).list();
		
		return keretaModelList;
	}
	
	/*@Override
	public List<KeretaModel> search(String kodeRole) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		
		List<KeretaModel> keretaModelList = new ArrayList<KeretaModel>();
		String query = " from KeretaModel";
		String kondisi = " where isDelete != 1";
		
		if (kodeRole.equals("ROLE_ADMIN")) {
			kondisi = " ";
		} else {
			kondisi = " where isDelete != 1";
		}
		
		keretaModelList = session.createQuery(query+kondisi).list();
		
		return keretaModelList;
	}*/

	@Override
	public KeretaModel searchId(Integer idKereta) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		
		KeretaModel keretaModel = new KeretaModel();
		String query = " from KeretaModel where idKereta=" + idKereta + " ";
		keretaModel = (KeretaModel) session.createQuery(query).uniqueResult();
		
		return keretaModel;
	}

	/*@Override
	public List<KeretaModel> searchNama(String namaKereta) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession(); // syntax start query
		
		List<KeretaModel> keretaModelList = new ArrayList<KeretaModel>(); // instance return
		String query = " from KeretaModel where "
				+ "upper(namaKereta) like upper('%" + namaKereta + "%')";
		keretaModelList = session.createQuery(query).list();
		
		return keretaModelList;
	}

	@Override
	public List<KeretaModel> searchKode(String kodeKereta) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession(); // syntax start query
		
		List<KeretaModel> keretaModelList = new ArrayList<KeretaModel>(); // instance return
		String query = " from KeretaModel where "
				+ "upper(kodeKereta) like upper('%" + kodeKereta + "%')";
		keretaModelList = session.createQuery(query).list();
		
		return keretaModelList;
	}
	
	@Override
	public List<KeretaModel> searchKodeOrNama(String kodeKereta, String namaKereta) {
		// // TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession(); // syntax start query
		
		List<KeretaModel> keretaModelList = new ArrayList<KeretaModel>(); // instance return
		String query = " from KeretaModel "
				+ "where upper(kodeKereta) like upper('%" + kodeKereta + "%') "
				+ "or upper(namaKereta) like upper('%" + namaKereta + "%')";
		keretaModelList = session.createQuery(query).list();
		
		return keretaModelList;
	}*/

	@Override
	public List<KeretaModel> searchKodeEqual(String kodeKereta) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession(); // syntax start query
		
		List<KeretaModel> keretaModelList = new ArrayList<KeretaModel>(); // instance return
		String query = " from KeretaModel where"
						+ " upper(kodeKereta) = upper('" + kodeKereta + "')"
						+ " and isDelete != 1 ";
		keretaModelList = session.createQuery(query).list();
		
		return keretaModelList;
	}

	@Override
	public List<KeretaModel> searchNamaEqual(String namaKereta, Integer isDelete) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession(); // syntax start query
		
		List<KeretaModel> keretaModelList = new ArrayList<KeretaModel>(); // instance return
		String query = " from KeretaModel where "
					+ "upper(namaKereta) = upper('" + namaKereta + "')"
					+ " and isDelete = " + isDelete + " ";
		keretaModelList = session.createQuery(query).list();
		
		return keretaModelList;
	}
}
