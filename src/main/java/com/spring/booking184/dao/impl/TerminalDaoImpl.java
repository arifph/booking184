package com.spring.booking184.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.booking184.dao.TerminalDao;
import com.spring.booking184.model.TerminalModel;

@Repository
public class TerminalDaoImpl implements TerminalDao{
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void create(TerminalModel terminalModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.save(terminalModel);
	}

	@Override
	public void update(TerminalModel terminalModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.update(terminalModel);
	}

	@Override
	public void delete(TerminalModel terminalModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(terminalModel);	
	}

	@Override
	public List<TerminalModel> search() {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<TerminalModel> terminalModelList = new ArrayList<TerminalModel>();
		
		String query = " from TerminalModel ";
		String kondisi = " where isDelete !=1 ";
		
		terminalModelList = session.createQuery(query+kondisi).list();
		
		return terminalModelList;
	}

	@Override
	public TerminalModel searchId(Integer idTerminal) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		
		TerminalModel terminalModel = new TerminalModel();
		String query = " from TerminalModel where idTerminal="+idTerminal+" ";
		terminalModel = (TerminalModel) session.createQuery(query).uniqueResult();
		
		return terminalModel;
	}

	@Override
	public List<TerminalModel> searchNama(String namaTerminal) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<TerminalModel> terminalModelList = new ArrayList<TerminalModel>();
		String query = " from TerminalModel where namaTerminal like '%"+namaTerminal+"%' ";
		terminalModelList = session.createQuery(query).list();
		
		return terminalModelList;
	}

	@Override
	public List<TerminalModel> searchKode(String kodeTerminal) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<TerminalModel> terminalModelList = new ArrayList<TerminalModel>();
		String query = " from TerminalModel where kodeTerminal like '%"+kodeTerminal+"%' ";
		terminalModelList = session.createQuery(query).list();
		
		return terminalModelList;
	}

	@Override
	public List<TerminalModel> searchKodeOrNama(String kodeTerminal, String namaTerminal) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<TerminalModel> terminalModelList = new ArrayList<TerminalModel>();
		String query = " from TerminalModel where kodeTerminal like '%"+kodeTerminal+"%' "
						+ " OR namaTerminal like '%"+namaTerminal+"%' ";
		terminalModelList = session.createQuery(query).list();
		return terminalModelList;
	}

	@Override
	public List<TerminalModel> searchKodeEqual(String kodeTerminal) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<TerminalModel> terminalModelList = new ArrayList<TerminalModel>();
		String query = " from TerminalModel "
				+ " where kodeTerminal = '"+kodeTerminal+"' ";
		terminalModelList= session.createQuery(query).list();
		return terminalModelList;

	}

	@Override
	public List<TerminalModel> searchNamaEqual(String namaTerminal) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<TerminalModel> terminalModelList = new ArrayList<TerminalModel>();
		String query = " from TerminalModel "
				+ " where namaTerminal = '"+namaTerminal+"' ";
		terminalModelList= session.createQuery(query).list();
		return terminalModelList;
	}

	@Override
	public void deleteTemporary(TerminalModel terminalModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.update(terminalModel);
	}

		

}
