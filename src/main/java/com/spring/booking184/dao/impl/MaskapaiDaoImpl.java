package com.spring.booking184.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.booking184.dao.MaskapaiDao;
import com.spring.booking184.model.FakultasModel;
import com.spring.booking184.model.MaskapaiModel;

@Repository
public class MaskapaiDaoImpl implements MaskapaiDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void create(MaskapaiModel maskapaiModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.save(maskapaiModel);
	}

	@Override
	public List<MaskapaiModel> search() {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<MaskapaiModel> maskapaiModelList = new ArrayList<MaskapaiModel>();
		
		String query = " from MaskapaiModel where isDelete != 1";
		maskapaiModelList = session.createQuery(query).list();
		return maskapaiModelList;
	}

	@Override
	public MaskapaiModel searchId(Integer idMaskapai) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		
		MaskapaiModel maskapaiModel = new MaskapaiModel();
		String query = " from MaskapaiModel where idMaskapai="+idMaskapai+" ";
		maskapaiModel = (MaskapaiModel) session.createQuery(query).uniqueResult(); //untuk 1 data (unique result)
		
		return maskapaiModel;
	}

	@Override
	public void update(MaskapaiModel maskapaiModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.update(maskapaiModel);
	}

	@Override
	public void delete(MaskapaiModel maskapaiModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.update(maskapaiModel);
	}

	@Override
	public List<MaskapaiModel> searchKodeEqual(String kodeMaskapai) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession(); //sintaks u/ memulai membuat query
		List<MaskapaiModel> maskapaiModelList = new ArrayList<MaskapaiModel>(); //instance return
		String query=" from MaskapaiModel where upper(kodeMaskapai) = upper('"+kodeMaskapai+"') and isDelete != 1 "; //karena string pakai petik
		//pilih semua dari tabel fakultas dimana colom nama fakultasnya mengandung kata kunci (pake hql)
		
		//bikin hibernate
		maskapaiModelList = session.createQuery(query).list();
		
		return maskapaiModelList;	}

	@Override
	public List<MaskapaiModel> searchNamaEqual(String namaMaskapai) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession(); //sintaks u/ memulai membuat query
		List<MaskapaiModel> maskapaiModelList = new ArrayList<MaskapaiModel>(); //instance return
		String query=" from MaskapaiModel where upper(namaMaskapai) = upper('"+namaMaskapai+"') and isDelete != 1 "; //karena string pakai petik
		//pilih semua dari tabel fakultas dimana colom nama fakultasnya mengandung kata kunci (pake hql)
		
		//bikin hibernate
		maskapaiModelList = session.createQuery(query).list();
		
		return maskapaiModelList;	}
	
	
}
