package com.spring.booking184.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.booking184.dao.StasiunDao;
import com.spring.booking184.model.StasiunModel;

@Repository
public class StasiunDaoImpl implements StasiunDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void save(StasiunModel stasiunModel) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		session.save(stasiunModel);
	}

	@Override
	public void update(StasiunModel stasiunModel) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		session.update(stasiunModel);
	}

	@Override
	public void delete(StasiunModel stasiunModel) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		session.delete(stasiunModel);
	}

	@Override
	public List<StasiunModel> search() {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		String query = "from StasiunModel where isDeleted = 0";
		
		return session.createQuery(query, StasiunModel.class).list();
	}

	@Override
	public StasiunModel searchByIdStasiun(Integer idStasiun) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		String query = "from StasiunModel where idStasiun = " + idStasiun;
		
		return session.createQuery(query, StasiunModel.class).uniqueResult();
	}

	@Override
	public List<StasiunModel> searchWithDeleted() {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		String query = "from StasiunModel";
		
		return session.createQuery(query, StasiunModel.class).list();
	}

}
