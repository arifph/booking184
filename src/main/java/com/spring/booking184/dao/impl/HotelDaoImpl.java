package com.spring.booking184.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.booking184.dao.HotelDao;
import com.spring.booking184.model.HotelModel;

@Repository
public class HotelDaoImpl implements HotelDao {
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void create(HotelModel hotelModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.save(hotelModel);
	}

	@Override
	public void update(HotelModel hotelModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.update(hotelModel);
	}

	@Override
	public void delete(HotelModel hotelModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(hotelModel);
	}

	@Override
	public List<HotelModel> search(String kodeRole) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<HotelModel> hotelModelList = new ArrayList<HotelModel>();
		
		String query = " from HotelModel ";
		String kondisi = " where isDelete !=1";
		
		if (kodeRole.equals("ROLE_ADMIN")) {
			kondisi = " ";
		} else {

		}
		hotelModelList = session.createQuery(query + kondisi).list();
		
		return hotelModelList;
	}

	@Override
	public List<HotelModel> searchKodeEqual(String kodeHotel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<HotelModel> hotelModelList = new ArrayList<HotelModel>();
		
		String query = " from HotelModel where upper(kodeHotel) = upper('"+kodeHotel+"') ";
		
		hotelModelList = session.createQuery(query).list();
		return hotelModelList;
	}

	@Override
	public List<HotelModel> searchNamaEqual(String namaHotel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<HotelModel> hotelModelList = new ArrayList<HotelModel>();
		
		String query = " from HotelModel where upper(namaHotel) = upper('"+namaHotel+"') ";
		
		hotelModelList = session.createQuery(query).list();
		return hotelModelList;
	}

	@Override
	public HotelModel searchId(Integer idHotel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		HotelModel hotelModel = new HotelModel();
		String query = " from HotelModel where idHotel= "+idHotel+" ";
		hotelModel = (HotelModel) session.createQuery(query).uniqueResult();
		
		return hotelModel;
	}


}
