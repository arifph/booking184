package com.spring.booking184.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.booking184.dao.BisDao;
import com.spring.booking184.model.BisModel;

@Repository
public class BisDaoImpl implements BisDao{
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void create(BisModel bisModel) {
		// TODO Auto-generated method stub
		Session session= this.sessionFactory.getCurrentSession(); //syntax start query
		session.save(bisModel); //HIbernate @ HQL (sama dengan SQL tapi beda bahasa)
	}

	@Override
	public void update(BisModel bisModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.update(bisModel);
	}

	@Override
	public void delete(BisModel bisModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(bisModel);
	}

	@Override
	public List<BisModel> search(String kodeRole) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<BisModel> bisModelList = new ArrayList<BisModel>();
		String query = " from BisModel";
		String kondisi = "";
		
		if(kodeRole.equals("ROLE_ADMIN")) {
			kondisi ="";
		}
		else {
			kondisi = " where isDeleted !=1";
		}
		
		bisModelList = session.createQuery(query+kondisi).list();
		return bisModelList;
	}

	@Override
	public BisModel searchId(Integer idBis) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		BisModel bisModel = new BisModel();
		String query = " from BisModel where idBis ='"+ idBis +"'";
		bisModel = (BisModel) session.createQuery(query).uniqueResult();
		return bisModel;
	}

}
