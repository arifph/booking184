package com.spring.booking184.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.booking184.dao.BandaraDao;
import com.spring.booking184.model.BandaraModel;

@Repository
public class BandaraDaoImpl implements BandaraDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void create(BandaraModel bandaraModel) {                                    
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.save(bandaraModel);
	}

	@Override
	public void update(BandaraModel bandaraModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.update(bandaraModel);
	}

	@Override
	public void delete(BandaraModel bandaraModel) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(bandaraModel);
	}

	@Override
	public List<BandaraModel> search(String kodeRole) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		
		List<BandaraModel> bandaraModelList = new ArrayList<BandaraModel>();
		String query = " from BandaraModel ";
		String kondisi= " where isDelete != 1";
		
		if(kodeRole.equals("ROLE_ADMIN")) {
			kondisi = " ";
		} else {
			
		}
		
		bandaraModelList = session.createQuery(query + kondisi).list();
		return bandaraModelList;
	}

	@Override
	public BandaraModel searchId(Integer idBandara) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		
		BandaraModel bandaraModel = new BandaraModel();
		String query = " from BandaraModel where idBandara="+idBandara+" ";
		bandaraModel = (BandaraModel) session.createQuery(query).uniqueResult();
		
		return bandaraModel;
	}

}
