<div class="panel"
	style="background: white; margin-top: 50px; min-height: 500px">
	<h1>Halaman Hotel</h1>
	

	<div>
		<table class="table" id="tbl-hotel">
			<tr>
				<td>Data Hotel</td>
				<td></td>
				<td></td>
				<td align="right"><button type="button" class="btn btn-primary" id="btn-add">Tambah</button></td>
			</tr>
			<tr>
				<td>Kode Hotel</td>
				<td>Nama Hotel</td>
				<td>Kota</td>
				<td>Aksi</td>
			</tr>

			<tbody id="list-hotel">

			</tbody>
		</table>
	</div>
</div>

<div id="modal-input" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<i class="fa fa-close"></i>
				</button>
				<h4>Form PopUp Hotel</h4>
			</div>
			<div class="modal-body">
				<!--tempat jsp popupnya-->
			</div>
		</div>
	</div>
</div>

<script>

	loadListHotel();
	
	function loadListHotel() {
		$.ajax({
			url : 'hotel/cruds/list.html',
			type : 'get',
			dataType : 'html',
			success : function(result) {
				$('#list-hotel').html(result);
			}
		});
	}
	
	//document ready itu setelah halamannya dipanggil
	$(document).ready(
			function() {
	
				//fungsi ajax untuk popup tambah
				$('#btn-add').on('click', function() {
					$.ajax({
						url : 'hotel/cruds/add.html',
						type : 'get',
						dataType : 'html',
						success : function(result) {
							$('#modal-input').find('.modal-body').html(result);
							$('#modal-input').modal('show');
						}
					});
				})
				
				$('#modal-input').on('submit','#form-hotel-add',function() {
							$.ajax({
								url : 'hotel/cruds/create.json',
								type : 'get',
								dataType : 'json',
								data : $(this).serialize(),
								success : function(result) {

									if (result.jumlahDataHotel > 0) {
										alert("Kode Hotel "
												+ result.kodeHotel
												+ " Sudah ada di Database");
									} else {
										$('#modal-input').modal('hide');
										alert("data telah ditambah");
										loadListHotel();
									}
								}
							});
							return false; //supaya tidak merefresh ulang semua data, jadi data ditambah saja
					});
				
				//fungsi ajax untuk lihat detail
				$('#list-hotel').on('click', '#btn-detail', function() {
					var idHotel = $(this).val(); //ambil dari list.jsp hanya 1 nilai
					$.ajax({
						url : 'hotel/cruds/detail.html',
						type : 'get',
						dataType : 'html',
						data : {
							idHotel : idHotel
						},
						success : function(result) {
							$('#modal-input').find('.modal-body').html(result);
							$('#modal-input').modal('show');
						}
					});
				});

				//edit popup btn
				$('#list-hotel').on('click', '#btn-edit', function() {
					var idHotel = $(this).val();
					$.ajax({
						url : 'hotel/cruds/edit.html',
						type : 'get',
						dataType : 'html',
						data : {
							idHotel : idHotel
						},
						success : function(result) {
							$('#modal-input').find('.modal-body').html(result);
							$('#modal-input').modal('show');
						}
					});
				});

				//fungsi ajax untuk update di dalam popup update
				$('#modal-input').on('submit', '#form-hotel-edit',
						function() {
							$.ajax({
								url : 'hotel/cruds/update.json',
								type : 'get',
								dataType : 'json',
								data : $(this).serialize(),
								success : function(result) {
									$('#modal-input').modal('hide');
									alert("data telah diubah");
									loadListHotel();
								}
							});
							return false;
						});

				//hapus popup btn
				$('#list-hotel').on('click', '#btn-delete', function() {
					var idHotel = $(this).val();
					$.ajax({
						url : 'hotel/cruds/remove.html',
						type : 'get',
						dataType : 'html',
						data : {
							idHotel : idHotel
						},
						success : function(result) {
							$('#modal-input').find('.modal-body').html(result);
							$('#modal-input').modal('show');
						}
					});
				});
				
				//fungsi ajax untuk delete di dalam popup delete
				$('#modal-input').on('submit', '#form-hotel-delete',
						function() {
							$.ajax({
								url : 'hotel/cruds/delete.json',
								type : 'get',
								dataType : 'json',
								data : $(this).serialize(),
								success : function(result) {
									$('#modal-input').modal('hide');
									alert("data telah dihapus");
									loadListHotel();
								}
							});
							return false;
						});	
				
		});

//akhir dokumen ready
</script>