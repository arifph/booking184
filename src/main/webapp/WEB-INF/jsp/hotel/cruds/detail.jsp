<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<div class="form-horizontal">
<h1>Lihat Data Hotel</h1>
	<form action="#" method="get" id="form-hotel-detail">
		<table>
			<tr>
				<td>Kode Hotel</td>
				<td><input type="text" id="kodeHotel" name="kodeHotel" value="${hotelModel.kodeHotel}"/></td>
			</tr>
			<tr>
				<td>Nama Hotel</td>
				<td><input type="text" id="namaHotel" name="namaHotel" value="${hotelModel.namaHotel}"/></td>
			</tr>
			<tr>
				<td>Bintang</td>
				<td>
					<select id="bintang" name="bintang">
							<option value="${hotelModel.bintang}"> 
								${hotelModel.bintang}
							</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>Kota</td>
				<td>
					<select id="idKota" name="idKota">
						<c:forEach items="${kotaModelList }" var="kotaModel">
							<option value="${kotaModel.idKota}">
								${kotaModel.namaKota}
							</option>
						</c:forEach>
					</select>
				
				</td>
			</tr>
			<tr>
				<td>Telp</td>
				<td><input type="text" id="telp" name="telp" value="${hotelModel.telp}"/></td>
			</tr>
		
			<tr>
				<td>Email</td>
				<td><input type="text" id="email" name="email" value="${hotelModel.email}"/></td>
			</tr>
			<tr>
				<td>Alamat Hotel</td>
				<td><input type="text" id="alamatHotel" name="alamatHotel" value="${hotelModel.alamatHotel}"/></td>
			</tr>
		
			<tr>
				<td>Fee Agen</td>
				<td><input type="text" id="feeAgen" name="feeAgen" value="${hotelModel.feeAgen}"/></td>
			</tr>
			
			<tr>
				<td>Created By: </td>
				<td>${hotelModel.xCreatedBy.username }</td>
			</tr>
			
			<tr>
				<td>Created Date: </td>
				<td>${hotelModel.xCreatedDate }</td>
			</tr>
			
			<tr>
				<td>Updated By: </td>
				<td>${hotelModel.xUpdatedBy.username}</td>
			</tr>
			
			<tr>
				<td>Updated Date: </td>
				<td>${hotelModel.xUpdatedDate }</td>
			</tr>
			
			<tr>
				<td>Deleted By: </td>
				<td>${hotelModel.xDeletedBy.username}</td>
			</tr>
			
			<tr>
				<td>Deleted Date: </td>
				<td>${hotelModel.xDeletedDate}</td>
			</tr>
		</table>
	
	</form>
</div>