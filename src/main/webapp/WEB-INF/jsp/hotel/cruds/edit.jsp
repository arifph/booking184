<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<div class="form-horizontal">
<h1>Update Data Hotel</h1>
	<form action="#" method="get" id="form-hotel-edit">
	<input type="hidden" id="idHotel" name="idHotel" value="${hotelModel.idHotel}">
		<table>
			<tr>
				<td>Kode Hotel</td>
				<td><input type="text" id="kodeHotel" name="kodeHotel" value="${hotelModel.kodeHotel}"/></td>
			</tr>
			<tr>
				<td>Nama Hotel</td>
				<td><input type="text" id="namaHotel" name="namaHotel" value="${hotelModel.namaHotel}"/></td>
			</tr>
			<tr>
				<td>Bintang</td>
				<td>
					<select id="bintang" name="bintang">
							<option value="${hotelModel.bintang}"> 
								${hotelModel.bintang}
							</option>
							<option value="1"> 1 </option>
							<option value="2"> 2 </option>
							<option value="3"> 3 </option>
							<option value="4"> 4 </option>
							<option value="5"> 5 </option>
					</select>
				</td>
			</tr>
			<tr>
				<td>Kota</td>
				<td>
					<select id="idKota" name="idKota">
						<c:forEach items="${kotaModelList }" var="kotaModel">
							<option value="${kotaModel.idKota}"
								${kotaModel.idKota == hotelModel.idKota ? 'selected="true"':''}>
								${kotaModel.namaKota}
							</option>
						</c:forEach>
					</select>
				
				</td>
			</tr>
			<tr>
				<td>Telp</td>
				<td><input type="text" id="telp" name="telp" value="${hotelModel.telp}"/></td>
			</tr>
		
			<tr>
				<td>Email</td>
				<td><input type="text" id="email" name="email" value="${hotelModel.email}"/></td>
			</tr>
			<tr>
				<td>Alamat Hotel</td>
				<td><input type="text" id="alamatHotel" name="alamatHotel" value="${hotelModel.alamatHotel}"/></td>
			</tr>
		
			<tr>
				<td>Fee Agen</td>
				<td><input type="text" id="feeAgen" name="feeAgen" value="${hotelModel.feeAgen}"/></td>
			</tr>
			<tr>
				<td></td>
				<td><button type="submit">Update</button></td>
			</tr>	
			
		</table>
	
	</form>
</div>