<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<div class="form-horizontal">
<h1>Tambah Fakultas</h1>
	<form action="#" method="get" id="form-hotel-add">
		<table>
			<tr>
				<td>Kode Hotel</td>
				<td><input type="text" id="kodeHotel" name="kodeHotel" value="${kodeHotelAuto}"/></td>
			</tr>
			<tr>
				<td>Nama Hotel</td>
				<td><input type="text" id="namaHotel" name="namaHotel"/></td>
			</tr>
			<tr>
				<td>Bintang</td>
				<td>
					<select id="bintang" name="bintang">
							<option value="1"> 1 </option>
							<option value="2"> 2 </option>
							<option value="3"> 3 </option>
							<option value="4"> 4 </option>
							<option value="5"> 5 </option>
					</select>
				</td>
			</tr>
			<tr>
				<td>Kota</td>
				<td>
					<select id="idKota" name="idKota">
						<c:forEach items="${kotaModelList }" var="kotaModel">
							<option value="${kotaModel.idKota}">
								${kotaModel.namaKota}
							</option>
						</c:forEach>
					</select>
				</td>
			</tr>
			<tr>
				<td>Telp</td>
				<td><input type="text" id="telp" name="telp"/></td>
			</tr>
		
			<tr>
				<td>Email</td>
				<td><input type="text" id="email" name="email"/></td>
			</tr>
			<tr>
				<td>Alamat Hotel</td>
				<td><input type="text" id="alamatHotel" name="alamatHotel"/></td>
			</tr>
		
			<tr>
				<td>Fee Agen</td>
				<td><input type="text" id="feeAgen" name="feeAgen"/></td>
			</tr>
			
			<tr>
				<td></td>
				<td><button type="submit">Create</button></td>
			</tr>
		</table>
	
	
	</form>
</div>