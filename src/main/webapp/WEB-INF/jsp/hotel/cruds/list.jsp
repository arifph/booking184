<%@ taglib uri = "http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:forEach items="${hotelModelList}" var="hotelModel">
	<tr>
		<td>${hotelModel.kodeHotel}</td>
		<td>${hotelModel.namaHotel}</td>
		<td>${hotelModel.kotaModel.namaKota}</td>
		<td>
			<button type="button" class="btn btn-success" value="${hotelModel.idHotel}" id="btn-edit">Ubah</button>
			<button type="button" class="btn btn-danger" value="${hotelModel.idHotel}" id="btn-delete">Delete</button>
			<button type="button" class="btn btn-warning" value="${hotelModel.idHotel}" id="btn-detail">Lihat</button>
		</td>

	</tr>
</c:forEach>