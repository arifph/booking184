<div class="panel" style="background: white; margin-top: 50px; min-height:500px;">
	<h1>Data Kereta</h1>
	<div align="right">
		<button type="button" class="btn btn-primary" id="btn-add">Tambah</button>
	</div>
	
	<div>
		<table class="table" id="tbl-kereta">
			<!-- <tr>
				<td></td>
				<td></td>
				<td align="right">Cari kode / nama: </td>
				<td>
					<form action="#" method="get" id="form-kereta-search">
						<input type="text" id="keyword" name="keyword"/>
						<button type="submit">Cari</button>
					</form>
				</td>
			</tr> -->
			<!-- <tr>
				<td>Data Kereta</td>
				<td></td>
				<td></td>
				<td></td>
			</tr> -->
			<tr>
				<td>Kode Kereta</td>
				<td>Nama Kereta</td>
				<td>Jenis</td>
				<td>Action</td>
			</tr>
			<!-- <tr>
				<td></td>
				<td>
					<form action="#" method="get" id="form-kereta-search-kode">
					<input type="text" id="kodeKeretaKey" name="kodeKeretaKey"/>
					<br/>
					<button type="submit">Cari</button>
				</form>
				</td>
				<td>
				<form action="#" method="get" id="form-kereta-search-nama">
					<input type="text" id="namaKeretaKey" name="namaKeretaKey"/>
					<br/>
					<button type="submit">Cari</button>
				</form>
				</td>
				<td></td>
			</tr> -->
			<tbody id="list-kereta">
				
			</tbody>
		</table>
	</div>
</div>

<div id="modal-input" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<!-- tombol close di popupnya -->
				<button type="button" class="close" data-dismiss="modal">
					<i class="fa fa-close"></i>
				</button>
				<!-- <h4>Form Pop Up Kereta</h4> -->
			</div>
			<div class="modal-body">
				<!-- tempat jsp popupnya -->
			</div>
		</div>
	</div>
</div>

<script>

	loadListKereta(); // untuk memanggil otomatis

	function loadListKereta(){ // fungsi ini tidak bisa jalan otomatis tanpa line di atas, karena di luar document.ready
		$.ajax({
			url:'kereta/cruds/list.html',
			type: 'get',
			dataType: 'html',
			success: function(result) {
				$('#list-kereta').html(result); // fungsi ini memanggil id list-kereta
			}
		});
	}

	//document ready itu setelah halamannya dipanggil
	$(document).ready(function(){
		
		// fungsi ajax untuk memunculkan POP UP tambah
		$('#btn-add').on('click',function(){
			$.ajax({
				url:'kereta/cruds/add.html',
				type:'get',
				dataType:'html',
				success: function(result){
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		// akhir fungsi ajax untuk memunculkan POP UP tambah
		
		/* // fungsi ajax untuk menambah kereta (tombol create pada popup tambah)
		$('#modal-input').on('submit', '#form-kereta-add',function(){
			$.ajax({
				url:'kereta/cruds/create.json',
				type:'get',
				dataType:'json',
				data:$(this).serialize(),
				success: function(result){
					$('#modal-input').modal('hide');
					alert("data telah ditambahkan");
					loadListKereta();
				}
			});
			return false;
		});
		// akhir fungsi ajax untuk menambah kereta */
		
		// fungsi ajax untuk menambah kereta (tombol create pada popup tambah)
		$('#modal-input').on('submit', '#form-kereta-add',function(){
			$.ajax({
				url:'kereta/cruds/create.json',
				type:'get',
				dataType:'json',
				data:$(this).serialize(),
				success: function(result){
				 	if (result.jumlahKeretaByKode > 0) {
						alert("ERROR: kode kereta " + result.kodeKereta + " sudah ada di database!");
					}
					else if (result.jumlahKeretaByNama > 0) {
						alert("ERROR: nama kereta " + result.namaKereta + " sudah ada di database!");
					}
					else {
						$('#modal-input').modal('hide');
						alert("data telah ditambahkan");
						loadListKereta();
					}
				}
			});
			return false;
		});
		// akhir fungsi ajax untuk menambah kereta
		
		// fungsi ajax untuk memunculkan POP UP lihat
		$('#list-kereta').on('click', '#btn-detail', function(){
			var idKereta = $(this).val();
			$.ajax({
				url:'kereta/cruds/detail.html',
				type:'get',
				dataType:'html',
				data:{idKereta:idKereta},
				/* data:$(this).serialize(), */
				success: function(result){
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		// akhir fungsi ajax untuk lihat
		
		// fungsi ajax untuk memunculkan POP UP ubah
		$('#list-kereta').on('click', '#btn-edit', function(){
			var idKereta = $(this).val();
			$.ajax({
				url:'kereta/cruds/edit.html',
				type:'get',
				dataType:'html',
				data:{idKereta:idKereta},
				/* data:$(this).serialize(), */
				success: function(result){
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		// akhir fungsi ajax untuk ubah
		
		// fungsi ajax untuk update kereta
		$('#modal-input').on('submit', '#form-kereta-edit', function(){
			
			$.ajax({
				url:'kereta/cruds/update.json',
				type:'get',
				dataType:'json',
				data:$(this).serialize(),
				success: function(result){
					$('#modal-input').modal('hide');
					alert("data telah diubah");
					loadListKereta();
				}
			});
			return false;
		});
		// akhir fungsi ajax untuk update kereta
		
		// fungsi ajax untuk memunculkan POP UP hapus
		$('#list-kereta').on('click', '#btn-delete', function(){
			var idKereta = $(this).val();
			$.ajax({
				url:'kereta/cruds/remove.html',
				type:'get',
				dataType:'html',
				data:{idKereta:idKereta},
				/* data:$(this).serialize(), */
				success: function(result){
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		// akhir fungsi ajax untuk hapus
		
		// fungsi ajax untuk delete kereta
		$('#modal-input').on('submit', '#form-kereta-delete', function(){
			
			$.ajax({
				url:'kereta/cruds/delete.json',
				type:'get',
				dataType:'json',
				data:$(this).serialize(),
				success: function(result){
					$('#modal-input').modal('hide');
					alert("data telah dihapus");
					loadListKereta();
				}
			});
			return false;
		});
		// akhir fungsi ajax untuk delete kereta
		
		/* // fungsi ajax untuk mencari kereta berdasarkan nama
		$('#form-kereta-search-nama').on('submit',function(){
			$.ajax({
				url:'kereta/cruds/search/nama.html',
				type:'get',
				dataType:'html',
				data:$(this).serialize(),
				success: function(result){
					alert("data telah dicari berdasarkan nama");
					$('#list-kereta').html(result);
				}
			});
			return false;
		});
		// akhir fungsi ajax untuk mencari kereta berdasarkan nama */
		
		/* // fungsi ajax untuk mencari kereta berdasarkan kode
		$('#form-kereta-search-kode').on('submit',function(){
			$.ajax({
				url:'kereta/cruds/search/kode.html',
				type:'get',
				dataType:'html',
				data:$(this).serialize(),
				success: function(result){
					alert("data telah dicari berdasarkan kode");
					$('#list-kereta').html(result);
				}
			});
			return false;
		});
		// akhir fungsi ajax untuk mencari kereta berdasarkan kode */
		
		/* // fungsi ajax untuk mencari berdasarkan kode / nama kereta
		$('#form-kereta-search').on('submit',function(){
			$.ajax({
				url:'kereta/cruds/search.html',
				type:'get',
				dataType:'html',
				data:$(this).serialize(),
				success: function(result){
					alert("data telah dicari berdasarkan keyword");
					$('#list-kereta').html(result);
				}
			});
			return false;
		});
		// akhir fungsi ajax untuk mencari berdasarkan kode / nama kereta */
	});
	// akhir dari dokumen ready
</script>