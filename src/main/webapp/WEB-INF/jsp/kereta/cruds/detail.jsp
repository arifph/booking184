<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<div class="form-horizontal">
	<h1>Lihat Kereta</h1>
	<form action="#" method="get" id="form-kereta-detail">
		<table>
			<tr>
				<td>Kode Kereta</td>
				<td><input type="text" id="kodeKereta" name="kodeKereta" value="${keretaModel.kodeKereta}" disabled /></td>
			</tr>
			<tr>
				<td>Nama Kereta</td>
				<td><input type="text" id="namaKereta" name="namaKereta" value="${keretaModel.namaKereta}" disabled /></td>
			</tr>
			<tr>
				<td>Jenis</td>
				<td><input type="text" id="jenisKereta" name="jenisKereta" value="${keretaModel.jenisKereta}" disabled /></td>
			</tr>
			<tr>
				<td>Keterangan</td>
				<td><input type="text" id="ketKereta" name="ketKereta" value="${keretaModel.ketKereta}" disabled /></td>
			</tr>
			<tr>
				<td>Fee Agen</td>
				<td><input type="text" id="feeKereta" name="feeKereta" value="${keretaModel.feeKereta}" disabled /></td>
			</tr>
			<%-- <tr>
				<td>Kota</td>
				<td>${var2}</td>
			</tr>
			<tr>
				<td>Makan</td>
				<td>${var3}</td>
			</tr> --%>
			<%-- <tr>
				<td>Lokasi Kereta</td>
				<td>
					<select id="idLokasi" name="idLokasi">
						<c:forEach items="${lokasiModelList}" var="lokasiModel">
							<option value="${lokasiModel.idLokasi}" 
							${lokasiModel.idLokasi==keretaModel.idLokasi ? 'selected="true"' : ''}>
								${lokasiModel.namaLokasi}
							</option>
						</c:forEach>
					</select>
				</td>
			</tr>
			<tr>
				<td>Created By: </td>
				<td>${keretaModel.xCreatedBy.username}</td>
			</tr>
			<tr>
				<td>Created Date: </td>
				<td>${keretaModel.xCreatedDate}</td>
			</tr>
			<tr>
				<td>Updated By: </td>
				<td>${keretaModel.xUpdatedBy.username}</td>
			</tr>
			<tr>
				<td>Updated Date: </td>
				<td>${keretaModel.xUpdatedDate}</td>
			</tr>
			<tr>
				<td>Deleted By: </td>
				<td>${keretaModel.xDeletedBy.username}</td>
			</tr>
			<tr>
				<td>Deleted Date:</td>
				<td>${keretaModel.xDeletedDate}</td>
			</tr> --%>
		</table>
	</form>
</div>