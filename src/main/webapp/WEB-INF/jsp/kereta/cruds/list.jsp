<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:forEach items="${keretaModelList}" var="keretaModel" varStatus="number">
	<tr>
		<%-- <td>${number.count}</td> --%>
		<td>${keretaModel.kodeKereta}</td>
		<td>${keretaModel.namaKereta}</td>
		<td>${keretaModel.jenisKereta}</td>
		<td>
			<button type="button" class="btn btn-success" value="${keretaModel.idKereta}" id="btn-edit">Edit</button>
			<button type="button" class="btn btn-danger" value="${keretaModel.idKereta}" id="btn-delete">Delete</button>
			<button type="button" class="btn btn-warning" value="${keretaModel.idKereta}" id="btn-detail">Detil</button>
		</td>
	</tr>
</c:forEach>