<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<div class="form-horizontal">
	<h1>Hapus Kereta</h1>
	<form action="#" method="get" id="form-kereta-delete">
	<input type="hidden" id="idKereta" name="idKereta" value="${keretaModel.idKereta}" />
		<table>
			<tr>
				<td>Kode Kereta</td>
				<td><input type="text" id="kodeKereta" name="kodeKereta" value="${keretaModel.kodeKereta}" disabled /></td>
			</tr>
			<tr>
				<td>Nama Kereta</td>
				<td><input type="text" id="namaKereta" name="namaKereta" value="${keretaModel.namaKereta}" disabled /></td>
			</tr>
			<tr>
				<td>Jenis</td>
				<td><input type="text" id="jenisKereta" name="jenisKereta" value="${keretaModel.jenisKereta}" disabled /></td>
			</tr>
			<tr>
				<td>Keterangan</td>
				<td><input type="text" id="ketKereta" name="ketKereta" value="${keretaModel.ketKereta}" disabled /></td>
			</tr>
			<tr>
				<td>Fee Agen</td>
				<td><input type="text" id="feeKereta" name="feeKereta" value="${keretaModel.feeKereta}" disabled /></td>
			</tr>
			<%-- <tr>
				<td>Lokasi Kereta</td>
				<td>
					<select id="idLokasi" name="idLokasi">
						<c:forEach items="${lokasiModelList}" var="lokasiModel">
							<option value="${lokasiModel.idLokasi}" 
							${lokasiModel.idLokasi==keretaModel.idLokasi ? 'selected="true"' : ''}>
								${lokasiModel.namaLokasi}
							</option>
						</c:forEach>
					</select>
				</td>
			</tr> --%>
			<tr>
				<td></td>
				<td><button type="submit">Delete</button></td>
			</tr>
		</table>
	</form>
</div>