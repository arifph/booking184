<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<div class="form-horizontal">
	<h1>Ubah Kereta</h1>
	<form action="#" method="get" id="form-kereta-edit">
	<input type="hidden" id="idKereta" name="idKereta" value="${keretaModel.idKereta}" />
		<table>
			<tr>
				<td>Kode Kereta</td>
				<td><input type="text" id="kodeKereta" name="kodeKereta" value="${keretaModel.kodeKereta}"/></td>
			</tr>
			<tr>
				<td>Nama Kereta</td>
				<td><input type="text" id="namaKereta" name="namaKereta" value="${keretaModel.namaKereta}"/></td>
			</tr>
			<%-- <tr>
				<td>Jenis</td>
				<td><input type="text" id="jenisKereta" name="jenisKereta" value="${keretaModel.jenisKereta}"/></td>
			</tr> --%>
			<tr>
				<td>Jenis</td>
				<td><input type="radio" id="Ekonomi" name="jenisKereta" value="Ekonomi" ${keretaModel.jenisKereta=='Ekonomi' ? 'checked="checked"' : ''} />Ekonomi</td>
				<td><input type="radio" id="Eksekutif" name="jenisKereta" value="Eksekutif" ${keretaModel.jenisKereta=='Eksekutif' ? 'checked="checked"' : ''} />Eksekutif</td>
			</tr>
			<tr>
				<td>Keterangan</td>
				<td><input type="text" id="ketKereta" name="ketKereta" value="${keretaModel.ketKereta}"/></td>
			</tr>
			<tr>
				<td>Fee Agen</td>
				<td><input type="text" id="feeKereta" name="feeKereta" value="${keretaModel.feeKereta}"/></td>
			</tr>
			<!-- <tr>
				<td>VarJSP</td>
				<td><input type="text" id="varJSP" name="varJSP"/></td>
			</tr> -->
			<%-- <tr>
				<td>Lokasi Kereta</td>
				<td>
					<select id="idLokasi" name="idLokasi">
						<c:forEach items="${lokasiModelList}" var="lokasiModel">
							<option value="${lokasiModel.idLokasi}" 
							${lokasiModel.idLokasi==keretaModel.idLokasi ? 'selected="true"' : ''}>
								${lokasiModel.namaLokasi}
							</option>
						</c:forEach>
					</select>
				</td>
			</tr> --%>
			<tr>
				<td></td>
				<td><button type="submit">Update</button></td>
			</tr>
		</table>
	</form>
</div>