<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<div class="form-horizontal">
	<!-- <h1>Tambah Fakultas</h1> -->
	<form action="#" method="get" id="form-kereta-add">
		<table>
			<tr>
				<td>Kode Kereta</td>
				<td><input type="text" id="kodeKereta" name="kodeKereta" value="${kodeKeretaAuto}"/></td>
			</tr>
			<tr>
				<td>Nama Kereta</td>
				<td><input type="text" id="namaKereta" name="namaKereta" /></td>
			</tr>
			<tr>
				<td>Jenis</td>
				<td><input type="radio" id="ekonomi" name="jenisKereta" value="Ekonomi"/>Ekonomi</td>
				<td><input type="radio" id="eksekutif" name="jenisKereta" value="Eksekutif"/>Eksekutif</td>
			</tr>
			<tr>
				<td>Keterangan</td>
				<td><input type="text" id="ketKereta" name="ketKereta" /></td>
			</tr>
			<tr>
				<td>Fee Agen</td>
				<td><input type="text" id="feeKereta" name="feeKereta" /></td>
			</tr>
			<%-- <tr>
				<td>Lokasi Fakultas</td>
				<td>
					<select id="idLokasi" name="idLokasi">
						<c:forEach items="${lokasiModelList}" var="lokasiModel">
							<option value="${lokasiModel.idLokasi}">
								${lokasiModel.namaLokasi}
							</option>
						</c:forEach>
					</select>
				</td>
			</tr> --%>
			<tr>
				<td></td>
				<td><button type="submit">Create</button></td>
			</tr>
		</table>
	</form>
</div>