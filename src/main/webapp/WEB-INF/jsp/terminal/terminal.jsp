<div class="panel" style="background: white; margin-top: 50px; min-height: 500px">
	<h1>Halaman Terminal</h1>
	<button type="button" class="btn btn-primary" id="btn-add">Tambah</button>
	
	<div>
		<table class="table" id="tbl-jurusan">
			<tr>
				<td></td>
				<td></td>
				<td></td>
				<td align="right" colspan="2">Cari Kode/Nama Jurusan</td>
				<td colspan="2"><form action="#" method="get" id="form-jurusan-search">
						<input type="text" id="keyword" name="keyword" size="20">
						<button type="submit">Cari</button>
					</form>
				</td>
			</tr>
			<tr>
				<td rowspan="2" align="center"><strong>No</strong></td>
				<td rowspan="2" align="center"><strong>Kode Terminal</strong></td>
				<td rowspan="2" align="center"><strong>Nama Terminal</strong></td>
				<td rowspan="2" align="center"><strong>Kota</strong></td>
				<td colspan="3" align="center"><strong>Action</strong></td>
				
			</tr>
			<tr>
				
				<td align="center"><strong>Edit</strong></td>
				<td align="center"><strong>Delete</strong></td>
				<td align="center"><strong>Detil</strong></td>
			</tr>
			
			<tbody id="list-terminal">
				<!-- isi Listnya -->
			</tbody>
		</table>
	</div>
	
</div>

<div id="modal-input" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<i class="fa fa-close"></i>
				</button>
				<h4>Form PopUp Terminal</h4>
			</div>
			<div class="modal-body">
				<!-- tempat jsp popupnya -->
			</div>
		</div>
	</div>
</div>

<script>

	loadListTerminal(); /* untuk manggil otomatis krn tidak ada ready */
	
	function loadListTerminal(){
		$.ajax({
			url:'terminal/cruds/list.html',
			type:'get',
			dataType:'html',
			success: function(result){
				$('#list-terminal').html(result);
			}
		});
	}
		
	//document ready itu setelah halaman dipanggil
	$(document).ready(function(){
		
		//fungsi ajax untuk popup tambah
		$('#btn-add').on('click',function(){
			$.ajax({
				url:'terminal/cruds/add.html',
				type:'get',
				dataType:'html',
				success: function(result){
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk popup tambah
		
		
		//fungsi ajax untuk create tambah
		$('#modal-input').on('submit','#form-terminal-add',function(){
			$.ajax({
				url:'terminal/cruds/create.json',
				type:'get',
				dataType:'json',
				data: $(this).serialize(),
				success: function(result){
					$('#modal-input').modal('hide');
					alert("data telah ditambah");
					loadListTerminal();
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk create tambah
		
		//fungsi ajax untuk lihat
		$('#list-terminal').on('click','#btn-detail',function(){
			var idTerminal = $(this).val();
			$.ajax({
				url:'terminal/cruds/detail.html',
				type:'get',
				dataType:'html',
				data:{idTerminal:idTerminal},
				success: function(result){
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk lihat
		
		//fungsi ajax untuk ubah
		$('#list-terminal').on('click','#btn-edit',function(){
			var idTerminal = $(this).val();
			$.ajax({
				url:'terminal/cruds/edit.html',
				type:'get',
				dataType:'html',
				data:{idTerminal:idTerminal},
				success: function(result){
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk ubah
		
		//fungsi ajax untuk update
		$('#modal-input').on('submit','#form-terminal-edit',function(){
			
			$.ajax({
				url:'terminal/cruds/update.json',
				type:'get',
				dataType:'json',
				data:$(this).serialize(),
				success: function(result){
					$('#modal-input').modal('hide');
					alert("data telah diubah");
					loadListTerminal();
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk update
		
		//fungsi ajax untuk hapus
		$('#list-terminal').on('click','#btn-delete',function(){
			var idTerminal = $(this).val();
			$.ajax({
				url:'terminal/cruds/remove.html',
				type:'get',
				dataType:'html',
				data:{idTerminal:idTerminal},
				success: function(result){
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk hapus
		
		//fungsi ajax untuk delete
		$('#modal-input').on('submit','#form-terminal-delete',function(){
			
			$.ajax({
				url:'terminal/cruds/delete.json',
				type:'get',
				dataType:'json',
				data:$(this).serialize(),
				success: function(result){
					$('#modal-input').modal('hide');
					alert("data telah dihapus");
					loadListTerminal();
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk delete
		
		//fungsi ajax untuk search namaJurusan
		$('#form-jurusan-search-nama').on('submit',function(){
			$.ajax({
				url:'jurusan/cruds/search/nama.html',
				type:'get',
				dataType:'html',
				data: $(this).serialize(),
				success: function(result){
					alert("data telah dicari berdasarkan nama");
					$('#list-jurusan').html(result); //reload list jurusan
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk search namaJurusan
		
		//fungsi ajax untuk search kodeJurusan
		$('#form-jurusan-search-kode').on('submit',function(){
			$.ajax({
				url:'jurusan/cruds/search/kode.html',
				type:'get',
				dataType:'html',
				data: $(this).serialize(),
				success: function(result){
					alert("data telah dicari berdasarkan kode");
					$('#list-jurusan').html(result); //reload list jurusan
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk search kodeJurusan
		
		//fungsi ajax untuk search kodeJurusan /namaJurusan
		$('#form-jurusan-search').on('submit',function(){
			$.ajax({
				url:'jurusan/cruds/search.html',
				type:'get',
				dataType:'html',
				data: $(this).serialize(),
				success: function(result){
					alert("data telah dicari berdasarkan kode/ nama jurusan");
					$('#list-jurusan').html(result); //reload list jurusan
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk search kodeJurusan/namaJurusan
		
	});
	//akhir dokumen ready
</script>