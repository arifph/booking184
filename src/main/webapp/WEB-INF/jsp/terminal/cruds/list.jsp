<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:forEach items="${terminalModelList}" var="terminalModel" varStatus="number">
	<tr>
		<td align="center">${number.count}</td>
		<td>${terminalModel.kodeTerminal}</td>
		<td>${terminalModel.namaTerminal}</td>
		<td>${terminalModel.kotaModel.namaKota}</td>
		<td align="center"><button type="button" class="btn btn-warning" value="${terminalModel.idTerminal}" id="btn-detail">Lihat</button></td>
		<td align="center"><button type="button" class="btn btn-success" value="${terminalModel.idTerminal}" id="btn-edit">Ubah</button></td>
		<td align="center">
			<button type="button" class="btn btn-danger" value="${terminalModel.idTerminal}" id="btn-delete">Hapus</button>
		</td>
	</tr>
</c:forEach>