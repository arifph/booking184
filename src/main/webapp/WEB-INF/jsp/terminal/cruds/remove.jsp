<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<div class="form-horizontal">
	<h1>Hapus Data Terminal</h1>
	<br/>
	<br/>
	<form action="#" method="get" id="form-terminal-delete">
		<table>
			<div class="form-group">
				<label class="control-label col-md-3">Kode Terminal</label>
				<input type="text" class="form-input" name="kodeTerminal" id="kodeTerminal" value="${terminalModel.kodeTerminal}" disable="disable"/>
				<input type="hidden" class="form-input" name="idTerminal" id="idTerminal" value="${terminalModel.idTerminal}" />
			</div>
			<div class="form-group">
				<label class="control-label col-md-3">Nama Terminal</label>
				<input type="text" class="form-input" name="namaTerminal" id="namaTerminal" value="${terminalModel.namaTerminal}" disabled="disabled" />
			</div>
			 <div class="form-group">
				<label class="control-label col-md-3">Kota</label>
				<select id="kota" name="kota" class="form-input" disabled="disabled">
					<option>--Pilih--</option>
						<c:forEach items="${kotaModelList}" var="kotaModel">
							<option value="${kotaModel.idKota}" ${kotaModel.idKota==terminalModel.idKota ? 'selected="true"':'' }>
								${kotaModel.namaKota}
							</option>
						</c:forEach>						
					</select>
			</div> 
			<div class="form-group">
				<label class="control-label col-md-3">Telepon</label>
				<input type="text" class="form-input" name="noTelpon" id="noTelpon" value="${terminalModel.noTelpon}" disabled="disabled" />
			</div>
			<div class="form-group">
				<label class="control-label col-md-3">Email</label>
				<input type="text" class="form-input" name="email" id="email" value="${terminalModel.email}" disabled="disabled" />
			</div>
			<div class="form-group">
				<label class="control-label col-md-3">Alamat</label>
				<textarea rows="3" cols="1" name="alamat" id="alamat" style="width:40%" disabled="disabled">${terminalModel.alamat}</textarea>
			</div>
			 <div class="modal-footer">
				<button type="submit" class="btn btn-danger">Hapus</button>
			</div> 
			
		</table>
	
	</form>
</div>