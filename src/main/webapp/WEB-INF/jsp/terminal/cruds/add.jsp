<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<div class="form-horizontal">
	<h1>Tambah Terminal</h1>
	
	<form action="#" method="get" id="form-terminal-add">
		<table>
			<div class="form-group">
				<label class="control-label col-md-3">Kode Terminal</label>
				<input type="text" class="form-input" name="kodeTerminal" id="kodeTerminal" />
			</div>
			<div class="form-group">
				<label class="control-label col-md-3">Nama Terminal</label>
				<input type="text" class="form-input" name="namaTerminal" id="namaTerminal" />
			</div>
			<div class="form-group">
				<label class="control-label col-md-3">Kota</label>
				<select id="kota" name="kota" class="form-input">
					<option>--Pilih--</option>
						<c:forEach items="${kotaModelList}" var="kotaModel">
							<option value="${kotaModel.idKota}">
								${kotaModel.namaKota}
							</option>
						</c:forEach>						
					</select>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3">Telepon</label>
				<input type="text" class="form-input" name="noTelepon" id="noTelepon" />
			</div>
			<div class="form-group">
				<label class="control-label col-md-3">Email</label>
				<input type="text" class="form-input" name="email" id="email" />
			</div>
			<div class="form-group">
				<label class="control-label col-md-3">Alamat</label>
				<textarea rows="3" cols="1" name="alamat" id="alamat" style="width:40%"></textarea>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-success">Simpan</button>
			</div>
			
		</table>
	
	</form>
</div>