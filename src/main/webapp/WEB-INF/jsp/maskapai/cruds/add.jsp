<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<div class="form-horizontal">
<h1>Tambah Maskapai</h1>
	<form action="#" method="get" id="form-maskapai-add">
		<table>
			<tr>
				<td>Kode Maskapai</td>
				<td><input type="text" id="kodeMaskapai" name="kodeMaskapai" value="${kodeMaskapaiAuto}"/></td>
			</tr>
		
			<tr>
				<td>Nama Maskapai</td>
				<td><input type="text" id="namaMaskapai" name="namaMaskapai"/></td>
			</tr>
			
			<tr>
				<td>Rute Maskapai</td>
				<td>
					<select id="ruteMaskapai" name="ruteMaskapai">

							<option value="domestik">
								Domestik
							</option>
							<option value="internasional">
								Internasional
							</option>
							<option value="keduanya">
								Keduanya
							</option>
			
					</select>
				</td>
			</tr>
			
			<tr>
				<td>Kepemilikan</td>
				<td>
					<input type="radio" id="milikMaskapai" name="milikMaskapai" value="lokal">Lokal
					<input type="radio" id="milikMaskapai" name="milikMaskapai" value="asing">Asing
				</td>
			</tr>
			
			<tr>
				<td>Keterangan</td>
				<td><input type="text" id="ketMaskapai" name="ketMaskapai"/></td>
			</tr>
			
			<tr>
				<td>Fee Agent</td>
				<td><input type="text" id="feeMaskapai" name="feeMaskapai"/></td>
			</tr>
			
<!-- 			<td>
					<select id="idLokasi" name="idLokasi">
						<c:forEach items="${lokasiModelList }" var="lokasiModel">
							<option value="${lokasiModel.idLokasi}">
								${lokasiModel.namaLokasi}
							</option>
						</c:forEach>
					</select>
			</td>
 -->			
			
			
			<tr>
				<td></td>
				<td><button type="submit">Create</button></td>
			</tr>
		</table>
	</form>
</div>