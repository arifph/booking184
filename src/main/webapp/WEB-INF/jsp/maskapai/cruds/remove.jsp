<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<div class="form-horizontal">
<h1>Hapus Maskapai</h1>
	<form action="#" method="get" id="form-maskapai-delete">
	<input type="hidden" id="idMaskapai" name="idMaskapai" value="${maskapaiModel.idMaskapai}"/>
		<table>
			<tr>
				<td>Kode Maskapai</td>
				<td><input type="text" id="kodeMaskapai" name="kodeMaskapai" value="${maskapaiModel.kodeMaskapai}"/></td>
			</tr>
		
			<tr>
				<td>Nama Maskapai</td>
				<td><input type="text" id="namaMaskapai" name="namaMaskapai" value="${maskapaiModel.namaMaskapai}"/></td>
			</tr>
						
			<tr>
				<td>Rute Maskapai</td>
				<td>
					<select id="ruteMaskapai" name="ruteMaskapai">
							<option value="domestik" 
								${maskapaiModel.ruteMaskapai == 'domestik' ? 'selected="true"':'' }>
								Domestik
							</option>
								
							<option value="internasional" 
								${maskapaiModel.ruteMaskapai == 'internasional' ? 'selected="true"':'' }>
								Internasional
							</option>
							
							<option value="keduanya" 
								${maskapaiModel.ruteMaskapai == 'keduanya' ? 'selected="true"':'' }>
								Keduanya
							</option>
					</select>
				</td>
			</tr>
			
			<tr>
				<td>Kepemilikan</td>
				<td>
					<input type="radio" id="milikMaskapai" name="milikMaskapai" value="lokal" ${maskapaiModel.milikMaskapai == 'lokal' ? 'checked="true"':'' }>Lokal
					<input type="radio" id="milikMaskapai" name="milikMaskapai" value="asing" ${maskapaiModel.milikMaskapai == 'asing' ? 'checked="true"':'' }>Asing
				</td>
			</tr>
			
			<tr>
				<td>Keterangan</td>
				<td><input type="text" id="ketMaskapai" name="ketMaskapai" value="${maskapaiModel.ketMaskapai}"/></td>								
			</tr>
			
			<tr>
				<td>Fee Agent</td>
				<td><input type="text" id="feeMaskapai" name="feeMaskapai" value="${maskapaiModel.feeMaskapai}"/></td>								
			</tr>
			
			<tr>
				<td></td>
				<td><button type="submit">Delete</button></td>
			</tr>					
			
		</table>
	
	</form>
</div>