<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<div class="form-horizontal">
<h1>Lihat Maskapai</h1>
	<form action="#" method="get" id="form-maskapai-detail">
		<table>
			<tr>
				<td>Kode Maskapai</td>
				<td><input type="text" id="kodeMaskapai" name="kodeMaskapai" value="${maskapaiModel.kodeMaskapai}"/></td>
			</tr>
		
			<tr>
				<td>Nama Maskapai</td>
				<td><input type="text" id="namaMaskapai" name="namaMaskapai" value="${maskapaiModel.namaMaskapai}"/></td>
			</tr>
			<tr>
				<td>Rute Maskapai</td>
				<td><input type="text" id="ruteMaskapai" name="ruteMaskapai" value="${maskapaiModel.ruteMaskapai}"/></td>				
			</tr>
			
			<tr>
				<td>Kepemilikan</td>
				<td><input type="text" id="milikMaskapai" name="milikMaskapai" value="${maskapaiModel.milikMaskapai}"/></td>								
			</tr>

			<tr>
				<td>Keterangan</td>
				<td><input type="text" id="ketMaskapai" name="ketMaskapai" value="${maskapaiModel.ketMaskapai}"/></td>								
			</tr>
			
			<tr>
				<td>Fee Agent</td>
				<td><input type="text" id="feeMaskapai" name="feeMaskapai" value="${maskapaiModel.feeMaskapai}"/></td>								
			</tr>



<!-- 		<tr>
				<td>Created By: </td>
				<td>${fakultasModel.xCreatedBy.username }</td>
			</tr>
			
			<tr>
				<td>Created Date: </td>
				<td>${fakultasModel.xCreatedDate }</td>
			</tr>
			
			<tr>
				<td>Updated By: </td>
				<td>${fakultasModel.xUpdatedBy.username }</td>
			</tr>
			
			<tr>
				<td>Updated Date: </td>
				<td>${fakultasModel.xUpdatedDate }</td>
			</tr>
			
			<tr>
				<td>Deleted By: </td>
				<td>${fakultasModel.xDeletedBy.username }</td>
			</tr>
			
			<tr>
				<td>Deleted Date: </td>
				<td>${fakultasModel.xDeletedDate }</td>
			</tr>
 -->			
		</table>
	
	
	</form>
</div>