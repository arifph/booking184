<div class="panel"
	style="background: white; margin-top: 50px; min-height: 500px">
	<h1>Data Maskapai</h1>
	<button type="button" class="btn btn-primary" id="btn-add">Tambah</button>

	<div>
		<table class="table" id="tbl-maskapai">

			<tr>
				<td>No</td>
				<td>Kode Maskapai</td>
				<td>Nama Maskapai</td>
				<td>Rute</td>
				<td>Aksi</td>
			</tr>
			<tbody id="list-maskapai">

			</tbody>
		</table>
	</div>
</div>



<div id="modal-input" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<i class="fa fa-close"></i>
				</button>
				<h4>Form PopUp Maskapai</h4>
			</div>
			<div class="modal-body">
				<!--tempat jsp popupnya-->
			</div>
		</div>
	</div>
</div>

<script>
loadListMaskapai();

function loadListMaskapai() {
	$.ajax({
		url : 'maskapai/cruds/list.html',
		type : 'get',
		dataType : 'html',
		success : function(result) {
			$('#list-maskapai').html(result);
		}
	});
}

	//document ready itu setelah halamannya dipanggil
	$(document).ready(
		function() {

			//fungsi ajax untuk popup tambah
			$('#btn-add').on('click', function() {
				$.ajax({
					url : 'maskapai/cruds/add.html',
					type : 'get',
					dataType : 'html',
					success : function(result) {
						$('#modal-input').find('.modal-body').html(result);
						$('#modal-input').modal('show');
					}
				});
			})
			//akhir fungsi ajax untuk popup tambah
		
			$('#modal-input').on('submit','#form-maskapai-add',function() {
				$.ajax({
					url : 'maskapai/cruds/create.json',
					type : 'get',
					dataType : 'json',
					data : $(this).serialize(),
					success : function(result) {
						if(result.jumlahMaskapaiByKode > 0){
							alert("ERROR : Kode Maskapai "+ result.kodeMaskapai + " sudah ada di Database!");
						}else if(result.jumlahMaskapaiByNama > 0){
							alert("ERROR : Nama Maskapai "+ result.namaMaskapai + " sudah ada di Database!");
						}else{
							$('#modal-input').modal('hide');
							alert("data telah ditambah");
							loadListMaskapai();							
						}
					}
				});
				return false; //supaya tidak merefresh ulang semua data, jadi data ditambah saja
			});


			//fungsi ajax untuk lihat detail
			$('#list-maskapai').on('click', '#btn-detail', function() {
				var idMaskapai = $(this).val(); //ambil dari list.jsp hanya 1 nilai
				$.ajax({
					url : 'maskapai/cruds/detail.html',
					type : 'get',
					dataType : 'html',
					data : {
						idMaskapai : idMaskapai
					},
					success : function(result) {
						$('#modal-input').find('.modal-body').html(result);
						$('#modal-input').modal('show');
					}
				});
			});

			//edit popup btn
			$('#list-maskapai').on('click', '#btn-edit', function() {
				var idMaskapai = $(this).val();
				$.ajax({
					url : 'maskapai/cruds/edit.html',
					type : 'get',
					dataType : 'html',
					data : {
						idMaskapai : idMaskapai
					},
					success : function(result) {
						$('#modal-input').find('.modal-body').html(result);
						$('#modal-input').modal('show');
					}
				});
			});

			//fungsi ajax untuk update di dalam popup update
			$('#modal-input').on('submit', '#form-maskapai-edit',
					function() {
						$.ajax({
							url : 'maskapai/cruds/update.json',
							type : 'get',
							dataType : 'json',
							data : $(this).serialize(),
							success : function(result) {
								$('#modal-input').modal('hide');
								alert("data telah diubah");
								loadListMaskapai();
							}
						});
						return false;
					});

			//btn hapus
			$('#list-maskapai').on('click', '#btn-delete', function() {
				var idMaskapai = $(this).val();
				$.ajax({
					url : 'maskapai/cruds/remove.html',
					type : 'get',
					dataType : 'html',
					data : {
						idMaskapai : idMaskapai
					},
					success : function(result) {
						$('#modal-input').find('.modal-body').html(result);
						$('#modal-input').modal('show');
					}
				});
			});

			//fungsi untuk menjalakan hapus pada pop up
			$('#modal-input').on('submit', '#form-maskapai-delete',
					function() {
						$.ajax({
							url : 'maskapai/cruds/delete.json',
							type : 'get',
							dataType : 'json',
							data : $(this).serialize(),// ambil data dari backend secara kesuluruhan
							success : function(result) {
								$('#modal-input').modal('hide');
								alert("data telah dihapus");
								loadListMaskapai();
							}
						});
						return false;
					});
		
		
		
		});







</script>