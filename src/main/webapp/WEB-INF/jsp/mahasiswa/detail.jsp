<h4>Halaman Detil Mahasiswa</h4>
<br/>
<div class="form-horizontal">
	<form action="#" method="get" id="form-mahasiswa-detail">
	
		<div class="form-group">
			<label class="control-label col-md-3">NIM</label>
			<input type="text" class="form-input" name="nimMahasiswa" id="nimMahasiswa" value="${mahasiswaModel.nimMahasiswa}" disabled="disabled"/>
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-3">Nama</label>
			<input type="text" class="form-input" name="namaMahasiswa" id="namaMahasiswa" value="${mahasiswaModel.namaMahasiswa}" disabled="disabled"/>
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-3">Telepon</label>
			<input type="text" class="form-input" name="teleponMahasiswa" id="teleponMahasiswa" value="${mahasiswaModel.teleponMahasiswa}" disabled="disabled"/>
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-3">Gender</label>
			<input type="radio" class="form-input" name="genderMahasiswa" id="pria" value="pria"/>Pria 
			<input type="radio" class="form-input" name="genderMahasiswa" id="wanita" value="wanita"/>Wanita
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-3">Alamat</label>
			<textarea rows="5" cols="30" name="alamatMahasiswa" id="alamatMahasiswa" disabled="disabled">${mahasiswaModel.alamatMahasiswa}</textarea>
		</div>
		
		<div class="form-group">
			<label class="control-label col-md-3">Tahun Lahir</label>
			<input type="text" class="form-input" name="tahunLahirMahasiswa" id="tahunLahirMahasiswa" value="${mahasiswaModel.tahunLahirMahasiswa}" disabled="disabled"/>
		</div>
		
		
	</form>
</div>

