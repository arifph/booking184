
<div class="panel"
	style="background: white; margin-top: 50px; min-height: 500px">
	<div>
		<table class="table" id="tbl-bis">
			<tr>
				<td>Data Bis</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td><button class="btn btn-primary" type="button" id="btn-add">Tambah</button></td>
			</tr>
			<tr>
				<td>Kode Bis</td>
				<td>Nama Bis</td>
				<td>Transit</td>
				<td></td>
				<td>Action</td>
				<td></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td></td>
				<td>Edit</td>
				<td>Delete</td>
				<td>Detail</td>
			</tr>
			<tbody id="list-bis">
			</tbody>
		</table>
	</div>
</div>

<div id="modal-input" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<i class="fa fa-close"></i>
				</button>
			</div>
			<div class="modal-body">
				<!-- tempat jsp popup -->
			</div>
		</div>
	</div>
</div>

<script>
	
	loadListBis();
	
	function loadListBis(){
		$.ajax({
			url:'bis/cruds/list.html',
			type:'get',
			dataType:'html',
			success: function(result){
				$('#list-bis').html(result);
			}
		});
	}
	
	$(document).ready(function(){

		$('#btn-add').on('click',function(){
			$.ajax({
				url:'bis/cruds/add.html',
				type:'get',
				dataType:'html',
				success: function(result){
				
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');	
				}
			});
		});
		
		$('#modal-input').on('submit','#form-bis-add',function(){
			$.ajax({
				url : 'bis/cruds/create.json',
				type : 'get',
				dataType : 'json',
				data : $(this).serialize(),
				success : function(result){
					alert("data anda telah tersimpan");
					$('#modal-input').modal('hide');
					loadListBis();
				}
			});
			return false;
		});
		
		$('#list-bis').on('click','#btn-detail', function(){
			var idBis = $(this).val();
			alert(idBis);
			$.ajax({
				url: 'bis/cruds/detail.html',
				type: 'get',
				dataType: 'html',
				data: {idBis:idBis},
				success: function(result){
					alert("sudah masuk");
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
			return false;
		});
		
		$('#list-bis').on('click','#btn-edit', function(){
			var idBis = $(this).val();
			alert(idBis);
			$.ajax({
				url: 'bis/cruds/edit.html',
				type: 'get',
				dataType: 'html',
				data: {idBis:idBis},
				success: function(result){
					alert("sudah masuk");
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		
		$('#modal-input').on('submit','#form-bis-edit', function(){
			$.ajax({
				url: 'bis/cruds/update.json',
				type: 'get',
				dataType: 'json',
				data: $(this).serialize(),
				success: function(result){
					$('#modal-input').modal('hide');
					loadListBis();
					alert("sudah masuk");
				}
			});
			return false;
		});
			
		$('#list-bis').on('click','#btn-delete', function(){
			var idBis = $(this).val();
			alert(idBis);
			$.ajax({
				url: 'bis/cruds/delete.html',
				type: 'get',
				dataType: 'html',
				data: {idBis:idBis},
				success: function(result){
					alert("sudah masuk");
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		
		$('#modal-input').on('submit','#form-bis-delete', function(){
			$.ajax({
				url: 'bis/cruds/remove.json',
				type: 'get',
				dataType: 'json',
				data: $(this).serialize(),
				success: function(result){
					$('#modal-input').modal('hide');
					loadListBis();
				}
			});
		});
		
	});
	//akhit dokumen ready
</script>