<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<div class="form-horizontal">
	<form action="#" method="get" id="form-bis-add">
		<table>
			<tr>
				<td>Kode Bis</td>
				<td><input type="text" id="kodeBis" name="kodeBis" value="${bisModel.kodeBis}"></td>
			</tr>
			<tr>
				<td>Nama Bis</td>
				<td><input type="text" id="namaBis" name="namaBis" value="${bisModel.namaBis}"></td>
			</tr>
			<tr>
				<td>Transit</td>
				<td>
  					<input type="radio" name="transitBis" value="Transit" /> Transit
  					<input type="radio" name="transitBis" value="Langsung"/> Langsung
				</td>
			</tr>
			<tr>
				<td>Telepon</td>
				<td><input type="text" id="teleponBis" name="teleponBis"></td>
			</tr>
			<tr>
				<td>Alamat</td>
				<td><input type="text" id="alamatBis" name="alamatBis"></td>
			</tr>
			<tr>
				<td>Fee Agen</td>
				<td><input type="text" id="feeAgenBis" name="feeAgenBis"></td>
			</tr>
			<tr>
				<td></td>
				<td><button type="submit">Create</button></td>
			</tr>
		</table>
	</form>
</div>