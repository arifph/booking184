<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<c:forEach items="${bisModelList}" var="bisModel" varStatus="number">
	<tr>
		<td>${bisModel.kodeBis}</td>
		<td>${bisModel.namaBis}</td>
		<td>${bisModel.transitBis}</td>
		<td>
			<button type="button" class="btn btn-success" value="${bisModel.idBis}" id="btn-edit">Ubah</button>
		</td>
		<td>
			<button type="button" class="btn btn-danger" value="${bisModel.idBis}" id="btn-delete">Hapus</button>
		</td>
		<td>
			<button type="button" class="btn btn-warning" value="${bisModel.idBis}" id="btn-detail">Lihat</button>
		</td>
	</tr>
</c:forEach>

<!-- <script>
	
	$(document).ready(function(){
	
		$('#btn-delete').on('click', function(){
			$.ajax({
				url:'fakultas/cruds/delete.json',
				type:'get',
				dataType:'json',
				success: function(result){
					$('#list-fakultas').html(result);
				}
			});
		});
		
	});


</script> -->