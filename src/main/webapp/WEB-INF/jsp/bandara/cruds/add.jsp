<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<div class="form-horizontal">
	<h1>Tambah Bandara</h1>
	<form action="#" method="get" id="form-bandara-add">
		<table>
			<tr>
				<td>Kode Bandara</td>
				<td><input type="text" id="kodeBandara" name="kodeBandara"/></td>
			</tr>
		
			<tr>
				<td>Nama Bandara</td>
				<td><input type="text" id="namaBandara" name="namaBandara"/></td>
			</tr>
			
			<tr>
				<td>Kota</td>
				<td>
					<select id="idKota" name="idKota">
						<c:forEach items="${kotaModelList}" var="kotaModel">
							<option value="${kotaModel.idKota}">
								${kotaModel.namaKota}
							</option>
						</c:forEach>
					</select>
				</td>
			</tr>
			
			<tr>
				<td>Jenis</td>
				<td>
					<select id="jenisBandara" name="jenisBandara">
							<option value= "International">International</option>
							<option value= "Domestik">Domestik</option>
							<option value= "Keduanya">Keduanya</option>
					</select>
				</td>
			</tr>
			
			<tr>
				<td>Alamat</td>
				<td><input type="text" id="alamatBandara" name="alamatBandara"/></td>
			</tr>
			
			<tr>
				<td></td>
				<td><button type="submit">Create</button></td>
			</tr>
		</table>
	
	
	</form>
</div>