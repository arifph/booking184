<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<div class="form-horizontal">
<h1>Lihat Bandara</h1>
	<form action="#" method="get" id="form-bandara-detail">
		<table>
			<tr>
				<td>Kode Bandara</td>
				<td><input type="text" id="kodeBandara" name="kodeBandara" value="${bandaraModel.kodeBandara}"/></td>
			</tr>
		
			<tr>
				<td>Nama Bandara</td>
				<td><input type="text" id="namaBandara" name="namaBandara" value="${bandaraModel.namaBandara}"/></td>
			</tr>
			
			<tr>
				<td>Kota</td>
				<td>
					<select id="idKota" name="idKota">
						<c:forEach items="${kotaModelList }" var="kotaModel">
							<option value="${kotaModel.idKota}" 
								${kotaModel.idKota == bandaraModel.idKota ? 'selected="true"':''}>
								${kotaModel.namaKota}
							</option>
						</c:forEach>
					</select>
				</td>
			</tr>
			
			<tr>
				<td>Jenis</td>
				<td>
					<select id="jenisBandara" name="jenisBandara">
						<option value= "${bandaraModel.jenisBandara}">${bandaraModel.jenisBandara}</option>
					</select>
				</td>
			</tr>
			
			<tr>
				<td>Alamat</td>
				<td><input type="text" id="alamatBandara" name="alamatBandara" value= "${bandaraModel.alamatBandara}"/></td>
			</tr>
			
			<tr>
				<td>Created By</td>
				<td>${bandaraModel.xCreatedBy.username}</td>
			</tr>
			
			<tr>
				<td>Created Date</td>
				<td>${bandaraModel.xCreatedDate}</td>
			</tr>
			
			<tr>
				<td>Updated By</td>
				<td>${bandaraModel.xUpdatedBy.username}</td>
			</tr>
			
			<tr>
				<td>Updated Date</td>
				<td>${bandaraModel.xUpdatedDate}</td>
			</tr>
			
			<tr>
				<td>Deleted By</td>
				<td>${bandaraModel.xDeletedBy.username}</td>
			</tr>
			
			<tr>
				<td>Deleted Date</td>
				<td>${bandaraModel.xDeletedDate}</td>
			</tr>
			
		</table>
	</form>
</div>