<%@ taglib uri = "http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:forEach items="${bandaraModelList}" var="bandaraModel" varStatus="number">
	<tr>
		<td>${bandaraModel.kodeBandara}</td>
		<td>${bandaraModel.namaBandara}</td>
		<td>${bandaraModel.kotaModel.namaKota}</td>
		<td>
			<button type="button" class="btn btn-warning" value="${bandaraModel.idBandara}" id="btn-edit">Edit</button>
			<button type="button" class="btn btn-danger" value="${bandaraModel.idBandara}" id="btn-delete">Delete</button>
			<button type="button" class="btn btn-success" value="${bandaraModel.idBandara}" id="btn-detail">Detail</button>
		</td>

	</tr>
</c:forEach>