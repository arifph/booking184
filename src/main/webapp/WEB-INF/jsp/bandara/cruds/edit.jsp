<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<div class="form-horizontal">
<h1>Ubah Bandara</h1>
	<form action="#" method="get" id="form-bandara-edit">
	<input type="hidden" id="idBandara" name="idBandara" value="${bandaraModel.idBandara}"/>
		<table>
			<tr>
				<td>Kode Bandara</td>
				<td><input type="text" id="kodeBandara" name="kodeBandara" value="${bandaraModel.kodeBandara}"/></td>
			</tr>
		
			<tr>
				<td>Nama Bandara</td>
				<td><input type="text" id="namaBandara" name="namaBandara" value="${bandaraModel.namaBandara}"/></td>
			</tr>
			
			<tr>
				<td>Kota</td>
				<td>
					<select id="idKota" name="idKota">
						<c:forEach items="${kotaModelList}" var="kotaModel">
							<option value="${kotaModel.idKota}"
								${kotaModel.idKota==bandaraModel.idKota ? 'selected="true"':''}>
								${kotaModel.namaKota}
							</option>
						</c:forEach>
					</select>
				</td>
			</tr>
			
			<tr>
				<td>Jenis</td>
				<td>
					<select id="jenisBandara" name="jenisBandara" >
								<option value= "${bandaraModel.jenisBandara}">${bandaraModel.jenisBandara}</option>
								<option value= "International">International</option>
								<option value= "Domestik">Domestik</option>
								<option value= "Keduanya">Keduanya</option>
					</select>
				</td>
			</tr>
			
			<tr>
				<td>Alamat</td>
				<td><input type="text" id="alamatBandara" name="alamatBandara" value= "${bandaraModel.alamatBandara}"/></td>
			</tr>
			
			
			<tr>
				<td></td>
				<td><button type="submit">Update</button></td>
			</tr>		
			
		</table>
	
	
	</form>
</div>