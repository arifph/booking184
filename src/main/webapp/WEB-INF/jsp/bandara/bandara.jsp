<div class="panel"
	style="background: white; margin-top: 50px; min-height: 500px">
	<h1>Halaman Bandara</h1>
	
	<div>
		<table class="table" id="tbl-bandara">
		
			<tr>
				<td>Data Bandara</td>
				<td></td>
				<td></td>
				<td align= "right">
					<button type= "button" class= "btn btn-primary" id= "btn-add">Tambah</button>
				</td>
			</tr>
			<tr>
				<td>Kode Bandara</td>
				<td>Nama Bandara</td>
				<td>Kota</td>
				<td>Action</td>
			</tr>
			
			<tbody id="list-bandara">

			</tbody>
		</table>
	</div>
</div>

<div id="modal-input" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<i class="fa fa-close"></i>
				</button>
				<h4>Form PopUp Bandara</h4>
			</div>
			<div class="modal-body">
				<!--tempat jsp popupnya-->
			</div>
		</div>
	</div>
</div>

<script>
	loadListBandara();

	function loadListBandara() {
		$.ajax({
			url : 'bandara/cruds/list.html',
			type : 'get',
			dataType : 'html',
			success : function(result) {
				$('#list-bandara').html(result);
			}
		});
	}
	
	//document ready itu setelah halamannya dipanggil
	$(document).ready(function() {

		//fungsi ajax untuk popup tambah
		$('#btn-add').on('click', function() {
			$.ajax({
				url : 'bandara/cruds/add.html',
				type : 'get',
				dataType : 'html',
				success : function(result) {
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		})
		//akhir fungsi ajax untuk popup tambah

		$('#modal-input').on('submit', '#form-bandara-add', function() {
			$.ajax({
				url : 'bandara/cruds/create.json',
				type : 'get',
				dataType : 'json',
				data : $(this).serialize(),
				success : function(result) {
					$('#modal-input').modal('hide');
					alert("data telah ditambah");
					loadListBandara();
				}
			});
			return false;
		});

		//fungsi ajax untuk lihat detail
		$('#list-bandara').on('click','#btn-detail', function() {
			var idBandara = $(this).val();
			$.ajax({
				url : 'bandara/cruds/detail.html',
				type : 'get',
				dataType : 'html',
				data:{idBandara:idBandara},
				success : function(result) {
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		
		//edit popup btn
		$('#list-bandara').on('click','#btn-edit', function() {
			var idBandara = $(this).val();
			$.ajax({
				url : 'bandara/cruds/edit.html',
				type : 'get',
				dataType : 'html',
				data:{idBandara:idBandara},
				success : function(result) {
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		
		//fungsi ajax untuk update di dalam popup update
		$('#modal-input').on('submit', '#form-bandara-edit', function() {
			$.ajax({
				url : 'bandara/cruds/update.json',
				type : 'get',
				dataType : 'json',
				data : $(this).serialize(),
				success : function(result) {
					$('#modal-input').modal('hide');
					alert("data telah diubah");
					loadListBandara();
				}
			});
			return false;
		});
		
		//btn hapus
		$('#list-bandara').on('click','#btn-delete', function() {
			var idBandara = $(this).val();
			$.ajax({
				url : 'bandara/cruds/remove.html',
				type : 'get',
				dataType : 'html',
				data:{idBandara:idBandara},
				success : function(result) {
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		
		//fungsi untuk menjalakan hapus pada pop up
		$('#modal-input').on('submit', '#form-bandara-delete', function() {
			$.ajax({
				url : 'bandara/cruds/delete.json',
				type : 'get',
				dataType : 'json',
				data : $(this).serialize(),// ambil data dari backend secara kesuluruhan
				success : function(result) {
					$('#modal-input').modal('hide');
					alert("data telah dihapus");
					loadListBandara();
				}
			});
			return false;
		});
		
	});
	//akhir dokumen ready
</script>