<div class="panel" style="background: white; margin-top: 50px; min-height: 500px;">
	<h1>Halaman Kota</h1>
	<button type="button" class="btn btn-primary" id="btn-add">Tambah</button>
	
	<div>
		<table class="table" id="tbl-kota">
			<tr>
				<td></td>
				<td></td>
				<td align="right">Cari Kode / Nama :</td>
				<td>
					<form action="#" method="get" id="form-kota-search">
						<input type="text" id="keyword" name="keyword" size="20">
						<button type="submit">Cari</button>
					</form>
				</td>
			</tr>
			<tr>
				<td>No</td>
				<td>Kode Kota</td>
				<td>Nama Kota</td>
				<td>Action</td>
			</tr>
			
			<tbody id="list-kota">
				<!-- isi Listnya -->
			</tbody>
		</table>
	</div>
	
</div>

<div id="modal-input" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<i class="fa fa-close"></i>			
				</button>
				<h4>Form PopUp Kota</h4>
			</div>
			<div class="modal-body">
				<!-- tempat jsp popupnya -->
			</div>
		</div>
	</div>
</div>


<script>

	loadListKota();
	// fungsi untuk list data Kota
	function loadListKota(){
		$.ajax({
			url:'kota/cruds/list.html',
			type:'get',
			dataType:'html',
			success: function(result){
				$('#list-kota').html(result);
			}
		});
	}

	//document ready itu setelah halaman dipanggil
	$(document).ready(function(){
		
		//fungsi ajax untuk popup tambah
		$('#btn-add').on('click',function(){
			$.ajax({
				url:'kota/cruds/add.html',
				type:'get',
				dataType:'html',
				success: function(result){
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		});
		//akhir fungsi ajax untuk popup tambah
		
		
		//fungsi ajax untuk create tambah
		$('#modal-input').on('submit','#form-kota-add',function(){
			$.ajax({
				url:'kota/cruds/create.json',
				type:'get',
				dataType:'json',
				data: $(this).serialize(),
				success: function(result){
					
					if (result.jumlahDataKota > 0) {
						alert("Kode "+result.namaKota+" sudah ada di DB")
					} else {
						$('#modal-input').modal('hide');
						alert("data telah ditambah");
						loadListKota();
					}
				}
			});
			return false; // tanpa load ulang atau refresh
		});
		//akhir fungsi ajax untuk create tambah
		
		//fungsi ajax untuk lihat
		 $('#list-kota').on('click','#btn-detail',function(){
			var idKota = $(this).val();
			$.ajax({
				url:'kota/cruds/detail.html',
				type:'get',
				dataType:'html',
				data:{idKota:idKota},
				success: function(result){
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		}); 
		//akhir fungsi ajax untuk lihat
		
		//fungsi ajax untuk ubah
		$('#list-kota').on('click','#btn-edit',function(){
			var idKota = $(this).val();
			$.ajax({
				url:'kota/cruds/edit.html',
				type:'get',
				dataType:'html',
				data:{idKota:idKota},
				success: function(result){
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		}); 
		//akhir fungsi ajax untuk ubah
		
		//fungsi ajax untuk update
		$('#modal-input').on('submit','#form-kota-edit',function(){
			
			$.ajax({
				url:'kota/cruds/update.json',
				type:'get',
				dataType:'json',
				data:$(this).serialize(),
				success: function(result){
					$('#modal-input').modal('hide');
					alert("data telah diubah");
					loadListKota();
				}
			});
			return false;
		}); 
		//akhir fungsi ajax untuk update
		
		 //fungsi ajax untuk hapus
		$('#list-kota').on('click','#btn-delete',function(){
			var idKota = $(this).val();
			$.ajax({
				url:'kota/cruds/remove.html',
				type:'get',
				dataType:'html',
				data:{idKota:idKota},
				success: function(result){
					$('#modal-input').find('.modal-body').html(result);
					$('#modal-input').modal('show');
				}
			});
		}); 
		//akhir fungsi ajax untuk hapus
		
		 //fungsi ajax untuk delete
		$('#modal-input').on('submit','#form-kota-delete',function(){
			
			$.ajax({
				url:'kota/cruds/delete.json',
				type:'get',
				dataType:'json',
				data:$(this).serialize(),
				success: function(result){
					$('#modal-input').modal('hide');
					alert("data telah dihapus");
					loadListKota();
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk delete
		
		//fungsi ajax untuk search namaKota
		$('#form-kota-search-nama').on('submit',function(){
			$.ajax({
				url:'kota/cruds/search/nama.html',
				type:'get',
				dataType:'html',
				data: $(this).serialize(),
				success: function(result){
					alert("data telah dicari berdasarkan nama");
					$('#list-kota').html(result); //reload list kota
				}
			});
			return false;
		}); 
		//akhir fungsi ajax untuk search namakota
		
		//fungsi ajax untuk search kodekota
		$('#form-kota-search-kode').on('submit',function(){
			$.ajax({
				url:'kota/cruds/search/kode.html',
				type:'get',
				dataType:'html',
				data: $(this).serialize(),
				success: function(result){
					alert("data telah dicari berdasarkan kode");
					$('#list-kota').html(result); //reload list kota
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk search kodekota
		
		//fungsi ajax untuk search kodekota / namakota
		$('#form-kota-search').on('submit',function(){
			$.ajax({
				url:'kota/cruds/search.html',
				type:'get',
				dataType:'html',
				data: $(this).serialize(),
				success: function(result){
					alert("data telah dicari berdasarkan kode atau nama");
					$('#list-kota').html(result); //reload list kota
				}
			});
			return false;
		});
		//akhir fungsi ajax untuk search kodekota / namakota */
		
	});
	//akhir dokumen ready

</script>