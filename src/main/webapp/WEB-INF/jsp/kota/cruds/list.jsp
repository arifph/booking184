<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:forEach items="${kotaModelList}" var="kotaModel" varStatus="number">
	<tr>
		<td>${number.count}</td>
		<td>${kotaModel.kodeKota}</td>
		<td>${kotaModel.namaKota}</td>
		<td>
			<button type="button" class="btn btn-warning" value="${kotaModel.idKota}" id="btn-detail">Lihat</button>
			<button type="button" class="btn btn-success" value="${kotaModel.idKota}" id="btn-edit">Ubah</button>
			<button type="button" class="btn btn-danger" value="${kotaModel.idKota}" id="btn-delete">Hapus</button>
		</td>
	</tr>
</c:forEach>