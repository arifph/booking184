<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<div class="form-horizontal">
	<h1>Lihat Kota</h1>
	
	<form action="#" method="get" id="form-kota-detail">
		<table>
		
			<tr>
				<td>Kode Kota</td>
				<td><input type="text" id="kodeKota" name="kodeKota" value="${kotaModel.kodeKota}"> </td>

			</tr>
			<tr>
				<td>Nama Kota</td>
				<td><input type="text" id="namaKota" name="namaKota" value="${kotaModel.namaKota}"> </td>
			</tr>
			<tr>						
				</td>
			</tr>
			 <tr>
				<td>Created By</td>
				<td>${kotaModel.xCreatedBy.username}</td>
			</tr>
			<tr>
				<td>Created Date</td>
				<td>${kotaModel.xCreatedDate}</td>
			</tr> 
			<tr>
				<td>Updated By</td>
				<td>${kotaModel.xUpdatedBy.username}</td>
			</tr>
			<tr>
				<td>Updated Date</td>
				<td>${kotaModel.xUpdatedDate}</td>
			</tr>
		</table>
	
	</form>
</div>