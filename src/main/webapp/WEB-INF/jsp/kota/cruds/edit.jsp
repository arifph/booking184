<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<div class="form-horizontal">
	<h1>Ubah Kota</h1>
	
	<form action="#" method="get" id="form-kota-edit">
		<input type="hidden" id="idKota" name="idKota" value ="${kotaModel.idKota}">
		<table>
	
		
			<tr>
				<td>Kode Kota</td>
				<td><input type="text" id="kodeKota" name="kodeKota" value="${kotaModel.kodeKota}"> </td>

			</tr>
			<tr>
				<td>Nama Kota</td>
				<td><input type="text" id="namaKota" name="namaKota" value="${kotaModel.namaKota}"> </td>
			</tr>
			<tr>
				
			</tr>
			<!-- <tr>
				<td>VarJSP</td>
				<td><input type="text" id="varJSP" name="varJSP"> </td>
			</tr> -->
			<tr>
				<td></td>
				<td><button type="submit">Update</button></td>
			</tr>
			
		</table>
	
	</form>
</div>