<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>

<div class="form-horizontal">
	<form action="#" method="get" id="form-stasiun-detail">
		<input type="hidden" name="idStasiun" id="idStasiun"
			value="${stasiunModel.idStasiun}">
		<table>
			<tr>
				<td>Kode Stasiun</td>
				<td><input type="text" id="kodeStasiun" name="kodeStasiun"
					readonly="readonly" value="${stasiunModel.kodeStasiun}"></td>
			</tr>
			<tr>
				<td>Nama Stasiun</td>
				<td><input type="text" id="namaStasiun" name="namaStasiun"
					readonly="readonly" value="${stasiunModel.namaStasiun}"></td>
			</tr>
			<tr>
				<td>Kota</td>
				<td><select id="idKota" name="idKota" disabled="disabled">
						<c:forEach items="${kotaModelList}" var="kotaModel">
							<option value="${kotaModel.idKota}"
								${kotaModel.idKota == stasiunModel.idKota ? 'selected="true"' : ''}>${kotaModel.namaKota}</option>
						</c:forEach>
				</select></td>
			</tr>
			<tr>
				<td>Telepon</td>
				<td><input type="text" id="telepon" name="telepon"
					readonly="readonly" value="${stasiunModel.telepon}"></td>
			</tr>
			<tr>
				<td>Email</td>
				<td><input type="email" id="email" name="email"
					readonly="readonly" value="${stasiunModel.email}"></td>
			</tr>
			<tr>
				<td>Alamat</td>
				<td><input type="text" id="alamat" name="alamat"
					readonly="readonly" value="${stasiunModel.alamat}"></td>
			</tr>
			<tr>
				<td>Created By</td>
				<td><input type="text" id="xCreatedBy" name="xCreatedBy"
					readonly="readonly" value="${stasiunModel.xCreatedBy.username}"></td>
			</tr>
			<tr>
				<td>Created Date</td>
				<td><input type="text" id="xCreatedBy" name="xCreatedBy"
					readonly="readonly" value="${stasiunModel.xCreatedDate}"></td>
			</tr>
			<tr>
				<td>Modified By</td>
				<td><input type="text" id="xCreatedBy" name="xCreatedBy"
					readonly="readonly" value="${stasiunModel.xUpdatedBy.username}"></td>
			</tr>
			<tr>
				<td>Modified Date</td>
				<td><input type="text" id="xCreatedBy" name="xCreatedBy"
					readonly="readonly" value="${stasiunModel.xUpdatedDate}"></td>
			</tr>
			<tr>
				<td>Deleted By</td>
				<td><input type="text" id="xCreatedBy" name="xCreatedBy"
					readonly="readonly" value="${stasiunModel.xDeletedBy.username}"></td>
			</tr>
			<tr>
				<td>Deleted Date</td>
				<td><input type="text" id="xCreatedBy" name="xCreatedBy"
					readonly="readonly" value="${stasiunModel.xDeletedDate}"></td>
			</tr>
			<!-- <tr>
				<td></td>
				<td><button type="submit">Simpan</button></td>
			</tr> -->
		</table>
	</form>
</div>