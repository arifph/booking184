<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>

<c:forEach items="${stasiunModelList}" var="stasiunModel">
	<tr>
		<td>${stasiunModel.kodeStasiun}</td>
		<td>${stasiunModel.namaStasiun}</td>
		<td>${stasiunModel.kotaModel.namaKota}</td>
		<td align="center"><button type="button" class="btn btn-warning"
				value="${stasiunModel.idStasiun}" id="btnEdit">Edit</button></td>
		<td align="center"><button type="button" class="btn btn-danger"
				value="${stasiunModel.idStasiun}" id="btnDelete">Delete</button></td>
		<td align="center"><button type="button" class="btn btn-success"
				value="${stasiunModel.idStasiun}" id="btnDetail">Lihat</button></td>
	</tr>
</c:forEach>