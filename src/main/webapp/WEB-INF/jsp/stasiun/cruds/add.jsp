<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>

<div class="form-horizontal">
	<form action="#" method="get" id="form-stasiun-add">
		<table>
			<tr>
				<td>Kode Stasiun</td>
				<td><input type="text" id="kodeStasiun" name="kodeStasiun"></td>
			</tr>
			<tr>
				<td>Nama Stasiun</td>
				<td><input type="text" id="namaStasiun" name="namaStasiun"></td>
			</tr>
			<tr>
				<td>Kota</td>
				<td>
					<select id="idKota" name="idKota">
						<c:forEach items="${kotaModelList}" var="kotaModel">
							<option value="${kotaModel.idKota}">${kotaModel.namaKota}</option>
						</c:forEach>
					</select>
				</td>
			</tr>
			<tr>
				<td>Telepon</td>
				<td><input type="text" id="telepon" name="telepon"></td>
			</tr>
			<tr>
				<td>Email</td>
				<td><input type="email" id="email" name="email"></td>
			</tr>
			<tr>
				<td>Alamat</td>
				<td><input type="text" id="alamat" name="alamat"></td>
			</tr>
			<tr>
				<td></td>
				<td><button type="submit">Simpan</button></td>
			</tr>
		</table>
	</form>
</div>