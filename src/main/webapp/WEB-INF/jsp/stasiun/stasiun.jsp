<div class="panel"
	style="background: white; min-height: 500px; margin-top: 50px">
	<div>
		<table class="table" id="tblStasiun">
			<tr>
				<td><h1>Data Stasiun</h1></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td align="right"><button type="button" class="btn btn-primary" id="btnAdd">Tambah</button></td>
			</tr>
			<tr align="center">
				<td rowspan="2">Kode Stasiun</td>
				<td rowspan="2">Nama Stasiun</td>
				<td rowspan="2">Kota</td>
				<td colspan="3">Action</td>
			</tr>
			<tr align="center">
				<td>Edit</td>
				<td>Delete</td>
				<td>Detil</td>
			</tr>
			
			<tbody id="list-stasiun"></tbody>
		</table>
	</div>
</div>

<div id="modal-input" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">

			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<i class="fa fa-close"></i>
				</button>
			</div>
			<div class="modal-body">
				<!-- Tempat jsp popupnya -->
			</div>
		</div>
	</div>
</div>

<script>
	loadListStasiun();

	function loadListStasiun() {
		$.ajax({
			url : 'stasiun/cruds/list.html',
			type : 'get',
			dataType : 'html',
			success : function(result) {
				$('#list-stasiun').html(result);
			}
		});
	}
	
	$('#btnAdd').on('click', function() {
		$.ajax({
			url : 'stasiun/cruds/add.html',
			type : 'get',
			dataType : 'html',
			success : function(result) {
				$('#modal-input').find('.modal-body').html(result);
				$('#modal-input').modal('show');
			}
		});
	});
	
	$('#modal-input').on('submit', '#form-stasiun-add', function() {
		$.ajax({
			url : 'stasiun/cruds/save.json',
			type : 'get',
			dataType : 'json',
			data : $(this).serialize(),
			success : function(result) {
				$('#modal-input').modal('hide');
				loadListStasiun();
			}
		});
		return false;
	});
	
	$('#list-stasiun').on('click', '#btnEdit', function() {
		var idStasiun = $(this).val();
		$.ajax({
			url : 'stasiun/cruds/edit.html',
			type : 'get',
			dataType : 'html',
			data : {idStasiun : idStasiun},
			success : function(result) {
				$('#modal-input').find('.modal-body').html(result);
				$('#modal-input').modal('show');
			}
		});
	});
	
	$('#modal-input').on('submit', '#form-stasiun-edit', function() {
		$.ajax({
			url : 'stasiun/cruds/update.json',
			type : 'get',
			dataType : 'json',
			data : $(this).serialize(),
			success : function(result) {
				$('#modal-input').modal('hide');
				loadListStasiun();
			}
		});
		return false;
	});
	
	$('#list-stasiun').on('click', '#btnDelete', function() {
		var idStasiun = $(this).val();
		$.ajax({
			url : 'stasiun/cruds/remove.html',
			type : 'get',
			dataType : 'html',
			data : {idStasiun : idStasiun},
			success : function(result) {
				$('#modal-input').find('.modal-body').html(result);
				$('#modal-input').modal('show');
			}
		});
	});
	
	$('#modal-input').on('submit', '#form-stasiun-delete', function() {
		$.ajax({
			url : 'stasiun/cruds/delete.json',
			type : 'get',
			dataType : 'json',
			data : $(this).serialize(),
			success : function(result) {
				$('#modal-input').modal('hide');
				loadListStasiun();
			}
		});
		return false;
	});
	
	$('#list-stasiun').on('click', '#btnDetail', function() {
		var idStasiun = $(this).val();
		$.ajax({
			url : 'stasiun/cruds/detail.html',
			type : 'get',
			dataType : 'html',
			data : {idStasiun : idStasiun},
			success : function(result) {
				$('#modal-input').find('.modal-body').html(result);
				$('#modal-input').modal('show');
			}
		});
	});
</script>